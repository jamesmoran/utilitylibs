#pragma once

/*	File: XmlUtils
	Includes and convenience functions for Xml data manipulation via the TinyXml library. Including this
	file will include TinyXml (nested in the xmlutils namespace) along with custom templated convenience 
	functions for type-safe value extraction and insertion.
*/

#include <time.h>
#include <string>
#include <tinyxml.h>
#include <tinystr.h>
#include <sstream>

/*	Namespace: xmlutils
	Namespace to encompass xml-related functionality, including the TinyXml library (3rd-party) and
	related convenience functions.
*/
namespace xmlutils{

/*	Function: getXmlAttribute
	Attempts to extract a value of type T from an attribute of pElement whose name is attrib_name

	Parameters:
		pElement - The TiXmlElement containing the attribute
		attrib_name - The name string of the attribute
		value_out - The reference to an object (type T) to which the extracted value is written on success.

	Return: Returns true on success, which means that an attribute was found with the specified name,
		and conversion to type T (via stringstream >> operator) was successful.
*/
template <class T>
bool getXmlAttribute(TiXmlElement* pElement, const std::string& attrib_name,T& value_out){
	if ( !pElement ) return false;

	TiXmlAttribute* pAttrib=pElement->FirstAttribute();
	while (pAttrib)
	{
		if(std::string(pAttrib->Name()).compare(attrib_name) == 0){
			
			std::stringstream ss(pAttrib->Value());
			ss >> value_out;
			return !!ss;
		}
		pAttrib=pAttrib->Next();
	}
	return false;
}

/*	Function: addChildElement
	Attempts to add a child TiXmlElement with text content to pElement, with a specified element name
	and value (converted to string via a stringstream).

	Parameters:
		pElement - The target TiXmlElement that will receive a child element.
		child_elem_name - The xml node name of the child element. Avoid spaces and special characters.
		elem_value - The value (type T) that will be converted to string via a stringstream and then inserted
			as the text content of the new child element.
	
	Return: Returns true on success, false otherwise. Failure can occur if pElement is NULL, or if conversion
		to string of elem_value fails.
	
*/
template <class T>
bool addChildElement(TiXmlElement* pElement, const std::string& child_elem_name, const T& elem_value){
	if(NULL == pElement){return false;}

	std::stringstream ss;
	ss << elem_value;

	TiXmlText* valText = new TiXmlText(ss.str().c_str());
	TiXmlElement* childElem = new TiXmlElement(child_elem_name.c_str());
	childElem->LinkEndChild(valText);
	pElement->LinkEndChild(childElem);

	return true;
}

/*	Function: getChildElement
	Attempts to extract a value of type T from the text content of the first child element of pElement
	with a specified name.

	Parameters:
		pElement - The target element whose appropirately-named child element will be the source of the text for the conversion.
		child_elem_name - The name of the child element whose text will be the source of the conversion.
		elem_value_out - The object (type T) that will be set by a stringstream >> operation on the text in the child element.

	Return:	Returns true on success, false on failure. Failures may occur if pElement is NULL, if the child_elem_name is not
		found among the child nodes of pElement, or if conversion from string of the child element text to type T fails.
*/
template <class T>
bool getChildElement(TiXmlElement* pElement, const std::string& child_elem_name, T& elem_value_out){
	if(NULL == pElement){return false;}

	TiXmlElement* childElem = pElement->FirstChildElement(child_elem_name.c_str());
	if(NULL == childElem){return false;}

	std::stringstream ss;
	if (strlen(childElem->GetText()) != 0)
	{
		ss << childElem->GetText();
	}
	ss >> elem_value_out;

	return !!ss;
}

}
