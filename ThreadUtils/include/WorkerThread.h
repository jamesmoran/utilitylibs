#pragma once

#include <deque>
#include <boost/function.hpp>
#include "ActiveObject.h"
#include "ScopedLock.h"

namespace threadutils{

	/*	Class: WorkerThread

		This class encapsulates a worker (background) thread with the capability to call an
		arbitrary TaskFunction when triggered. The thread can only execute one TaskFunction
		instance at a time, so multiple calls to RunTask() will have no effect if the
		TaskFunction is still running.
	
		Please Note:
			Startup - You must remember to call <Start> or the worker thread will never run.
			Timing - The worker thread may take time to complete: just because you called 
				<Process> doesn't mean it's finished.
			Function Signature: Your function must conform to the definition of TaskFunction
				below. Remember it takes a CONST REFERENCE as a parameter.
			Boost Bind: Remember the _1 parameter in the bind call.
			Thread Safety: Your function that you bind will be called from a different thread
				than the rest of your code. You are responsible for what this thread does. If
				you are unsure how to proceed, then give a static or global function as the 
				processing function and NEVER call that function from anywhere else.

		Example:
		(start code)
	
		class MultWorker : public threadutils::WorkerThread{
		public:

			MultWorker(const std::string& name, threadutils::ThreadSafeQueue<int>* p_inputQueue, int p_multiplier) : WorkerThread(boost::bind(&MultWorker::Task, this))
				, m_name(name)
				, m_inputQueue(p_inputQueue)
				, m_multiplier(p_multiplier)
			{

			}

			~MultWorker()
			{

			}

			boost::signals2::connection RegisterFor_Result(const MultWorkerResult::slot_type& p_slot)
			{
				return m_resultSignal.connect(p_slot);
			}

		protected:
			void Task()
			{
				bool enabled = true;
				while (enabled){
					int val = 0;
					bool gotData = m_inputQueue->Pop_Front(val);
					if (gotData){
					m_resultSignal(val*m_multiplier);
					}
					else{
					enabled = false;
					}
				}
			}

			MultWorkerResult m_resultSignal;
			threadutils::ThreadSafeQueue<int>* m_inputQueue;
			int m_multiplier;
			std::string m_name;
		};

		(end)
	*/
	class WorkerThread : public ActiveObject{
	public:

		/*	Typedef: TaskFunction
			This typedef names the boost function definition that is used within the worker
			thread. You must provide a function (or bound method) that matches this definition
			exactly to the <WorkerThread> constructor. That function/method will then be 
			called by the worker whenever the worker is triggered.
		*/
		typedef boost::function<void ()> TaskFunction;


		/*	Method: WorkerThread
			Creates a new worker thread

			Parameters:
				const TaskFunction & p_taskFunction - The function that the thread should run.
				This function will run to completion, once, each time the RunTask() function is
				called, unless it was busy when the function call came in. Typically one would
				use boost::bind() to create this function object.
		*/
		MMI_EXPORT WorkerThread(const TaskFunction& p_taskFunction);

		/*	Destructor: ~BufferedWorker
			The destructor will clear the queue if leftover items remain. It will block for up to 100ms
			waiting for the thread to voluntarily exit.
		*/
		MMI_EXPORT virtual ~WorkerThread();

		/*	Method: Start
			Starts the worker thread. You must call this at least once or the worker will do nothing.
		*/
		MMI_EXPORT void Start();

		/*	Method: RunTask
			Triggers the task function one time.
		*/
		MMI_EXPORT void RunTask();

		/*	Method: IsActive
			Returns true if the worker is currently iterating through items. Returns false if the worker is
			waiting for a call to <Process> or <EnqueueAndProcess>.
		*/
		MMI_EXPORT bool IsActive() const;

	protected:
		/*	Method: InitThread
			Noop.
		*/
		virtual void InitThread();

		/*	Method: Run
			The main worker thread loop. Waits for a command to process, and then swaps the queue into 
			and internal temporary queue. It then iterates over the queue, calling the process function
			on each item. Finally it discards the temporary queue and returns to waiting. 
		*/
		virtual void Run();

		/*	Method: FlushThread
			Clears the queue when the thread is killed.
		*/
		virtual void FlushThread();

	private:

		/*	Method: BufferedWorker
			Private copy constructor prevents copying according to the noncopyable pattern.
		*/
		WorkerThread(const WorkerThread& p_other);
		
		/*	Method: operator=
			Private operator= prevents copying according to the noncopyable pattern.
		*/
		void operator=(const WorkerThread& p_other);

		/*	Variable: m_taskFunction
			The boost::function to be called when RunTask() is called.
		*/
		TaskFunction m_taskFunction;
	
		/*	Variable: m_doTaskEvent
			The Event that is used to govern the worker's behavior. The worker waits until this
			event pulses before pulling a new set of queued items and processing them.
		*/
		Event m_doTaskEvent;

		/*	Variable: m_active
			State variable indicates whether the worker is currently iterating over items.
		*/
		bool m_active;
	};
}