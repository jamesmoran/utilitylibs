#pragma once

#include "CoreDefines.h"

namespace threadutils{

	/*	Class: PerformanceStats
		Class to represent a sample of the performance of a cyclical computational process.
		Intended usage is to track the performance of threads in the VES.
	*/
	class PerformanceStats{
	public:

		/*	Constructor: PerformanceStats
			Creates a new PerformanceStats struct, with all zero values
		*/
		MMI_EXPORT PerformanceStats();

		/*	Constructor: PerformanceStats
			There is an implicit copy constructor for this class
		*/

		/*	Method: AddCycle
			Add timing information for an individual cycle to the PerformanceStats. Increments the cycle count by one.

			Parameters:
				p_workingTime_ms - Working time in milliseconds. Working time is defined as the time between when the "work" portion
					of the loop is occurring.
				p_idleTime_ms - Idle time in milliseconds. Idle time is defined as any time where the thread is sleeping or blocked
					outside of the "work" being done. This value is only significant (typically) when there is some kind of 
					blocking synchronization call (like a call to WaitForSingleObject) or a call to Sleep() in the working loop.
					Setting this value to zero is valid if you don't care about or can't measure the idle time.
		*/
		MMI_EXPORT void AddCycle(double p_workingTime_ms, double p_idleTime_ms);

		/*	Method: AverageWorkingMs
			Returns the average working time, which is the sum of all reported working times divided by the cycle count.
		*/
		MMI_EXPORT double AverageWorkingMs() const;

		/*	Method: AverageIdleMs
			Returns the average idle time, which is the sum of all reported idle times divided by the cycle count.
		*/
		MMI_EXPORT double AverageIdleMs() const;

		/*	Method: AverageCycleMs
			Returns the average cycle time, which is the total time recorded divided by the cycle count.
		*/
		MMI_EXPORT double AverageCycleMs() const;

		/*	Method: AverageDutyCycle
			Returns the average duty cycle, which is the total time spent "working" divided by the total elapsed time.
		*/
		MMI_EXPORT double AverageDutyCycle() const;

		/*	Method: CycleCount
			Returns the cycle count, i.e. the number of times <AddCycle> was called.
		*/
		MMI_EXPORT __int64 CycleCount() const;

	protected:

		/*	Variable: m_cycles
			The number of times <AddCycle> has been called.
		*/
		__int64 m_cycles;

		/*	Variable: m_workingTime_sum
			The sum of all workingTime values entered into <AddCycle>
		*/
		double m_workingTime_sum;

		/*	Variable: m_workingTime_sum
			The sum of all idleTime values entered into <AddCycle>
		*/
		double m_idleTime_sum;

	};


}