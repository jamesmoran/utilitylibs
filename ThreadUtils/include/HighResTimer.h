#ifndef HIGH_RES_TIMER_H
#define HIGH_RES_TIMER_H

#if _MSC_VER > 1000
#pragma once
#endif // _MSC_VER > 1000

#include <windows.h> // needed for 'LARGE_INTEGER' typedef to be defined
#include <list>
#include "CoreDefines.h"

//
//
// high_res_timer.h
// A. Ohadi, MMI 2007
//
//	can be used to get a current-timestamp, as well as to measure intervals
//	uses the Windows routines accessing the performance counters (present in all modern CPUs)
//
//
//  sample usage - to get a timestamp (in units of seconds):
//
//		double the_timestamp = high_res_timer::timestamp_secs();
//
//
//  sample usage - to measure intervals in seconds:
//
//	{
//		high_res_timer interval;	// gets the start time on creation
//		{
//			// code block A
//		}
//		double block_a_duration = interval.get_lap_secs();
//		{
//			// code block B
//		}
//		double block_b_duration = interval.get_lap_secs();
//	}
//
//


namespace threadutils{

typedef double timestamp_type;
typedef std::list<timestamp_type> time_list_type;
typedef std::list<std::pair<timestamp_type, timestamp_type> > time_pair_list_type;


class high_res_timer  
{
public:
	MMI_EXPORT static double get_timestamp_secs();
	MMI_EXPORT static double get_timestamp_msecs();
	MMI_EXPORT void restart();

	MMI_EXPORT double get_start_secs() const;
	MMI_EXPORT double get_lap_secs() const;
	MMI_EXPORT double get_delta_from_start_secs() const;

	MMI_EXPORT double get_start_msecs() const;
	MMI_EXPORT double get_lap_msecs() const;
	MMI_EXPORT double get_delta_from_start_msecs() const;

	MMI_EXPORT high_res_timer();

private:
	high_res_timer(const high_res_timer&);
	high_res_timer &operator=(const high_res_timer&);

	mutable LARGE_INTEGER start_counter;
	mutable LARGE_INTEGER lap_start_counter;
	static double frequency; 
};


}

#endif
