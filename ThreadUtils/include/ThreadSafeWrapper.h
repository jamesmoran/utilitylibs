#pragma once

#include <windows.h>

namespace threadutils{

	/*	Class: ThreadSafeWrapper
		Templates utility class wraps an object with a critical section, permitting blocking and non-blocking
		access to Get or Set the value of the object within. Direct access is prevented. The template parameter
		class T must implement a default constructor, copy constructor and operator= (copyable pattern). The
		wrapper is non-copyable.
	*/
	template <class T>
	class ThreadSafeWrapper{
	public:
		MMI_EXPORT ThreadSafeWrapper(){
			::InitializeCriticalSection(&m_accessSerializer);
		}

		MMI_EXPORT ThreadSafeWrapper(const T& initialValue){
			::InitializeCriticalSection(&m_accessSerializer);
			m_value = initialValue;
		}

		MMI_EXPORT ~ThreadSafeWrapper(){
			::DeleteCriticalSection(&m_accessSerializer);
		}


		MMI_EXPORT ThreadSafeWrapper(const ThreadSafeWrapper& w){
			::InitializeCriticalSection(&m_accessSerializer);

			w.Get(m_value);
		}


		/*
			Method: operator=
			Warning! I don't know if this is 100% thread safe for w. In fact, I'm pretty sure it's not...
		*/
		MMI_EXPORT void operator=(const ThreadSafeWrapper& w){
			T temp;
			w.Get(temp);
			Set(temp);
		}


		/*
			Method: Get
			Blocking read-only accessor. Variable passed in as parameter val_out will be set to the wrapped value.
		*/
		MMI_EXPORT void Get(T& val_out) const{
			::EnterCriticalSection(&m_accessSerializer);
			val_out = m_value;
			::LeaveCriticalSection(&m_accessSerializer);
		}

		/*
			Method: Set
			Blocking set function. Internal value is set to value of parameter val.
		*/
		MMI_EXPORT void Set(const T& val){
			::EnterCriticalSection(&m_accessSerializer);
			m_value = val;
			::LeaveCriticalSection(&m_accessSerializer);
		}

		/*
			Method: TryGet
			Non-blocking read-only accessor. Variable passed in as parameter val_out will be set to the wrapped value IF
			wrapper is not currently locked by another thread. Otherwise this function returns immediately and has no effect.
			Return: Returns true if access was gained and val_out was set to the wrapped value, false otherwise;
		*/
		MMI_EXPORT bool TryGet(T& val_out) const{
			BOOL success = ::TryEnterCriticalSection(&m_accessSerializer);
			if(0 != success){
				val_out = m_value;
				::LeaveCriticalSection(&m_accessSerializer);
			}
			return 0 != success;
		}

		/*
			Method: TrySet
			Non-blocking set function. Internal value will be set to value of parameter val IF wrapper is not currently 
			locked by another thread. Otherwise this function returns immediately and has no effect.
			Return: Returns true if access was gained and wrapped value was set to val, false otherwise;
		*/
		MMI_EXPORT bool TrySet(const T& val){
			BOOL success = ::TryEnterCriticalSection(&m_accessSerializer);
			if(0 != success){
				m_value = val;
				::LeaveCriticalSection(&m_accessSerializer);
			}
			return 0 != success;
		}

	protected:

		mutable CRITICAL_SECTION m_accessSerializer;

		T m_value;

	
		


	};













}
