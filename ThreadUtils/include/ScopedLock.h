#ifndef UTILITYLIBS_SCOPEDLOCK_H_
#define UTILITYLIBS_SCOPEDLOCK_H_

#include <windows.h>
#include "CoreDefines.h"

namespace threadutils
{
	class ScopedLock
	{
	public:
		MMI_EXPORT ScopedLock(CRITICAL_SECTION* SynchronizationObject);
		MMI_EXPORT void Lock();
		MMI_EXPORT void Unlock();
		MMI_EXPORT ~ScopedLock();
	private:
		CRITICAL_SECTION *pSection;
	};
}


#endif