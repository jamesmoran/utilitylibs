#pragma once

#include <deque>
#include <boost/signals2/signal.hpp>
#include <ScopedLock.h>
#include <ActiveObject.h>
#include <opencv/cxcore.h>

namespace threadutils{

	template<class T>
	class ThreadSafeQueue{
	public:

		MMI_EXPORT ThreadSafeQueue(size_t p_maxLen)
			: m_maxLen(p_maxLen)
			, m_newDataEvent(false,false)
		{
			::InitializeCriticalSection(&m_queueCS);
		}

		MMI_EXPORT ~ThreadSafeQueue(){
			::DeleteCriticalSection(&m_queueCS);
		}

		MMI_EXPORT bool Empty() const{
			threadutils::ScopedLock lock(&m_queueCS);
			return m_queue.size() == 0;
		}

		MMI_EXPORT size_t Length() const{
			threadutils::ScopedLock lock(&m_queueCS);
			return m_queue.size();
		}

		MMI_EXPORT bool Push_Front(const T& val){
			bool success = false;
			threadutils::ScopedLock lock(&m_queueCS);
			if(m_queue.size() < m_maxLen){
				m_queue.push_front(val);
				m_newDataEvent.SetEvent();
				success = true;
			}
			return success;
		}

		MMI_EXPORT bool Push_Back(const T& val){
			bool success = false;
			threadutils::ScopedLock lock(&m_queueCS);
			if(m_queue.size() < m_maxLen){
				m_queue.push_back(val);
				m_newDataEvent.SetEvent();
				success = true;
			}
			return success;
		}

		MMI_EXPORT bool Pop_Front(T& val_out){
			bool success = false;
			threadutils::ScopedLock lock(&m_queueCS);
			if(m_queue.size() > 0){
				val_out = m_queue.front();
				m_queue.pop_front();
				success = true;
			}
			return success;
		}

		MMI_EXPORT bool Pop_Back(T& val_out){
			bool success = false;
			threadutils::ScopedLock lock(&m_queueCS);
			if(m_queue.size() > 0){
				val_out = m_queue.back();
				m_queue.pop_back();
				success = true;
			}
			return success;
		}
		
		MMI_EXPORT void Clear(){
			threadutils::ScopedLock lock(&m_queueCS);
			m_queue.clear();
		}

		HANDLE GetNewDataEvent(){
			return m_newDataEvent.GetHandle();
		}
		
	protected:

		size_t m_maxLen;
		std::deque<T> m_queue;
		mutable CRITICAL_SECTION m_queueCS;
		threadutils::Event m_newDataEvent;

	
	};

}
