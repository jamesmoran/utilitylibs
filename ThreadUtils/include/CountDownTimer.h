#pragma once

#include "ActiveObject.h"
#include <boost/signals2.hpp>
#include <boost/signals2/connection.hpp>
#include <vector>

namespace threadutils{

class CountDownTimer : public ActiveObject
{
public:

	typedef boost::signals2::signal<void()> CountDownEvent;

	MMI_EXPORT CountDownTimer();
	MMI_EXPORT ~CountDownTimer();

	MMI_EXPORT bool Start(int p_duration_ms, int p_interval_ms);
	MMI_EXPORT bool Stop();

	MMI_EXPORT boost::signals2::connection RegisterFor_CountDownExpiredEvent(const CountDownEvent::slot_type& p_slot);

	MMI_EXPORT virtual void InitThread();
	MMI_EXPORT virtual void Run();
	MMI_EXPORT virtual void FlushThread();

	MMI_EXPORT int Interval() const;
	MMI_EXPORT int Duration() const;
	MMI_EXPORT int Remaining() const;

	MMI_EXPORT bool Ready();

protected:

	int m_interval_ms;
	int m_duration_ms;
	int m_remaining_ms;

	int m_runCount;

	CountDownEvent m_event;
	std::vector< boost::signals2::connection > m_eventConnections;
};

}