#pragma once

#include <deque>
#include <boost/function.hpp>
#include "ActiveObject.h"
#include "ScopedLock.h"

namespace threadutils{

	/*	Class: BufferedWorker
		The BufferedWorker class is a wrapper around a thread and a queue. It follows a single-
		consumer pattern. Any number of entities can enqueue objects of template type Data_T
		via the <Enqueue> method. When the <Process> method is called, the worker thread
		becomes active. It will swap the queue with an empty one, and then drain the queue.
		As it drains the queue it will call the provided function on each item in the queue.
		When the queue is empty the thread goes back to sleep unless <Process> has been called 
		again. The function that is run is provided via the constructor, and can be any 
		boost::function. The typical usage would be to encapsulate the BufferedWorker as a
		member of another class, and to provide a method of that outer class to the 
		BufferedWorker via boost::bind. This means that the outer class can enqueue items,
		and then the worker thread will call the method on each item, permitting the outer
		class member variables to be accessible, but in a threaded context.

		Please Note:
			Startup - You must remember to call <Start> or the worker thread will never run.
			Timing - The worker thread may take time to complete: just because you called 
				<Process> doesn't mean it's finished.
			Function Signature: Your function must conform to the definition of ProcessFunction
				below. Remember it takes a CONST REFERENCE as a parameter.
			Boost Bind: Remember the _1 parameter in the bind call.
			Thread Safety: Your function that you bind will be called from a different thread
				than the rest of your code. You are responsible for what this thread does. If
				you are unsure how to proceed, then give a static or global function as the 
				processing function and NEVER call that function from anywhere else.

		Example:
		(start code)
		class BufferedWorkerTest_IntAdder{
			public:
				BufferedWorkerTest_IntAdder();

				void Add(int val);
				int GetSum() const;
				bool IsActive() const;
				bool NotFinished();
			
			protected:
				void AddToSum(const int& value);

				threadutils::BufferedWorker<int> m_bufferedWorker;

				int m_sum;
		};

		BufferedWorkerTest_IntAdder::BufferedWorkerTest_IntAdder()
			: m_bufferedWorker(boost::bind(&BufferedWorkerTest_IntAdder::AddToSum,this,_1),1000)
			, m_sum(0)
		{
			m_bufferedWorker.Start();
		}

		void BufferedWorkerTest_IntAdder::Add( int val )
		{
			m_bufferedWorker.EnqueueAndProcess(val);
		}

		int BufferedWorkerTest_IntAdder::GetSum() const
		{
			return m_sum;
		}

		void BufferedWorkerTest_IntAdder::AddToSum( const int& value )
		{
			m_sum += value;
		}

		bool BufferedWorkerTest_IntAdder::IsActive() const
		{
			return m_bufferedWorker.IsActive();
		}

		bool BufferedWorkerTest_IntAdder::NotFinished()
		{
			return m_bufferedWorker.QueueLength() > 0;
		}

		//---- Usage -----------------------

		BOOST_AUTO_TEST_CASE(test_bufferedworker){

			BufferedWorkerTest_IntAdder adder;

			//let the thread start up
			Sleep(100);

			//add some ints, and compute the reference solution
			int N = 100;
			int correctSum = 0;
			for(int i = 1; i < N; i++){
				adder.Add(i);
				correctSum+=i;
			}

			//wait until the thread is no longer active, and the queue is empty,
			//i.e. the worker has finished processing all the integers
			while(adder.IsActive() || adder.NotFinished()){
				Sleep(10);
			}

			//get the result 
			int sum = adder.GetSum();

			//is the result correct?
			BOOST_CHECK_EQUAL(correctSum, sum);
		}
		(end)
	*/
	template<class Data_T>
	class BufferedWorker : public ActiveObject{
	public:

		/*	Typedef: ProcessFunction
			This typedef names the boost function definition that is used within the worker
			thread. You must provide a function (or bound method) that matches this definition
			exactly to the <BufferedWorker> constructor. That function/method will then be 
			called by the worker on every enqueued item.
		*/
		typedef boost::function<void (const Data_T&)> ProcessFunction;

		/*	Constructor: BufferedWorker
			Create a new BufferedWorker. This is the only constructor, and it requires a 
			boost::function with a signature as defined by <ProcessFunction>. It also requires 
			a maximum queue length.

			Parameters:
				p_dataProcessFunction - A boost::function defined according to the ProcessFunction
				typedef. This can be a global function, a static member function of a class, or a
				non-static member of a class bound with boost::bind.
				p_maxQueueLength - The maximum number of items in the queue. Don't set it to zero.
		*/
		MMI_EXPORT BufferedWorker(const ProcessFunction& p_dataProcessFunction, size_t p_maxQueueLength)
			: m_processDataEvent(false,false)
			, m_processFunction(p_dataProcessFunction)
			, m_maxQueueLength(p_maxQueueLength)
			, m_active(false)
		{
			::InitializeCriticalSection(&m_dataQueueCS);	
		}

		/*	Destructor: ~BufferedWorker
			The destructor will clear the queue if leftover items remain. It will block for up to 100ms
			waiting for the thread to voluntarily exit.
		*/
		MMI_EXPORT virtual ~BufferedWorker(){
			m_processDataEvent.SetEvent();
			Kill(100);
			::DeleteCriticalSection(&m_dataQueueCS);
		}

		/*	Method: Start
			Starts the worker thread. You must call this at least once or the worker will do nothing.
		*/
		MMI_EXPORT void Start(){
			_thread.Resume();
		}

		/*	Method: Enqueue
			Enqueue an object. Note that this will invoke the copy constructor of the object to push it
			into the queue. The function will fail and return false if the queue is already full. Please
			note that this method does not signal the worker to start draining the queue. If you want that 
			to happen immediately, use <EnqueueAndProcess>.

			Return:
				bool - Returns true if the data was successfully enqueued, false otherwise.
		*/
		MMI_EXPORT bool Enqueue(const Data_T& p_data){
			ScopedLock queueLock(&m_dataQueueCS);
			if(m_dataQueue.size() <= m_maxQueueLength){
				m_dataQueue.push_back(p_data);
				return true;
			}
			return false;
		}

		/*	Method: Process
			Signal the worker to start draining the queue. If the worker is currently busy processing 
			a previous queue, this will cause it to immediately begin processing the new queue as soon
			as it is ready.
		*/
		MMI_EXPORT void Process(){
			m_processDataEvent.SetEvent();
		}

		/*	Method: EnqueueAndProcess
			Enqueue an object and signal the worker to drain the queue. Note that this will invoke the copy
			constructor of the object to push it into the queue. The function will fail and return false if
			the queue is already full. If the worker is currently busy processing a previous queue, this
			will cause it to immediately begin processing the new queue as soon as it is ready.

			Return:
				bool - Returns true if the data was successfully enqueued, false otherwise.
		*/
		MMI_EXPORT bool EnqueueAndProcess(const Data_T& p_data){
			bool enqueueSuccess = Enqueue(p_data);
			Process();
			return enqueueSuccess;
		}

		/*	Method: ClearQueue
			Clears any and all items in the current queue. Items already pulled into internal memory
			within the worker are not cleared.
		*/
		MMI_EXPORT void ClearQueue(){
			ScopedLock queueLock(&m_dataQueueCS);
			m_dataQueue.clear();
		}

		/*	Method: QueueLength
			Returns the current pending queue length. Does not count items in internal memory of the worker.
		*/
		MMI_EXPORT size_t QueueLength() const{
			ScopedLock queueLock(&m_dataQueueCS);
			return m_dataQueue.size();
		}

		/*	Method: IsActive
			Returns true if the worker is currently iterating through items. Returns false if the worker is
			waiting for a call to <Process> or <EnqueueAndProcess>.
		*/
		MMI_EXPORT bool IsActive() const{
			return m_active;
		}

	protected:
		/*	Method: InitThread
			Noop.
		*/
		virtual void InitThread(){
		
		};

		/*	Method: Run
			The main worker thread loop. Waits for a command to process, and then swaps the queue into 
			and internal temporary queue. It then iterates over the queue, calling the process function
			on each item. Finally it discards the temporary queue and returns to waiting. 
		*/
		virtual void Run(){
			
			while(!_isDying){
				m_active = false;
				WaitForSingleObject(m_processDataEvent,INFINITE);
				if (_isDying){
					break;
				}

				m_active = true;

				std::deque<Data_T> tempQueue;
				
				{
					ScopedLock queueLock(&m_dataQueueCS);
					m_dataQueue.swap(tempQueue);
				}

				for(auto dataIter = tempQueue.begin(); dataIter != tempQueue.end() && !_isDying; dataIter++){
					if(m_processFunction){
						m_processFunction(*dataIter);
					}
				}

				tempQueue.clear();
			}

		};

		/*	Method: FlushThread
			Clears the queue when the thread is killed.
		*/
		virtual void FlushThread(){
			ClearQueue();
		};

	private:

		/*	Method: BufferedWorker
			Private copy constructor prevents copying according to the noncopyable pattern.
		*/
		BufferedWorker(const BufferedWorker& p_other){}
		
		/*	Method: operator=
			Private operator= prevents copying according to the noncopyable pattern.
		*/
		void operator=(const BufferedWorker& p_other){}

		/*	Variable: m_processFunction
			The boost::function to be called on each queued item.
		*/
		ProcessFunction m_processFunction;
	
		/*	Variable: m_dataQueue
			The data queue.
		*/
		std::deque<Data_T> m_dataQueue;

		/*	Variable: m_dataQueueCS
			The critical section protecting the data queue from concurrent modifications.
		*/
		mutable CRITICAL_SECTION m_dataQueueCS;

		/*	Variable: m_processDataEvent
			The Event that is used to govern the worker's behavior. The worker waits until this
			event pulses before pulling a new set of queued items and processing them.
		*/
		Event m_processDataEvent;

		/*	Variable: m_maxQueueLength
			The max. queue length.
		*/
		size_t m_maxQueueLength;

		/*	Variable: m_active
			State variable indicates whether the worker is currently iterating over items.
		*/
		bool m_active;
	};
}