#ifndef MMISERVER_ACTIVEOBJECT_H_
#define MMISERVER_ACTIVEOBJECT_H_

#include <windows.h>
#include "CoreDefines.h"

namespace threadutils{

	/*
		Class: Event
		Provides a simple object-oriented wrapper around an anonymous Win32 event (HANDLE).
		This class is intended to be a direct replacement for the MFC CEvent class, to avoid
		MFC dependency.
	*/
	class Event{
	public:
		MMI_EXPORT Event(bool p_manualReset, bool p_initialState);
		MMI_EXPORT ~Event();

		MMI_EXPORT bool SetEvent();
		MMI_EXPORT bool ResetEvent();

		MMI_EXPORT HANDLE GetHandle();

		MMI_EXPORT operator HANDLE();

	protected:
		HANDLE m_eventHandle;
	
	};

	class Thread
	{
	public:

		MMI_EXPORT Thread ( DWORD (WINAPI * pFun) (void* arg), void* pArg)
		{
			_handle = CreateThread (
				0, // Security attributes
				0, // Stack size
				pFun,
				pArg,
				CREATE_SUSPENDED,
				&_tid);
		}

		MMI_EXPORT ~Thread () { 
			CloseHandle (_handle); 
		}

		MMI_EXPORT void Resume () { 
			ResumeThread (_handle); 
		}

		MMI_EXPORT bool SetThreadPriority(int p_priority){
			
			bool ok = false;
			int priority = THREAD_PRIORITY_NORMAL;
			switch (p_priority){
			case THREAD_PRIORITY_LOWEST:
			case THREAD_PRIORITY_BELOW_NORMAL:
			case THREAD_PRIORITY_NORMAL:
			case THREAD_PRIORITY_HIGHEST:
			case THREAD_PRIORITY_ABOVE_NORMAL:
				priority = p_priority;
				ok = true;
				break;
			default:
				priority = THREAD_PRIORITY_NORMAL;
				ok = false;
			}

			return ok && (0 != ::SetThreadPriority(_handle, priority));
		}

		MMI_EXPORT void WaitForDeath (DWORD p_milliseconds)
		{
			WaitForSingleObject (_handle, p_milliseconds);
		}

		MMI_EXPORT HANDLE getHandle(){
			return _handle;
		}

	private:
		HANDLE _handle;
		DWORD  _tid;     // thread id
	};

	class ActiveObject
	{
	public:
		MMI_EXPORT ActiveObject ();
		MMI_EXPORT virtual ~ActiveObject () {
		
		}

		/*	Method: SetThreadPriority
		
			Parameters:
				int p_priority - The intended thread priority. Only the following values are accepted.

			Values:
				Lowest - THREAD_PRIORITY_LOWEST
				Low - THREAD_PRIORITY_BELOW_NORMAL
				Normal - THREAD_PRIORITY_NORMAL
				High - THREAD_PRIORITY_ABOVE_NORMAL
				Highest - THREAD_PRIORITY_HIGHEST    (USE WITH CARE!)
				
			Returns: 
				bool - Returns true if the priority was set.
		*/
		MMI_EXPORT bool SetThreadPriority(int p_priority){
			return _thread.SetThreadPriority(p_priority);
		}

		MMI_EXPORT void Kill (DWORD p_waitMilliseconds);

		MMI_EXPORT void WaitForDeath(DWORD p_waitMilliseconds);

		MMI_EXPORT HANDLE gethandle(){
			return _thread.getHandle();
		}

	protected:
		virtual void InitThread () = 0;
		virtual void Run () = 0;
		virtual void FlushThread () = 0;

		

		static DWORD WINAPI ThreadEntry (void *pArg);

		int             _isDying;
		Thread          _thread;
	};

	class Mutex
	{
		friend class Lock;
		friend class Event;
	public:
		MMI_EXPORT Mutex () { InitializeCriticalSection (& _critSection); }
		MMI_EXPORT ~Mutex () { DeleteCriticalSection (& _critSection); }
	private:
		MMI_EXPORT void Acquire ()
		{
			EnterCriticalSection (& _critSection);
		}
		MMI_EXPORT void Release ()
		{
			LeaveCriticalSection (& _critSection);
		}

		CRITICAL_SECTION _critSection;
	};

	class Lock
	{
	public:
		// Acquire the state of the semaphore
		MMI_EXPORT Lock ( Mutex & mutex )
			: _mutex(mutex)
		{
			_mutex.Acquire();
		}
		// Release the state of the semaphore
		MMI_EXPORT ~Lock ()
		{
			_mutex.Release();
		}
	private:
		Mutex & _mutex;
	};

}

#endif