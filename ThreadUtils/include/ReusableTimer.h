#pragma once

#include "ActiveObject.h"
#include <vector>
#include <boost/signals2/signal.hpp>
#include "HighResTimer.h"

namespace threadutils{

class ReusableTimer : public ActiveObject
{
public:

	typedef boost::signals2::signal<void(int)> TickEvent;
	typedef boost::signals2::signal<void()> EndEvent;


	MMI_EXPORT ReusableTimer();
	MMI_EXPORT ~ReusableTimer();

	MMI_EXPORT bool Start(int p_duration, int p_interval, bool repeat);
	MMI_EXPORT bool Stop();

	MMI_EXPORT int GetRemainingMs() const;
	MMI_EXPORT int GetDurationMs() const;
	MMI_EXPORT int GetIntervalMs() const;
	MMI_EXPORT int GetRepeatCount() const;
	MMI_EXPORT bool IsRepeating() const;
	MMI_EXPORT bool IsActive() const;

	MMI_EXPORT void SetDebugOutput(bool enableDebugOut);

	MMI_EXPORT boost::signals2::connection RegisterFor_TickEvent(const TickEvent::slot_type& p_slot);
	MMI_EXPORT boost::signals2::connection RegisterFor_EndEvent(const EndEvent::slot_type& p_slot);

protected:


	virtual void InitThread();
	virtual void Run();
	virtual void FlushThread();

	void DoOneCycle();

	bool m_threadStarted;

	bool m_active;

	HANDLE m_controlEvent;

	bool m_threadStayAlive;

	int m_durationMs;
	int m_intervalMs;
	int m_remainingMs;
	bool m_repeat;
	int m_repeatCount;
	bool m_debugout;

	TickEvent m_tickEvent;
	EndEvent m_endEvent;
private:
	high_res_timer m_sleepTimer;
};

/* Class: NetworkTimer

   NetworkTimer is a wrapper around ResuableTimer meant to periodically check for network connections.
*/
class NetworkTimer : public ReusableTimer
{
	typedef boost::signals2::signal<bool()> PingEvent;
	typedef boost::signals2::signal<void(bool)> ConnectionEvent;

	NetworkTimer(const PingEvent::slot_type& p_slot, int seconds = 5);
	~NetworkTimer();

	void StartTimer();
	boost::signals2::connection RegisterFor_ConnectionEvent(const ConnectionEvent::slot_type& p_slot);

protected:
	void OnEndEvent();

	PingEvent m_pingEvent;
	ConnectionEvent m_connectionEvent;
	int m_seconds;
};
}
