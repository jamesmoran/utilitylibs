#pragma once
///@file MultimediaTimerWrapper.h 
///@brief Contains the MultimediaTimer class and supporting functions.
///
///This class relies on the BOOST library's signals and slots. Be sure to include %BOOST_ROOT% in the "additional include
///directories" and %BOOST_ROOT%\lib in the "additional library directories" for proper compilation and linking. 

#include <windows.h>						//for timeSetEvent, timeKillEvent
#include <vector>							//for std::vector<>
#include <boost/signals2.hpp>					//for boost::signal<>
#include <boost/signals2/connection.hpp>		//for boost::signals::connection
#include "CoreDefines.h"

namespace threadutils{

	///@brief The global callback function used to connect the Multimedia Timer timeSetEvent() to 
	///the threadustil::MultimediaTimer class
	///@warning This function is not intended to be called manually.
	MMI_EXPORT void CALLBACK MultimediaTimerCallback(UINT wTImerID, UINT msg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2);

	///@brief The MultimediaTimer's signal type.
	typedef boost::signals2::signal<void ()> MultimediaTimerEvent;

	///@brief The MultimediaTimer is a wrapper class on the Windows multimedia timer (threaded timer) functionality.
	///
	///This class permits you to create a single-shot or repeating timer that uses a BOOST signal to invoke an arbitrary
	///number of functions. The timer permits you to specify the interval between timer calls and the desired resolution
	///of the timing.
	class MultimediaTimer{
	
	public:

		///Creates a new multimedia timer object. No threads are initialized at this point. The Start() function performs
		///the actual thread creation and initialization steps.
		MMI_EXPORT MultimediaTimer();

		///Closes all connections, stops the timer and exits.
		MMI_EXPORT ~MultimediaTimer();

		///Start the timer. The timer will run and begin invoking any functions that have been registered to the TimerEvent.
		///@param uDelay The delay before the timer fires, or the delay between successive firings, depending on whether this
		///is a repeating timer or not. Units are milliseconds.
		///@param uResolution The resolution of the timer's accuracy. Units are milliseconds, smaller numbers indicate greater accuracy.
		///A value of 0 indicates maximum attainable accuracy. Lower resolution values increase CPU load.
		///@param repeat Determines if this is a repeating or one-shot timer. A value of true will cause the timer to continuously fire
		///until Kill() is called.
		///@return Returns true if the timer has been successfully initialized. Returns false if called when the timer is already running.
		MMI_EXPORT bool Start(UINT uDelay, UINT uResolution, bool repeat);

		///Stops the timer. 
		///@return Returns true if the timer is not running when this function exits. This include both a successful shutdown, or if the
		///timer was not running previously.
		MMI_EXPORT void Kill();

		///@return Returns the number of times the signal has been invoked.
		MMI_EXPORT int GetCycleCount() const;
		
		///Forces execution of the signal. This permits a manual override of the timer if it is desired. Executions forced by use of this
		///function are included in the cycle count.
		MMI_EXPORT void ExecuteNow();

		///Registers a function to be called by the MultimediaTimer's event. These can be global functions, static class members or 
		///non-static class member functions. To register a non-static class member function, you must use boost::bind() to create the
		///slot. 
		///@param p_slot A slot (function) with a signature consistent with the MultimediaTimerEvent signal. Functions that are compatible
		///have a void return type and no parameters. This function will be called whenever the timer fires or whenever the ExecuteNow()
		///function is called. 
		///@warning Make sure your slot function is thread safe: the timer uses its own thread to fire the signal, so the slot can
		///be called concurrently with any other functionality in the program.
		///@return Returns the connection object representing the connection between the signal and the provided slot. You can call the
		///disconnect() function of this connection object at any time to sever the connection. The connection is also severed if the 
		///MultimediaTimer is destroyed.
		MMI_EXPORT boost::signals2::connection RegisterFor_TimerEvent(const MultimediaTimerEvent::slot_type& p_slot);

	protected:
	
		///Closes all connections to the m_timerEventConnections signal
		void CloseAllConnections();

		///Flag for whether the timer is active or not.
		bool m_active;

		///Stores the ID of the windows multimedia timer as returned my the timeSetEvent() function.
		UINT m_timerID;

		///The boost signal that is invoked when the timer fires.
		MultimediaTimerEvent m_timerEvent;

		///The list of conenctions to the timer Event, stored so that the connections can be automatically severed if the timer goes out of scope.
		std::vector< boost::signals2::connection > m_timerEventConnections;
	
		///Critical section to prevent simultaneous calls to the Start() or Kill() functions
		CRITICAL_SECTION m_threadControlSerializer;

		///Critical section to prevent simultaneous firings of the timer, including via the ExecuteNow() public override.
		CRITICAL_SECTION m_eventSerializer;
		
		///The counter for the number of times the signal is fired.
		int m_cycleCount;
	};

}
