#pragma once

#include <windows.h>
#include <HighResTimer.h>
#include "PerformanceStats.h"

namespace threadutils{

	class PerformanceTimer{
	public:

		/*	Constructor: PerformanceTimer
			Initializes a new performance timer.
		*/
		MMI_EXPORT PerformanceTimer();

		/*	Destructor: ~PerformanceTimer
			Cleans up and destroys the PerformanceTimer
		*/
		MMI_EXPORT ~PerformanceTimer();

		/*	Method: GetPerformanceStats
			Get the latest performance statistics, by writing them into output parameter stats_out.
		*/
		MMI_EXPORT void GetPerformanceStats(PerformanceStats& stats_out);

		/*	Method: WorkStart
			Timing function used to instrument the performance of the module. Call this function in a loop right before
			the "work" done within the loop. This tells the instrumentation code that the "idle" period is over and
			the "work" period has begun. Results of performance instrumentation are available in m_performanceStats.

			See Also:
				<WorkEnd>
				<m_performanceStats>

			Example:
				(Start Code)

				while(threadAlive){
		
					WaitForSingleObject(cycleControlEvent, ... );	//this contributes to "idle time"

					WorkStart();

					DoSomethingComputationallyExpensive();		//this contributes to "working time"

					WorkEnd();
		
					Sleep(30);		//this constributes to "idle time"
				}

				(End)
		*/
		MMI_EXPORT void WorkStart();

		/*	Method: WorkStart
			Timing function used to instrument the performance of the module. Call this function in a loop right after
			the "work" done within the loop. This tells the instrumentation code that the "work" period is over and
			the "idle" period has begun. See example in entry on <WorkStart>

			See Also:
				<WorkStart>
				<m_performanceStats>
		*/
		MMI_EXPORT void WorkEnd();

	protected:

		/*	Variable: m_performanceStatsAccessSerializer
			Critical section, protects access to the m_performanceStats variable to prevent threaded access
			to the performance data from causing a concurrency problem.
		*/
		mutable CRITICAL_SECTION m_performanceStatsAccessSerializer;

		/*	Variable: m_performanceStats
			Performance statistics object, keeps track of the module's performance via the "WorkStart" and "WorkEnd" functions.
		*/
		PerformanceStats m_performanceStats;

		/*	Variable: m_cycleTimer
			High resolution timer, used to keep track of the timing of this module.
		*/
		threadutils::high_res_timer m_cycleTimer;
	
		/*	Variable: m_idleTime
			Variable used to keep track of the time spent at idle. (Milliseconds)
		*/
		double m_idleTime;

		/*	Variable: m_workingTime
			Variable used to keep track of the time spent working. (Milliseconds)
		*/
		double m_workingTime;

	private:

		/*	Constructor: PerformanceTimer
			Private copy constructor prevents copying (non-copyable pattern)
		*/
		PerformanceTimer(const PerformanceTimer& p);

		/*	Method: operator=
			Private assignment operator prevents copying (non-copyable pattern)
		*/
		void operator=(const PerformanceTimer& p);
	};

}
