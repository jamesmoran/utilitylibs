find_package(Boost 1.58.0 REQUIRED COMPONENTS
	signals
)
if (Boost_FOUND)
else()
	message("Unable to locate boost 1.58, please set BOOST_ROOT with the required libraries.")
endif()

set(INCLUDE_FILES
	include/ActiveObject.h
	include/BufferedWorker.h
	include/CountDownTimer.h
	include/CoreDefines.h
	include/HighResTimer.h
	include/MultimediaTimerWrapper.h
	include/PerformanceStats.h
	include/PerformanceTimer.h
	include/ReusableTimer.h
	include/ScopedLock.h
	include/ThreadSafeQueue.h
	include/ThreadSafeWrapper.h
	include/ThreadUtils.h
	include/WorkerThread.h
)
set(SRC_FILES
	src/ActiveObject.cpp
	src/CountDownTimer.cpp
	src/HighResTimer.cpp
	src/MultimediaTimerWrapper.cpp
	src/PerformanceStats.cpp
	src/PerformanceTimer.cpp
	src/ReusableTimer.cpp
	src/ScopedLock.cpp
	src/WorkerThread.cpp
)

add_library(ThreadUtils SHARED 
	${INCLUDE_FILES}
	${SRC_FILES}
)
target_include_directories(ThreadUtils PUBLIC
	include/
	${Boost_INCLUDE_DIRS}
)
target_link_libraries(ThreadUtils
	${Boost_LIBRARIES}
	Winmm.lib
)

install(TARGETS
	ThreadUtils 
	RUNTIME DESTINATION ${INSTALL_BINARIES_DIR} COMPONENT Runtime
	ARCHIVE DESTINATION ${INSTALL_LIBRARIES_DIR} COMPONENT Runtime
)
install(FILES
	${INCLUDE_FILES}
	DESTINATION ${INSTALL_HEADERS_DIR}/ThreadUtils
)
