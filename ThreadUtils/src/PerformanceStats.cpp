#include "PerformanceStats.h"

namespace threadutils{

	PerformanceStats::PerformanceStats()
		: m_cycles(0)
		, m_idleTime_sum(0)
		, m_workingTime_sum(0)
	{

	}

	void PerformanceStats::AddCycle( double p_workingTime_ms, double p_idleTime_ms )
	{
		m_workingTime_sum += p_workingTime_ms;
		m_idleTime_sum += p_idleTime_ms;
		m_cycles++;
	}

	double PerformanceStats::AverageWorkingMs() const
	{
		if(m_cycles <= 0){
			return 0;
		}
		return m_workingTime_sum / (double) m_cycles;
	}

	double PerformanceStats::AverageIdleMs() const
	{
		if(m_cycles <= 0){
			return 0;
		}
		return m_idleTime_sum /  (double) m_cycles;
	}

	double PerformanceStats::AverageCycleMs() const
	{
		if(m_cycles <= 0){
			return 0;
		}
		return (m_workingTime_sum + m_idleTime_sum) / (double) m_cycles;
	}

	double PerformanceStats::AverageDutyCycle() const
	{
		if(0 == (m_workingTime_sum + m_idleTime_sum )){
			return 0;
		}
		return m_workingTime_sum / (m_workingTime_sum + m_idleTime_sum);
	}

	__int64 PerformanceStats::CycleCount() const
	{
		return m_cycles;
	}

}