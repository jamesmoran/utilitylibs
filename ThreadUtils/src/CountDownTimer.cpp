#include "CountDownTimer.h"

namespace threadutils{

CountDownTimer::CountDownTimer()
	: m_interval_ms(1000)
	, m_duration_ms(1000)
	, m_remaining_ms(1000)
	, m_runCount(-1)
{

}

CountDownTimer::~CountDownTimer()
{
	std::vector<boost::signals2::connection>::iterator iter;
	for(iter = m_eventConnections.begin(); iter != m_eventConnections.end(); iter++){
		iter->disconnect();
	}
	m_eventConnections.clear();
}

bool CountDownTimer::Start(int p_duration_ms, int p_interval_ms){
	if(Ready() && p_duration_ms > 0 && p_interval_ms > 0){
	
		m_interval_ms = p_interval_ms;
		m_duration_ms = p_duration_ms;
		m_remaining_ms = p_duration_ms;

		_thread.Resume();
		m_runCount = 0;
		return true;
	}
	return false;
}

bool CountDownTimer::Stop(){
	if(0 == m_runCount){
		m_runCount = 1;
		Kill(20);
		return true;
	}
	return false;
}

boost::signals2::connection CountDownTimer::RegisterFor_CountDownExpiredEvent(const CountDownEvent::slot_type& p_slot){
	boost::signals2::connection conn = m_event.connect(p_slot);
	m_eventConnections.push_back(conn);
	return conn;
}

void CountDownTimer::InitThread(){

}

void CountDownTimer::Run(){

	while(0 == _isDying){
		if(0 >= m_remaining_ms){
			m_event();
			Stop();
		}else{
			Sleep(m_interval_ms);
			m_remaining_ms -= m_interval_ms;
		}
	}
}

void CountDownTimer::FlushThread(){

}

int CountDownTimer::Interval() const{
	return m_interval_ms;
}

int CountDownTimer::Duration() const{
	return m_duration_ms;
}

int CountDownTimer::Remaining() const{
	return m_remaining_ms;
}

bool CountDownTimer::Ready(){
	return -1 == m_runCount;
}

}