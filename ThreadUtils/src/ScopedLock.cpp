#include "ScopedLock.h"

namespace threadutils
{
	ScopedLock::ScopedLock(CRITICAL_SECTION* SynchronizationObject)
		: pSection(SynchronizationObject)
	{
		Lock();
	}

	ScopedLock::~ScopedLock()
	{
		Unlock();
	}

	void ScopedLock::Lock()
	{
		EnterCriticalSection(pSection);
	}

	void ScopedLock::Unlock()
	{
		LeaveCriticalSection(pSection);
	}

}
