#include "MultimediaTimerWrapper.h"

namespace threadutils{

	void CALLBACK MultimediaTimerCallback(UINT wTImerID, UINT msg, DWORD_PTR dwUser, DWORD_PTR dw1, DWORD_PTR dw2){
		MultimediaTimer* caller = reinterpret_cast<MultimediaTimer*>(dwUser);
		caller->ExecuteNow();
	}

	MultimediaTimer::MultimediaTimer() :
		m_cycleCount(NULL),
		m_active(false)
	{
		InitializeCriticalSection(&m_eventSerializer);
		InitializeCriticalSection(&m_threadControlSerializer);
	}

	MultimediaTimer::~MultimediaTimer()
	{
		m_timerEvent.disconnect_all_slots();
		if (m_active && NULL != m_timerID) { Kill(); }
		DeleteCriticalSection(&m_eventSerializer);
		DeleteCriticalSection(&m_threadControlSerializer);
		CloseAllConnections();
	}

	bool MultimediaTimer::Start(UINT uDelay, UINT uResolution, bool repeat)
	{
		EnterCriticalSection(&m_threadControlSerializer);
		if(!m_active){
			m_timerID = timeSetEvent(uDelay, uResolution, MultimediaTimerCallback, (DWORD_PTR)this, (UINT)(repeat ? TIME_PERIODIC : TIME_ONESHOT));
			m_active = m_timerID != NULL;
		}
		LeaveCriticalSection(&m_threadControlSerializer);
		return m_active;
	}

	void MultimediaTimer::Kill()
	{
		EnterCriticalSection(&m_threadControlSerializer);
		if(m_active && NULL != m_timerID){
			MMRESULT result = timeKillEvent(m_timerID);
			if(TIMERR_NOERROR == result){
				m_timerID = NULL;
				m_active = false;
			}
			m_active = false;
		}
		LeaveCriticalSection(&m_threadControlSerializer);
	}

	int MultimediaTimer::GetCycleCount() const
	{
		return m_cycleCount;
	}

	void MultimediaTimer::ExecuteNow()
	{
		//Ensure that the timer Event is not signaled simultaneously by separate threads.
		EnterCriticalSection(&m_eventSerializer);
		m_timerEvent();
		m_cycleCount++;
		LeaveCriticalSection(&m_eventSerializer);
		
	}

	boost::signals2::connection MultimediaTimer::RegisterFor_TimerEvent( const MultimediaTimerEvent::slot_type& p_slot )
	{
		//you cannot connect to a signal while it's signalling, so protect the connect() call by making it mutually 
		//exclusive with the signal() call in ExecuteNow()
		EnterCriticalSection(&m_eventSerializer);
		boost::signals2::connection newConnection = m_timerEvent.connect(p_slot);
		m_timerEventConnections.push_back(newConnection);
		LeaveCriticalSection(&m_eventSerializer);
		return newConnection;
	}

	void MultimediaTimer::CloseAllConnections(){
		std::vector< boost::signals2::connection >::iterator iter;
		for(iter = m_timerEventConnections.begin(); iter != m_timerEventConnections.end(); iter++){
			iter->disconnect();
		}
		m_timerEventConnections.clear();
	}
}

