//
// high_res_timer.cpp: implementation of the high_res_timer class.
//
//////////////////////////////////////////////////////////////////////

#include <windows.h>
#include "winbase.h"
#include "HighResTimer.h"

namespace threadutils{

double high_res_timer::frequency = 0;


high_res_timer::high_res_timer()
{
	restart();
}


void high_res_timer::restart()
{
	if (frequency == 0)		// frequency value not yet set
	{
		LARGE_INTEGER freq;
		QueryPerformanceFrequency(&freq);
		frequency = (double)freq.QuadPart;
	}

	QueryPerformanceCounter(&start_counter);

	lap_start_counter.QuadPart = start_counter.QuadPart;
}


double high_res_timer::get_timestamp_secs()
{
	LARGE_INTEGER current_counter;
	QueryPerformanceCounter(&current_counter);

	if (frequency == 0)		// frequency value not yet set
	{
		LARGE_INTEGER freq;
		QueryPerformanceFrequency(&freq);
		frequency = (double)freq.QuadPart;
	}

	return (frequency == 0) ? 0 : current_counter.QuadPart/frequency;
}


double high_res_timer::get_lap_secs() const
{
	LARGE_INTEGER current_counter;
	QueryPerformanceCounter(&current_counter);
	double res = (frequency == 0) ? 0 : (current_counter.QuadPart-lap_start_counter.QuadPart)/frequency;
	lap_start_counter.QuadPart = current_counter.QuadPart;
	return res;
}


double high_res_timer::get_start_secs() const
{
	return (frequency == 0) ? 0 : start_counter.QuadPart/frequency;
}


double high_res_timer::get_delta_from_start_secs() const
{
	LARGE_INTEGER current_counter;
	QueryPerformanceCounter(&current_counter);
	double res = (frequency == 0) ? 0 : (current_counter.QuadPart-start_counter.QuadPart)/frequency;
	return res;
}



double high_res_timer::get_timestamp_msecs()
{
	return get_timestamp_secs() * 1e3;
}


double high_res_timer::get_lap_msecs() const
{
	return get_lap_secs() * 1e3;
}


double high_res_timer::get_start_msecs() const
{
	return get_start_secs() * 1e3;
}

double high_res_timer::get_delta_from_start_msecs() const
{
	return get_delta_from_start_secs() * 1e3;
}

}