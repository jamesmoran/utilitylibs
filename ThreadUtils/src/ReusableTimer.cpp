#include "ReusableTimer.h"
#include <iostream>

namespace threadutils{

ReusableTimer::ReusableTimer()
	: m_active(false)
	, m_durationMs(1000)
	, m_intervalMs(100)
	, m_remainingMs(1000)
	, m_repeat(false)
	, m_repeatCount(0)
	, m_threadStarted(false)
	, m_debugout(false)
	, m_threadStayAlive(true)
{
	m_controlEvent = CreateEvent(NULL,TRUE,FALSE,NULL);
}

ReusableTimer::~ReusableTimer()
{
	m_threadStayAlive = false;
	SetEvent(m_controlEvent);
	WaitForDeath(100);
	CloseHandle(m_controlEvent);
}

bool ReusableTimer::Start(int p_duration, int p_interval, bool repeat){
	if(p_duration > 0 && p_interval < p_duration){

		if(!m_threadStarted){
			_thread.Resume();
			if(m_debugout){
				std::cout << "RTIMER RESUME\n" << std::flush;
			}
			m_threadStarted = true;
		}

		m_durationMs = p_duration;
		m_intervalMs = p_interval;
		m_remainingMs = p_duration;
		m_repeat = repeat;
		m_repeatCount = 0;
		m_active = true;

		if(m_debugout){
			std::cout << "RTIMER Start("<<p_duration<<","<<p_interval<< "," <<repeat<<")\n" << std::flush;
		}

		SetEvent(m_controlEvent);
		return true;
	}
	return false;
}

bool ReusableTimer::Stop(){
	if(m_active){
		m_active = false;

		if(m_debugout){
			std::cout << "RTIMER Stop\n" << std::flush;
		}

		ResetEvent(m_controlEvent);
		return true;
	}
	return false;
}

int ReusableTimer::GetRemainingMs() const{
	return m_remainingMs;
}

int ReusableTimer::GetDurationMs() const{
	return m_durationMs;
}

int ReusableTimer::GetIntervalMs() const{
	return m_intervalMs;
}

int ReusableTimer::GetRepeatCount() const{
	return m_repeatCount;
}

bool ReusableTimer::IsRepeating() const{
	return m_repeat;
}

bool ReusableTimer::IsActive() const{
	return m_active;
}

void ReusableTimer::InitThread(){
	if(m_debugout){
		std::cout << "RTIMER INITTHREAD\n" << std::flush;
	}
}

void ReusableTimer::Run(){

	if(m_debugout){
		std::cout << "RTIMER RUN\n" << std::flush;
	}

	while(m_threadStayAlive){

		//control whether the timer is running or not.
		UINT result = WaitForSingleObject(m_controlEvent,INFINITE);

		if(m_debugout){
			std::cout << "RTIMER GO\n" << std::flush;
		}

		//check again. It may have been a long time since the while loop conditional was checked.
		if(m_threadStayAlive){

			DoOneCycle();
			m_repeatCount++;
			if(!m_repeat){

				if(m_debugout){
					std::cout << "RTIMER STOP\n" << std::flush;
				}

				//if we're not repeating, reset the event so that we can block again.
				ResetEvent(m_controlEvent);
			}else{

				if(m_debugout){
					std::cout << "RTIMER GOAGAIN\n" << std::flush;
				}

				m_active = true;
				m_remainingMs = m_durationMs;
			}
		}

	}
}


void ReusableTimer::DoOneCycle(){

	if(m_debugout){
		std::cout << "RTIMER CYCLE\n" << std::flush;
	}

	while(m_active){

		if(m_debugout){
			std::cout << "RTIMER SLEEP\n" << std::flush;
		}

		m_sleepTimer.restart();
		Sleep(m_intervalMs);

		if(m_debugout){
			std::cout << "RTIMER WAKE\n" << std::flush;
		}

		m_remainingMs -= static_cast<int>(m_sleepTimer.get_delta_from_start_msecs());

		if(0 >= m_remainingMs){
				m_active = false;

				if(m_debugout){
					std::cout << "RTIMER END\n" << std::flush;
				}

				m_endEvent();

		}else{

			if(m_debugout){
				std::cout << "RTIMER TICK\n" << std::flush;
			}

			m_tickEvent(m_remainingMs);
		}
	}
}

void ReusableTimer::FlushThread(){
	if(m_debugout){
		std::cout << "RTIMER FLUSH\n" << std::flush;
	}
}

boost::signals2::connection ReusableTimer::RegisterFor_TickEvent(const TickEvent::slot_type& p_slot){
	return m_tickEvent.connect(p_slot);
}

boost::signals2::connection ReusableTimer::RegisterFor_EndEvent(const EndEvent::slot_type& p_slot){
	return m_endEvent.connect(p_slot);
}

void ReusableTimer::SetDebugOutput( bool enableDebugOut )
{
	m_debugout = enableDebugOut;
}


NetworkTimer::NetworkTimer(const PingEvent::slot_type& p_slot, int seconds /*= 5*/)
:ReusableTimer()
, m_seconds(seconds)
{
	m_pingEvent.connect(p_slot);

	RegisterFor_EndEvent(boost::bind(&NetworkTimer::OnEndEvent, this));
}

NetworkTimer::~NetworkTimer()
{
	Stop();
}

void NetworkTimer::StartTimer()
{
	Start(m_seconds * 1000, 100, true);
}

boost::signals2::connection NetworkTimer::RegisterFor_ConnectionEvent(const ConnectionEvent::slot_type& p_slot)
{
	return m_connectionEvent.connect(p_slot);
}

void NetworkTimer::OnEndEvent()
{
	boost::optional<bool> connected = m_pingEvent();

	if (connected)
	{
		m_connectionEvent(connected.get());
	}
}

}
