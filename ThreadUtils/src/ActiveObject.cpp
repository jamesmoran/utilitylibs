#include "ActiveObject.h"
#include <iostream>
#include <assert.h>

namespace threadutils{


	Event::Event(bool p_manualReset, bool p_initialState)
		: m_eventHandle(NULL)
	{
		//create an unnamed event with no specific parameters
		m_eventHandle = ::CreateEvent(NULL,
			p_manualReset ? TRUE : FALSE,
			p_initialState ? TRUE : FALSE,
			NULL);

		assert(NULL != m_eventHandle);
	}

	Event::~Event(){
		assert(NULL != m_eventHandle);

		if(NULL != m_eventHandle){
			::CloseHandle(m_eventHandle);
		}
	}

	bool Event::SetEvent(){
		assert(NULL != m_eventHandle);
		return FALSE != ::SetEvent(m_eventHandle);
	}

	bool Event::ResetEvent(){
		assert(NULL != m_eventHandle);
		return FALSE != ::ResetEvent(m_eventHandle);
	}

	HANDLE Event::GetHandle(){
		return m_eventHandle;
	}

	Event::operator HANDLE(){
		return m_eventHandle;
	}


	// The constructor of the derived class
	// should call
	//    _thread.Resume ();
	// at the end of construction

	ActiveObject::ActiveObject ()
		: _isDying (0),
#pragma warning(disable: 4355) // 'this' used before initialized
		_thread (ThreadEntry, this)
#pragma warning(default: 4355)
	{
	}

	void ActiveObject::Kill (DWORD p_waitMilliseconds)
	{
		_isDying++;
		FlushThread ();
		// Let's make sure it's gone
		_thread.WaitForDeath (p_waitMilliseconds);
	}

	void ActiveObject::WaitForDeath(DWORD p_waitMilliseconds){
		_thread.WaitForDeath(p_waitMilliseconds);
	}

	DWORD WINAPI ActiveObject::ThreadEntry (void* pArg)
	{
		ActiveObject * pActive = (ActiveObject *) pArg;
		pActive->InitThread ();
		pActive->Run ();
		return 0;
	}
}