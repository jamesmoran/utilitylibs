#include "WorkerThread.h"

namespace threadutils{

	
		WorkerThread::WorkerThread(const TaskFunction& p_taskFunction)
			: m_doTaskEvent(false, false)
			, m_taskFunction(p_taskFunction)
			, m_active(false)
		{

		}

		WorkerThread::~WorkerThread(){
			m_doTaskEvent.SetEvent();
			Kill(100);
		}

		void WorkerThread::Start(){
			_thread.Resume();
		}

		void WorkerThread::RunTask(){
			m_doTaskEvent.SetEvent();
		}

		bool WorkerThread::IsActive() const{
			return m_active;
		}

		void WorkerThread::InitThread(){

		};

		void WorkerThread::Run(){

			while (!_isDying){
				m_active = false;
				WaitForSingleObject(m_doTaskEvent, INFINITE);
				if (_isDying){
					break;
				}

				m_active = true;

				if (m_taskFunction){
					m_taskFunction();
				}

			}

		};

		void WorkerThread::FlushThread(){

		};


		WorkerThread::WorkerThread(const WorkerThread& p_other)
			: m_doTaskEvent(false, false)
		{}

}