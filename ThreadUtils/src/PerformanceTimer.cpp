#include "PerformanceTimer.h"
#include <ScopedLock.h>

namespace threadutils{

	PerformanceTimer::PerformanceTimer()
		: m_idleTime(0)
		, m_workingTime(0)
	{
		::InitializeCriticalSection(&m_performanceStatsAccessSerializer);
	}

	PerformanceTimer::~PerformanceTimer()
	{
		::DeleteCriticalSection(&m_performanceStatsAccessSerializer);
	}

	PerformanceTimer::PerformanceTimer( const PerformanceTimer& p )
	{
		//this function intentionally does nothing
	}

	void PerformanceTimer::operator=( const PerformanceTimer& p )
	{
		//this function intentionally does nothing
	}

	void PerformanceTimer::WorkStart()
	{
		m_idleTime = m_cycleTimer.get_lap_msecs();
	}

	void PerformanceTimer::WorkEnd()
	{
		m_workingTime = m_cycleTimer.get_lap_msecs();

		ScopedLock lock(&m_performanceStatsAccessSerializer);
		m_performanceStats.AddCycle(m_workingTime,m_idleTime);
	}

	void PerformanceTimer::GetPerformanceStats( PerformanceStats& stats_out )
	{
		ScopedLock lock(&m_performanceStatsAccessSerializer);
		stats_out = m_performanceStats;
	}


	

}