#pragma once

#include <memory>
#include <map>
#include <vector>
#include "CoreDefines.h"

namespace datautils{

	/*	Class: DependencyGraphNode
		Node class contains supporting logic for the <DependencyGraph> class.
	*/
	class DependencyGraphNode{
	public:

		/*	Constructor: DependencyGraphNode

			Parameters:
				id - The (supposedly unique) integer id of this node.
		*/
		MMI_EXPORT DependencyGraphNode(int id);

		/*	Destructor: ~DependencyGraphNode
		*/
		MMI_EXPORT ~DependencyGraphNode();

		/*	Method: AddEdge
			Add an edge "from" this node "to" the specified destination node. The edge will
			be added unless it already exists.
			
			Parameters:
				node - A pointer to the destination node
		
			Return:
				bool - Returns true if the edge was added. Returns false if the edge
				already exists.
		*/
		MMI_EXPORT bool AddEdge(DependencyGraphNode* node);


		/*	Method: HasEdge
			Predicate that tests if this node has an edge to the specified destination
			node.

			Parameters:
				node - The destination node (pointer, not ID).
		
			Return:
				bool - Returns true if this node has an edge to the destination node.
		*/
		MMI_EXPORT bool HasEdge(DependencyGraphNode* node) const;

		/*	Method: HasEdge
			Predicate that tests if this node has an edge to the specified destination
			node.

			Parameters:
				id - The destination node integer ID.
		
			Return:
				bool - Returns true if this node has an edge to the destination node.
		*/
		MMI_EXPORT bool HasEdge(int id) const;


		/*	Method: RemoveEdge
			Removes an edge between this node and the node with the specified id.

			Parameters:
				id - The id of the destination node of the edge to remove.
		
			Return:
				bool - returns true if the edge existed and was removed. False otherwise.
		*/
		MMI_EXPORT bool RemoveEdge(int id);

		/*	Method: ID
		
			Return:
				int - Returns the integer ID of the current node.
		*/
		MMI_EXPORT int ID() const;

		/*	Method: BeginEdges
			
			Return:
				std::map<int,DependencyGraphNode*>::const_iterator - returns a const iterator at
				the beginning of the collection of edges.
		*/
		MMI_EXPORT std::map<int,DependencyGraphNode*>::const_iterator BeginEdges() const;

		/*	Method: EndEdges
			
			Return:
				std::map<int,DependencyGraphNode*>::const_iterator - returns a const iterator at
				the end of the collection of edges.
		*/
		MMI_EXPORT std::map<int,DependencyGraphNode*>::const_iterator EndEdges() const;

	protected:	
		int m_id;
		std::map<int,DependencyGraphNode*> m_edges;
	};

	/*	Class: DependencyGraph
		The Dependency Graph represents the dependency relationships between entities as a 
		directed graph. It can compute an ordering that satisfies all dependency relationships
		if no cycles exist in the graph.

		URL:
			source - http://www.electricmonk.nl/docs/dependency_resolving_algorithm/dependency_resolving_algorithm.html

	*/
	class DependencyGraph{
	public:

		/*	Constructor: DependencyGraph
		*/
		MMI_EXPORT DependencyGraph();

		/*	Destructor: ~DependencyGraph
		*/
		MMI_EXPORT ~DependencyGraph();


		/*	Method: AddNode
			Adds a node to the graph. Node IDs must be unique so insertion will be
			rejected if the ID already exists in the graph.

			Parameters:
				id - The id of the new node.
		
			Return:
				bool - Returns true if the ID was unique and the new node was inserted.
				Returns false if the ID was not unique, in which case the graph will not
				be altered.
		*/
		MMI_EXPORT bool AddNode(int id);

		/*	Method: HasNode
			Predicate that checks to see if a given node ID exists in the graph already.

			Parameters:
				id - The node ID.
		
			Return:
				bool - returns true if a node of that ID already exists, false otherwise.
		*/
		MMI_EXPORT bool HasNode(int id) const;

		/*	Method: AddEdge
			Adds an edge FROM src_id TO dst_id. If this edge already exists, returns false 
			and nothing happens.

			Parameters:
				src_id - The id of the node from which the edge emanates. This is the entity that 
				depends on dst_id.
				dst_id - The id of the node to which to the edge arrives. This is the entity that
				is depended on by src_id.
		
			Return:
				bool - returns true if this is a new edge that does not already exist (new edge inserted),
				returns false otherwise.
		*/
		MMI_EXPORT bool AddEdge(int src_id, int dst_id);

		/*	Method: HasEdge
			Predicate that checks to see if an edge from src_id to dst_id exists. (i.e. if src_id
			depends on dst_id).

			Parameters:
			src_id - The id of the node from which the edge emanates. This is the entity that 
			depends on dst_id.
			dst_id - The id of the node to which to the edge arrives. This is the entity that
			is depended on by src_id.
		
			Return:
				bool - Return true if the edge exists, false otherwise.
		*/
		MMI_EXPORT bool HasEdge(int src_id, int dst_id) const;

		/*	Method: RemoveNode
			Removes a node AND all edges terminating at that node. Integrity of the graph
			should be maintained.

			Parameters:
				id - The id of the node to remove.
		
			Return:
				bool - Returns true if the node existed and was removed. False otherwise.
		*/
		MMI_EXPORT bool RemoveNode(int id);

		/*	Method: RemoveEdge
			Removes an edge from the graph.
			
			Parameters:
				src_id - The id of the source node of the edge.
				dst_id - The id of the destination node of the edge.
		
			Return:
				bool - Returns true if the edge existed and was removed, false otherwise.
		*/
		MMI_EXPORT bool RemoveEdge(int src_id, int dst_id);


		/*	Method: GetOrdering
			Computes an order in of the ids that resolves all dependencies, starting at a specified node
			 i.e: if node A is dependent on by another node B then A will precede B in the ordering.
		
			Parameters:
				id - The id of the node from which to start. Note that this is not necessarily an exhaustive
				traversal of the graph.

			Return:
				std::vector<int> - Returns an ordered vector of node ids in the resolved order.

			Throws:
				std::exception - Throws an exception if a cycle is encountered - this invalidates the
				algorithm so resolution cannot continue.
		*/
		MMI_EXPORT std::vector<int> GetOrdering(int id) const;
		
		/*	Method: Clear
			Clears the contents of the dependency graph
		*/
		MMI_EXPORT void Clear();

	protected:
		static void dep_resolve(DependencyGraphNode* node, std::vector<int>& resolved, std::vector<int>& seen);
		
		static bool vec_contains(const std::vector<int>& vec, int val);

		std::map<int,DependencyGraphNode*> m_nodes;
	};
}