#pragma once

#include <sstream>
#include "CoreDefines.h"

#define DEFAULT_STYLE_ID ""

namespace datautils{

enum Config_Control_Type
{
	control_on_off, control_multi_select, control_button, control_numeric_up_down, control_undefined
};

// Class: ConfigMetadata
// This class is a serializable (to string) struct containing the necessary values to describe a configuration element
class ConfigMetadata{
public:

	static const char DATA_SEPARATOR;

	MMI_EXPORT static std::string MakeConfigMetadata(unsigned int description_resource_id
		, unsigned int group_description_resource_id
		, Config_Control_Type control_type
		, unsigned int privilege
		, const std::string& style_id
		, const std::string& params);

	MMI_EXPORT ConfigMetadata();

	MMI_EXPORT ConfigMetadata(unsigned int description_resource_id
		, unsigned int group_description_resource_id
		, Config_Control_Type control_type
		, unsigned int privilege
		, const std::string& style_id=""
		, const std::string& params="");

	MMI_EXPORT ConfigMetadata(const std::string& metaString);

	MMI_EXPORT std::string to_str() const;
	MMI_EXPORT bool from_str(const std::string& str);

	unsigned int m_description_resource_id;
	unsigned int m_group_description_resource_id;
	Config_Control_Type m_control_type;
	unsigned int m_privilege;
	std::string m_style_id;
	std::string m_params;
};

}