#pragma once

#include <string>
#include "Tuple.h"
#include "ConfigMetadata.h"

namespace datautils{

// Class: ConfigStruct
// A thin wrapper around fully generic Tuple class, the ConfigStruct provides a small amount
// of additional functionality including storage of an unique filename.
class ConfigStruct : public Tuple
{
public:

	MMI_EXPORT ConfigStruct(const std::string& p_filename);

	MMI_EXPORT std::string GetName() const;

protected:
	std::string m_filename;

};

}