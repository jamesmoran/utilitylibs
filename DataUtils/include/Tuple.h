#pragma once

#include <string>
#include <sstream>
#include <map>
#include <StringUtils.h>
#include "CoreDefines.h"

// Namespace: datautils
// This namespace contains functions and classes devoted to data storage, formatting and serialization.
namespace datautils
{

// Macro: INIT_PARAM
// This macro is used in Tuple subclass constructors to properly initialize
#define INIT_PARAM(param_table, param_name, param_value, param_metadata) param_name(param_table, #param_name, param_value, param_metadata)

	// Class: Tuple
	// The Tuple class is a base class intended to be subclassed for the purpose of creating iterable structs.
	// A normal struct contains always-present, named member variables, but it not iterable or introspective.
	// A std::map contains key-value pairs, and is iterable, so you can ask it what keys it contains. However,
	// you cannot be sure whether a given key exists without polling the data structure, and access requires
	// a key-based lookup which is optimized but potentially significant.
	//
	// The Tuple combines these two into a hybrid. A tuple subclass may implement as many param_type objects,
	// and initializes them using the INIT_PARAM macro in the subclass constructor. These param_types support
	// implicit casting to their templatized type, and support the assignment operator. Therefore, you can create
	// a param_type<int> foo in "MyTuple", and from then on you can say MyTuple.foo = 8, or int y = MyTuple.foo.
	// Internally, the values are stored twice: once in the std::map, and once in each param_type. Externally,
	// Client code can rely on each param_type to be present and can treat them as normal variables. Client
	// code may also iterate through the Tuple using a common param_base class which implements a string-based
	// type-agnostic interface for file I/O and generic operations.
	//
	class Tuple
	{
	public:
		// Struct: param_base
		// This abstract base "class" serves to define the interface common to all param_types. 
		// All subclasses of the param_base class must support conversion to/from a string object.
		struct param_base
		{
		public:

			param_base(){}
			param_base(const std::string& meta)
				: m_metaData(meta)
			{
				
			}

			virtual std::string to_str() const = 0;
			virtual void from_str(const std::string& str) = 0;

			const std::string& GetMetaData() const{
				return m_metaData;
			}
			void SetMetaData(const std::string& str){
				m_metaData = str;
			}

		protected:
			std::string m_metaData;
		};

		// Typedef: ParamTable
		// The ParamTable is the type of the map that connects parameter names to parameters,
		// permitting lookup and iteration.
		typedef std::map<std::string, param_base*> ParamTable;

	public:

		// Struct: param_type<T> 
		// This templatized subclass of param_base permits compile-time definition of a parameter with an arbitrary
		// type T. A param_type<int> will contain an integer, and a param_type<Quux> will contain an object of type
		// Quux. So long as the << and >> stringstream operators are defined for both type int and type Quux, everything 
		// will compile.
		template<typename T>
		struct param_type : param_base
		{
			// Constructor: 
			// the param_type constructor takes a reference to the ParamTable, and it adds itself to the
			// table ensuring proper lookup. The INIT_PARAM macro ensures this happens properly.
			// Parameters:
			//  Table - The ParamTable to which this param_type should add itself.
			//  name - The name of the parameter, which will be its key in the param_table. The INIT_PARAM macro uses the
			//    # macro operator to convert the variable name into a string literal, permitting the name param to be
			//    automatically identical to the name of the param_type object.
			//  param_value - The initial value of the parameter.
			param_type(ParamTable& Table, const std::string& name, T param_value, const std::string& metadata = std::string())
				: param_base(metadata)
				, value_(param_value)
			{
				Table[name] = this;
			}

			// Method: operator=
			// Assignment operator, sets the internally stored value type T in the param_type object. Externally, this means
			// that you can assign a type T object to the param_type variable seamlessly.
			param_type& operator=(T param_value)
			{
				value_ = param_value;
				return *this;
			}

			// Method: to_str
			// This method overrides the pure virtual function defined in the param_base class with a generic conversion to
			// string that relies on the ostreamstream class. If an ostringstream::operator<< for type T is defined, this function will
			// work. A class for which this operator cannot or is not defined must subclass param_type and override this function
			// with type-specific code.
			// A typical << operator signature for a user-defined class Baz would look like this:
			// 
			// class Baz{
			//    [...]
			//
			//    friend std::ostream& operator << (std::ostream& stream, const Baz& b);
			//
			//    [...]
			// };
			//
			virtual std::string to_str() const
			{
				std::ostringstream ss;
				ss << value_;
				return ss.str();
			}

			// Method: from_str
			// This method overrides the pure virtual function defined in the param_base class with a generic conversion from
			// string that relies on the istreamstream class. If an istringstream::operator>> for type T is defined, this function will
			// work. A class for which this operator cannot or is not defined must subclass param_type and override this function
			// with type-specific code.
			// A typical >> operator signature for a user-defined class Baz would look like this:
			// 
			// class Baz{
			//    [...]
			//
			//    friend std::istream& operator >> (std::istream& stream, const Baz& b);
			//
			//    [...]
			// };
			//
			virtual void from_str(const std::string& str)
			{
				std::istringstream ss(str);
				ss >> value_;
			}

			// Method: operator()
			// This is an implicit cast operator to converts the param_type<T> into an object type T
			// This exists so you can do the following:
			//
			// param_type<int> myIntegerParameter;
			// int i = myIntegerParameter;
			//
			// The conversion is seamless so long as the correct type is specified, or there is an implicit cast
			// possible from type T to the type being assigned.
			operator const T() const { return value_; }

		private:

			// Variable: 
			// The actual stored value of type T
			T value_;
		};

	public:

		// Constructor: Tuple
		// The base class constructor does nothing.
		MMI_EXPORT Tuple();

		// Destructor: ~Tuple
		// The base class destructor does nothing
		MMI_EXPORT ~Tuple();

		// Method: Read
		// This method supports conversion from a file. The file must contain the param_type names, followed by their values as converted to string.
		MMI_EXPORT virtual bool Read(const std::string& FilePath);

		// Method: Write
		// This method supports conversion to a file. The file will contain the param_type names, followed by their values as converted to string.
		MMI_EXPORT virtual bool Write(const std::string& FilePath) const;

		// Method: FirstEntry
		// Returns: Returns a const_iterator to the first element in the Parameter Table, permitting iteration. 
		MMI_EXPORT ParamTable::const_iterator FirstEntry() const;

		// Method: LastEntry
		// Returns: Returns a const_iterator to the last element in the Parameter Table, permitting iteration. 
		MMI_EXPORT ParamTable::const_iterator LastEntry() const;

		// Method: SetValue
		// Sets the value of a parameter within the Tuple, specified by a const_iterator. The value is
		// provided as a string.
		// Parameters:
		//  param - A ParamTable::const_iterator referring to the value to be modified.
		//  new_value - A string that can be converted to the appropriate value for the param in question.
		MMI_EXPORT bool SetValue(ParamTable::const_iterator& param, const std::string& new_value);

		// Method: SetValue
		// Sets the value of a parameter within the Tuple, specified by parameter name. The value is
		// provided as a string.
		// Parameters:
		//  param_name - The string name of a parameter to be modified.
		//  new_value - A string that can be converted to the appropriate value for the param in question.
		MMI_EXPORT bool SetValue(const std::string& param_name, const std::string& new_value);

		/*	Method: GetValue
			Get the value of a particular tuple element, as string.

			Parameters:
				param_name - The name of the element
		
			Return:
				std::string - The value of the element's to_str() function. An empty string is returned
				if param_name does not correspond to a valid field of the Tuple.
		*/
		MMI_EXPORT std::string GetValue(const std::string& param_name) const;

		/*	Method: GetValue
			Get the value of a particular tuple element, as string.

			Parameters:
				param_name - The name of the parameter.
				val_out - A reference to the string to be filled with the value of the parameter. If param_name
				is invalid, this string is left unaltered.
		
			Return:
				bool - Returns true of the param_name is valid and a value is written; False otherwise.
		*/
		MMI_EXPORT bool GetValue(const std::string& param_name, std::string& val_out) const;

		// Method: ContainsKey
		// Predicate, checks to see if param_name is a valid parameter within the Tuple.
		MMI_EXPORT bool ContainsKey(const std::string& param_name) const;

			/*	Method: ToJSON
			Create a JSON property tree representation of the Tuple
	
			Return:
				std::string - Returns a JSON string.
		*/
		MMI_EXPORT std::string ToJSON() const;

		/*	Method: FromJSON
			Attempts to parse the provided JSON data to extract new values for the fields of the Tuple.

			Parameters:
				p_jsonString - The JSON string describing the Tuple. Values whose JSON identifiers
				exactly match the field names of the Tuple will be loaded.
				p_requireAll - If this flag is true, the operation will return false unless all
				Tuple fields were filled by values from the JSON string.

			Return:
				bool - Returns true on success. If p_requireAll is false, this simply means that
				the JSON string was well-formed. If p_requireAll is true, then a return value of true 
				indicates that the JSON was well-formed, and contained a property corresponding to 
				each field of the Tuple, and that each of those JSON properties was successfully
				parsed.
		*/
		MMI_EXPORT bool FromJSON(const std::string& p_jsonString, bool p_requireAll) const;


	private:
		// Constructor:
		// Does nothing: this constructor is private and should not be used.
		// A Tuple is not copyable
		Tuple(const Tuple& t);

		// Method: operator=
		// Does nothing: this method is private and should not be used.
		// A Tuple is not copyable
		void operator=(const Tuple& t);

	protected:

		// Variable: Parameters
		// This parameter table keeps track of the mapping between string names and the param_type<T> objects.
		ParamTable Parameters;
	};

	// Method: from_str
	// This method is pre-implemented here for the param_type<bool> templated type, to ensure that
	// the desired format is observed. Bool parameters are represented as text by the words "ON"
	// (true) and "OFF" (false)
	template <>
	void Tuple::param_type<bool>::from_str(const std::string& str)
	{
		std::string lower_str = stringutils::StringManip::ToLower(str);
		value_ = ( lower_str == "on" || lower_str == "true" || str == "1");
	}

	// Method: to_str
	// This method is pre-implemented here for the param_type<bool> templated type, to ensure that
	// the desired format is observed. Bool parameters are represented as text by the words "ON"
	// (true) and "OFF" (false)
	template <>
	std::string Tuple::param_type<bool>::to_str() const
	{
		return value_ ? "ON" : "OFF";
	}

	// Method: to_str
	// This method is pre-implemented here for the param_type<std::string> templated type, to ensure that
	// this trivial conversion is performed correctly.
	template <>
	void Tuple::param_type<std::string>::from_str(const std::string& str)
	{
		value_ = str;
	}

	// Method: from_str
	// This method is pre-implemented here for the param_type<std::string> templated type, to ensure that
	// this trivial conversion is performed correctly.
	template <>
	std::string Tuple::param_type<std::string>::to_str() const
	{
		return value_;
	}
}

