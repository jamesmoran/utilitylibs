#include "ConfigMetadata.h"
#include <StringUtils.h>

namespace datautils{

const char ConfigMetadata::DATA_SEPARATOR = ';';

std::string ConfigMetadata::MakeConfigMetadata(unsigned int description_resource_id
	, unsigned int group_description_resource_id
	, Config_Control_Type control_type
	, unsigned int privilege
	, const std::string& style_id
	, const std::string& params){
	return ConfigMetadata(description_resource_id,group_description_resource_id,control_type,privilege,style_id,params).to_str(); 
}

ConfigMetadata::ConfigMetadata()
	: m_description_resource_id(0)
	, m_group_description_resource_id(0)
	, m_control_type(control_undefined)
	, m_privilege(0)
	, m_style_id("")
	, m_params("")
{

}

ConfigMetadata::ConfigMetadata(unsigned int description_resource_id
	, unsigned int group_description_resource_id
	, Config_Control_Type control_type
	, unsigned int privilege
	, const std::string& style_id
	, const std::string& params)
	: m_description_resource_id(description_resource_id)
	, m_group_description_resource_id(group_description_resource_id)
	, m_control_type(control_type)
	, m_privilege(privilege)
	, m_style_id(style_id)
	, m_params(params)
{

}

ConfigMetadata::ConfigMetadata(const std::string& metaString)
	: m_description_resource_id(0)
	, m_group_description_resource_id(0)
	, m_control_type(control_undefined)
	, m_privilege(0)
	, m_style_id("")
	, m_params("")
{
	from_str(metaString);
}

std::string ConfigMetadata::to_str() const{
	std::stringstream meta;
	meta << m_description_resource_id << DATA_SEPARATOR;
	meta << m_group_description_resource_id << DATA_SEPARATOR;
	meta << (int) m_control_type << DATA_SEPARATOR;
	meta << (int) m_privilege << DATA_SEPARATOR;
	meta << m_style_id << DATA_SEPARATOR;
	meta << m_params << DATA_SEPARATOR;
	return meta.str();
}

bool ConfigMetadata::from_str(const std::string& str){
	
	std::vector<std::string> tokens;
	if(stringutils::Split(str, DATA_SEPARATOR, tokens) < 6){
		return false;
	}

	m_description_resource_id = stringutils::StringManip::lexical_cast<unsigned int>(tokens[0]);
	m_group_description_resource_id = stringutils::StringManip::lexical_cast<unsigned int>(tokens[1]);
	m_control_type = (Config_Control_Type) stringutils::StringManip::lexical_cast<int>(tokens[2]);
	m_privilege = stringutils::StringManip::lexical_cast<unsigned int>(tokens[3]);
	m_style_id = tokens[4];
	m_params = tokens[5];

	return true;
}

}
