#include "ConfigStruct.h"

namespace datautils{

ConfigStruct::ConfigStruct(const std::string& p_filename) 
	: m_filename(p_filename)
{
	
}

std::string ConfigStruct::GetName() const{
	return m_filename;
}

}