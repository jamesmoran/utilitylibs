#include <vector>
#include <fstream>

#include "Tuple.h"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>

namespace pt = boost::property_tree;

namespace datautils
{
	Tuple::Tuple()
	{

	}

	Tuple::~Tuple(void)
	{
	
	}

	bool Tuple::Read( const std::string& FilePath )
	{
		std::ifstream ifs;
		ifs.open(FilePath.c_str());
		if (!ifs.is_open())
			return false;
		while (!ifs.eof())
		{
			std::string line;
			std::getline(ifs, line);
			std::vector<std::string> tokens = stringutils::StringManip::Split(line, ' ');
			if (tokens.size() < 2)
				continue;
			std::string param_name = stringutils::StringManip::Trim(tokens[0]);
			std::string param_val_str = stringutils::StringManip::Trim(line.substr(param_name.size(),line.size()-param_name.size()));

			ParamTable::iterator it = Parameters.find(param_name);
			if (it != Parameters.end())
				it->second->from_str(param_val_str);
		}
		ifs.close();
		return true;
	}

	bool Tuple::Write( const std::string& FilePath ) const
	{
		std::ofstream ofs;
		ofs.open(FilePath.c_str());
		if (!ofs.is_open())
			return false;
		ParamTable::const_iterator it = Parameters.begin();
		for (; it != Parameters.end() ; ++it)
			ofs << it->first << " " << it->second->to_str() << std::endl;
		ofs.close();
		return true;
	}


	Tuple::ParamTable::const_iterator Tuple::FirstEntry() const{
		return Parameters.begin();
	}

	Tuple::ParamTable::const_iterator Tuple::LastEntry() const{
		return Parameters.end();
	}

	bool Tuple::SetValue(Tuple::ParamTable::const_iterator& param, const std::string& new_value){
		return SetValue(param->first,new_value);
	}

	bool Tuple::SetValue(const std::string& param_name, const std::string& new_value){
		bool success = false;
		if(ContainsKey(param_name)){
			Parameters[param_name]->from_str(new_value);
			success = true;
		}
		return success;
	}

	std::string Tuple::GetValue(const std::string& param_name) const
	{
		ParamTable::const_iterator& iter = Parameters.find(param_name);
		if(Parameters.end() != iter){
			return iter->second->to_str();
		}
		return "";
	}

	bool Tuple::GetValue(const std::string& param_name, std::string& val_out ) const
	{
		bool success = false;
		ParamTable::const_iterator& iter = Parameters.find(param_name);
		if(Parameters.end() != iter){
			val_out = iter->second->to_str();
			success = true;
		}
		return success;
	}

	bool Tuple::ContainsKey(const std::string& param_name) const{
		return Parameters.end() != Parameters.find(param_name);
	}

	std::string Tuple::ToJSON() const
	{
		pt::ptree properties;

		auto it = Parameters.begin();
		for (; it != Parameters.end() ; ++it){
			properties.put(it->first,it->second->to_str());
		}

		std::stringstream ss;
		pt::write_json(ss,properties);

		return ss.str();
	}

	bool Tuple::FromJSON( const std::string& p_jsonString, bool p_requireAll ) const
	{
		int missing = 0;
		bool success = true;

		pt::ptree properties;
		try{
			pt::read_json(std::stringstream(p_jsonString),properties);
			success = true;
		}catch(std::exception pe){
			std::string msg = pe.what();
			success = false;
		}

		if(success){
			
			//if requireAll, check first that everything is there.
			auto it = Parameters.begin();
			for (; it != Parameters.end() ; ++it){
				try{
					std::string val = properties.get<std::string>(it->first);
				}catch(std::exception ve){
					missing++;
				}
			}

			//apply the available changes (if everything required is present, or we don't require all params).
			if(0 == missing || !p_requireAll){

				it = Parameters.begin();
				for (; it != Parameters.end() ; ++it){
					try{
						std::string val = properties.get<std::string>(it->first);
						it->second->from_str(val);
					}catch(std::exception ve){

					}
				}
			}

		}

		bool foundAll = !p_requireAll || 0 == missing;

		return success && foundAll;
	}

	Tuple::Tuple(const Tuple& t){
		// Does nothing: this constructor is private and should not be used.
	}

	void Tuple::operator=(const Tuple& t){
		// Does nothing: this method is private and should not be used.
	}

	

	

}
