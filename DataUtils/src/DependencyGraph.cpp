#include "DependencyGraph.h"

#include <iostream>
#include "StringUtils.h"

namespace datautils{
	



	DependencyGraphNode::DependencyGraphNode( int id )
		: m_id(id)
	{

	}

	DependencyGraphNode::~DependencyGraphNode()
	{

	}

	bool DependencyGraphNode::AddEdge( DependencyGraphNode* node)
	{
		if(NULL == node){
			return false;
		}

		if(!HasEdge(node)){
			m_edges[node->ID()] = node;
			return true;
		}
		return false;
	}

	bool DependencyGraphNode::HasEdge( DependencyGraphNode* node) const
	{
		if(NULL == node){
			return false;
		}
		
		return HasEdge(node->ID());
	}

	bool DependencyGraphNode::HasEdge( int id ) const
	{
		auto iter = m_edges.find(id);
		if(iter != m_edges.end()){
			return true;
		}
		return false;
	}

	int DependencyGraphNode::ID() const
	{
		return m_id;
	}

	std::map<int,DependencyGraphNode*>::const_iterator DependencyGraphNode::BeginEdges() const
	{
		return m_edges.begin();
	}

	std::map<int,DependencyGraphNode*>::const_iterator DependencyGraphNode::EndEdges() const
	{
		return m_edges.end();
	}

	bool DependencyGraphNode::RemoveEdge( int id )
	{
		if(!HasEdge(id)){
			return false;
		}

		m_edges.erase(id);
		return true;
	}

	//-------------------------------------

	DependencyGraph::DependencyGraph()
	{

	}

	DependencyGraph::~DependencyGraph()
	{
		Clear();
	}

	bool DependencyGraph::AddNode( int id )
	{
		if(!HasNode(id)){
			m_nodes[id] = new DependencyGraphNode(id);
			return true;
		}
		return false;
	}

	bool DependencyGraph::HasNode( int id ) const
	{
		auto iter = m_nodes.find(id);
		return iter != m_nodes.end();
	}

	bool DependencyGraph::AddEdge( int src_id, int dst_id )
	{
		if(!HasNode(src_id) || ! HasNode(dst_id)){
			return false;
		}

		DependencyGraphNode* src_node = m_nodes[src_id];
		DependencyGraphNode* dst_node = m_nodes[dst_id];

		return src_node->AddEdge(dst_node);
	}

	bool DependencyGraph::HasEdge( int src_id, int dst_id ) const
	{
		if(!HasNode(src_id) || ! HasNode(dst_id)){
			return false;
		}

		auto iter = m_nodes.find(src_id);
		return iter->second->HasEdge(dst_id);
	}

	std::vector<int> DependencyGraph::GetOrdering(int id) const
	{
		std::vector<int> resolved;
		std::vector<int> seen;

		if(HasNode(id)){
			dep_resolve(m_nodes.find(id)->second, resolved, seen);
		}
		//for(int i = 0; i < (int) resolved.size(); i++){
		//	std::cout << resolved[i] << " ";
		//}
		//std::cout << std::endl;

		return resolved;
	}

	void DependencyGraph::dep_resolve(DependencyGraphNode* node, std::vector<int>& resolved, std::vector<int>& seen)
	{
		if(NULL != node){
			//std::cout << node->ID() << std::endl;
			seen.push_back(node->ID());
			auto iter = node->BeginEdges();
			for(; iter != node->EndEdges(); iter++){
				if( !vec_contains(resolved,iter->first) ){
					if( vec_contains(seen,iter->first) ){
						throw std::exception( std::string("Circular reference detected " + stringutils::str(node->ID()) + " -> " + stringutils::str(iter->first)).c_str() );
					}else{
						dep_resolve(iter->second,resolved,seen);
					}
				}
			}
			resolved.push_back(node->ID());
		}
	}

	bool DependencyGraph::vec_contains( const std::vector<int>& vec, int val )
	{
		auto iter = vec.begin();
		for(; iter != vec.end(); iter++){
			if(val == *iter){
				return true;
			}
		}
		return false;
	}

	bool DependencyGraph::RemoveNode( int id )
	{
		if(!HasNode(id)){ 
			return false;
		}

		for(auto iter = m_nodes.begin(); iter != m_nodes.end(); iter++){
			iter->second->RemoveEdge(id);
		}

		DependencyGraphNode* node = m_nodes[id];
		delete node;
		m_nodes.erase(id);
		return true;
	}

	bool DependencyGraph::RemoveEdge( int src_id, int dst_id )
	{
		if(HasNode(src_id)){
			if(HasEdge(src_id,dst_id)){
				return m_nodes[src_id]->RemoveEdge(dst_id);
			}
		}
		return false;
	}

	void DependencyGraph::Clear()
	{
		auto iter = m_nodes.begin();
		for(; iter != m_nodes.end(); iter++){
			delete iter->second;
		}
		m_nodes.clear();
	}
}
