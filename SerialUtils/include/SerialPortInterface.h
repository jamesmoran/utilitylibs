#pragma once

#include <windows.h>
#include <string>
#include "CoreDefines.h"


#define SERIAL_INTERFACE_MAXNAMELEN 64

namespace serialutils{

	///@brief Simple serial port I/O class based on windows ReadFile/WriteFile functions.
	///
	///Serial I/O class implements synchronous calls to serial (COM) port. I/O is done using char arrays.
	///Class handles initialization and destruction of "file" handle associated with COM port connection.
	class SerialPortInterface{
	public:
		///enumeration specifies semantics of status codes.
		enum SerialPortStatus{
			SERIAL_STATUS_NULL,
			SERIAL_STATUS_UNINITIALIZED,
			SERIAL_STATUS_OPEN,
			SERIAL_STATUS_CLOSED,
			SERIAL_STATUS_ERROR,
			SERIAL_STATUS_ERROR_UNABLE_TO_OPEN_PORT,
			SERIAL_STATUS_ERROR_SET_TIMEOUT_FAILED,
			SERIAL_STATUS_ERROR_UNABLE_TO_GET_PARAMS,
			SERIAL_STATUS_ERROR_UNABLE_TO_SET_PARAMS
		};

		///Basic constructor. Does no setup.
		///@warning You must call Open() function to initialize an actual serial connection if you use
		///this constructor, but this can be deferred to a later point in the code if necessary.
		MMI_EXPORT SerialPortInterface();

		///Constructor immediately attempts to open the specified COM port.
		///@param p_portNumber The number of the COM port to open.
		///@param p_baudrate The baud rate at which to initialize the connection parameters. 
		///Valid values are: CBR_110, CBR_300, CBR_600, CBR_1200, CBR_2400, CBR_4800, CBR_9600, 
		///CBR_14400, CBR_19200, CBR_38400, CBR_56000, CBR_57600, CBR_115200, CBR_128000, CBR_256000.
		MMI_EXPORT SerialPortInterface(UINT p_portNumber, DWORD p_baudRate = CBR_9600);
		
		///Destructor, ensures proper closing of the port if it is still open, and destroys other dynamically-allocated members.
		MMI_EXPORT ~SerialPortInterface();

		///Opens a new connection on the specified COM port. Regardless of success, it will close any existing connection.
		///@param p_portNumber The number of the COM port to open.
		///@param p_baudrate The baud rate at which to initialize the connection parameters. 
		///Valid values are: CBR_110, CBR_300, CBR_600, CBR_1200, CBR_2400, CBR_4800, CBR_9600, 
		///CBR_14400, CBR_19200, CBR_38400, CBR_56000, CBR_57600, CBR_115200, CBR_128000, CBR_256000.
		///@return Returns true on success, false otherwise. Use GetStatus() function for detailed error information on failure.
		MMI_EXPORT bool Open(UINT p_portNumber, DWORD p_baudRate = CBR_9600);

		///Closes the current connection, if any exists.
		MMI_EXPORT void Close();

		///@return Returns true when a valllid connection is open.
		MMI_EXPORT bool IsOpen() const;

		///@return Returns the most recent status code set by the last operation.
		MMI_EXPORT SerialPortInterface::SerialPortStatus GetStatus() const;

		///@brief Reads the specified number of bytes or fewer from the COM port. 
		///
		///Synchronous, blocking call returns on one of the following conditions:
		///1) The requested number of bytes is read. 2) The read timeout interval elapses without receiving any data. 
		///3) A write operation completes on the write end of the pipe. 4) an error occurs.
		///@param p_byteArr The byte array to be filled. Must be of length >= p_maxBytes.
		///@param p_maxBytes The maximum number of bytes to read.
		///@return Returns the total number of bytes read. If this is less than p_maxBytes, then a timeout or error has occurred.
		MMI_EXPORT int Read(char* p_byteArr, int p_maxBytes);

		///Writes the specified number of bytes to the COM port.
		///
		///@param p_byteArr The byte array to be sent. Must be of length >= p_length
		///@param p_length The number of bytes to send.
		///@return Returns the total number of bytes sent. If this is less than p_length, an error has occurred.
		MMI_EXPORT int Write(char* p_byteArr, int p_length);

		///Clears the incoming (RX) buffer.
		///@return Returns true on success. False would indicate that the port is in an error state.
		MMI_EXPORT bool PurgeReadBuffer();

		///Set the timeout lengths, thus determining the timing behaviour of the system.
		///
		///This is a wrapper on the SetCommTimeouts() function. 
		///@param ReadIntervalTimeout The maximum time allowed to elapse between the arrival of two bytes on the communications line,
		///in milliseconds. During a ReadFile operation, the time period begins when the first byte is received. If the interval 
		///between the arrival of any two bytes exceeds this amount, the ReadFile operation is completed and any buffered data is 
		///returned. A value of zero indicates that interval time-outs are not used. A value of MAXDWORD, combined with zero values 
		///for both the ReadTotalTimeoutConstant and ReadTotalTimeoutMultiplier members, specifies that the read operation is to 
		///return immediately with the bytes that have already been received, even if no bytes have been received.
		///@param ReadTotalTimeoutConstant A constant used to calculate the total time-out period for read operations, in milliseconds. 
		///For each read operation, this value is added to the product of the ReadTotalTimeoutMultiplier member and the requested 
		///number of bytes. A value of zero for both the ReadTotalTimeoutMultiplier and ReadTotalTimeoutConstant members indicates 
		///that total time-outs are not used for read operations.
		///@param ReadTotalTimeoutMultiplier The multiplier used to calculate the total time-out period for read operations, in 
		///milliseconds. For each read operation, this value is multiplied by the requested number of bytes to be read.
		///@param WriteTotalTimeoutConstant A constant used to calculate the total time-out period for write operations, in 
		///milliseconds. For each write operation, this value is added to the product of the WriteTotalTimeoutMultiplier member 
		///and the number of bytes to be written. A value of zero for both the WriteTotalTimeoutMultiplier and 
		///WriteTotalTimeoutConstant members indicates that total time-outs are not used for write operations.
		///@param WriteTotalTimeoutMultiplier The multiplier used to calculate the total time-out period for write operations, in 
		///milliseconds. For each write operation, this value is multiplied by the number of bytes to be written.
		///@return Returns the status of the serial port following the attempted operation.s
		MMI_EXPORT SerialPortInterface::SerialPortStatus SetTimeouts(DWORD ReadIntervalTimeout= 50, DWORD ReadTotalTimeoutConstant = 50, DWORD ReadTotalTimeoutMultiplier = 10, DWORD WriteTotalTimeoutConstant = 50, DWORD WriteTotalTimeoutMultiplier = 10);
		
		///Set the communication parameters. Wrapper around SetCommState function, utilising DCB parameter
		///struct.
		///@param BaudRate Valid values are: CBR_110, CBR_300, CBR_600, CBR_1200, CBR_2400, CBR_4800, CBR_9600, CBR_14400, CBR_19200, CBR_38400, CBR_56000, CBR_57600, CBR_115200, CBR_128000, CBR_256000.
		///@param ByteSize The number of "data bits". Default is 8.
		///@param Parity Parity system to use to validate packets. Valid values are: NOPARITY, ODDPARITY, EVENPARITY, MARKPARITY, SPACEPARITY.
		///@param StopBits Stop bit convention. valid values are: ONESTOPBIT, ONE5STOPBITS, TWOSTOPBITS.
		///@return Returns current state of serial port interface after attempting to set these parameters.
		MMI_EXPORT SerialPortInterface::SerialPortStatus SetCommunicationParameters(DWORD BaudRate = CBR_9600, BYTE ByteSize = 8, BYTE Parity = NOPARITY, BYTE StopBits = ONESTOPBIT);

	protected:
	
		///Initialization function sets up the COM port
		///@param p_portNumber The number of the COM port to open.
		///@param p_baudrate The baud rate at which to initialize the connection parameters. 
		///Valid values are: CBR_110, CBR_300, CBR_600, CBR_1200, CBR_2400, CBR_4800, CBR_9600, 
		///CBR_14400, CBR_19200, CBR_38400, CBR_56000, CBR_57600, CBR_115200, CBR_128000, CBR_256000.
		///@return Returns current state of serial port interface after attempting to set these parameters.
		SerialPortInterface::SerialPortStatus InitSerialPort(UINT p_portNumber, DWORD p_baudrate);

		///Sets the serial port timeouts based on the provided struct (pointer)
		///@return Returns current state of serial port interface after attempting to set these parameters.
		SerialPortInterface::SerialPortStatus SetTimeouts(COMMTIMEOUTS* ct);

		///Sets the serial port parameters based on the provided struct (pointer)
		///@return Returns current state of serial port interface after attempting to set these parameters.
		SerialPortInterface::SerialPortStatus SetCommunicationParameters(DCB* dcb);

		///Access serialization critical section
		CRITICAL_SECTION m_accessSerializer;

		///The current port number
		UINT m_portNumber;

		//The current baud rate
		int m_baudRate;

		///The current status
		SerialPortInterface::SerialPortStatus m_status;

		//-------------------------------------------

		///The handle of the "file" representing the COM port
		HANDLE m_hComm;	

		///C string representing the name of the COM port "file"
		std::wstring m_portName;

		///Timeouts struct
		COMMTIMEOUTS m_timeouts;

		///Communication parameters
		DCB m_commParams;

		//Get various information about the connection
		COMSTAT status;
		DWORD errors;
	};

}
