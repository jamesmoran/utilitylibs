#pragma once

#include <cstdint>
#include "SerialPortInterface.h"

#define MMISERIALPACKET_MAX_SIZE 32
#define MMISERIALPACKET_START_SIGNATURE 0xfdfd

// Namespace: serialutils
// This namespace contains utility functions pertaining to the transmission, reception, parsing
// and construction for serial communication messages.
namespace serialutils{

	// Function: Serialize
	// Writes the 16-bit unsigned integer p_value to the specified buffer as a pair of bytes.
	// Returns: 
	// Returns the number of bytes written. Returns 0 if there is an error, 2 on success.
	MMI_EXPORT size_t Serialize(uint16_t p_value, char* p_destBuffer, size_t p_destBufferSize);

	// Function: Deserialize 
	// Reads a 16-bit unsigned integer from the first two of bytes in the specified buffer.
	// Returns: 
	// Returns the number of bytes read. 0 on failure, 2 on success.
	MMI_EXPORT size_t Deserialize(char* p_srcBuffer, size_t p_srcBufferSize, uint16_t& p_value_out);

	// Class: MMISerialPacket 
	// A struct representing a packet in the MMISerialProtocol
	class MMISerialPacket{
	public:
		
		MMI_EXPORT MMISerialPacket(byte p_ID, byte* p_payload, size_t p_payloadSize);
		MMI_EXPORT MMISerialPacket(const MMISerialPacket& packet);

		MMI_EXPORT byte GetID() const;
		MMI_EXPORT byte GetSize() const;
		MMI_EXPORT uint16_t GetCheckSum() const;

		MMI_EXPORT size_t GetSerializedLength() const;

		MMI_EXPORT void SetID(byte p_ID);
		MMI_EXPORT size_t SetPayload(const byte* p_payload, size_t p_payloadSize);
		MMI_EXPORT 
		MMI_EXPORT size_t Serialize(char* p_destBuffer, size_t p_destBufferSize) const;

		MMI_EXPORT bool operator==(const MMISerialPacket& p) const;
	
	protected:
		uint16_t ComputeCheckSum() const;

		uint16_t Checksum;
		byte ID;
		byte Size;
		byte Payload[MMISERIALPACKET_MAX_SIZE];
	};

	// Class: MMISerialProtocol 
	// A is a generic packet-based protocol that provides the ability to 
	// transmit up to 32 bytes in a data packet as a simple byte array. The intended usage is
	// to create a subclass of this protocol specific to the client program's usage.
	class MMISerialProtocol{
		
	public:
		MMI_EXPORT MMISerialProtocol(int p_portNumber, DWORD p_baudRate = CBR_9600);
		MMI_EXPORT ~MMISerialProtocol();

		MMI_EXPORT bool Ready() const;
		MMI_EXPORT SerialPortInterface::SerialPortStatus GetInterfaceStatus() const;

		MMI_EXPORT size_t SendPacket(const MMISerialPacket* p_packet);
		MMI_EXPORT size_t SendPacket(byte p_ID, byte* p_payload, byte p_payloadSize);

	protected:
		
		SerialPortInterface* m_interface;

	};



}
