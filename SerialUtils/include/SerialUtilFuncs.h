#pragma once

#include <vector>
#include "AutoHModule.h"
#include "AutoHandle.h"
#include "CoreDefines.h"
#include "AutoHeapAlloc.h"
#include <SetupAPI.h>
#include <sstream>
#include <Windows.h>
#include <tchar.h>

#ifndef GUID_DEVINTERFACE_COMPORT
	DEFINE_GUID(GUID_DEVINTERFACE_COMPORT, 0x86E0D1E0L, 0x8089, 0x11D0, 0x9C, 0xE4, 0x08, 0x00, 0x3E, 0x30, 0x1F, 0x73);
#endif

namespace serialutils{
	typedef HKEY(__stdcall SETUPDIOPENDEVREGKEY)(HDEVINFO, PSP_DEVINFO_DATA, DWORD, DWORD, DWORD, REGSAM);
	typedef BOOL(__stdcall SETUPDICLASSGUIDSFROMNAME)(LPCTSTR, LPGUID, DWORD, PDWORD);
	typedef BOOL(__stdcall SETUPDIDESTROYDEVICEINFOLIST)(HDEVINFO);
	typedef BOOL(__stdcall SETUPDIENUMDEVICEINFO)(HDEVINFO, DWORD, PSP_DEVINFO_DATA);
	typedef HDEVINFO(__stdcall SETUPDIGETCLASSDEVS)(LPGUID, LPCTSTR, HWND, DWORD);
	typedef BOOL(__stdcall SETUPDIGETDEVICEREGISTRYPROPERTY)(HDEVINFO, PSP_DEVINFO_DATA, DWORD, PDWORD, PBYTE, DWORD, PDWORD);


	MMI_EXPORT HMODULE LoadLibraryFromSystem32(LPCTSTR lpFileName);

	MMI_EXPORT BOOL RegQueryValueString(HKEY kKey, LPCTSTR lpValueName, LPTSTR& pszValue);

	MMI_EXPORT BOOL IsNumeric(LPCSTR pszString, BOOL bIgnoreColon);

	MMI_EXPORT BOOL QueryRegistryPortName(HKEY hDeviceKey, int& nPort);

	MMI_EXPORT BOOL GetPortsUsingSetupAPI2(std::vector<UINT>& ports, std::vector<std::string>& friendlyNames);

	MMI_EXPORT bool StrContains(const std::string& str1, const std::string& str2);

	MMI_EXPORT int FindPortWithName(std::vector<UINT>& ports, std::vector<std::string>& friendlyNames, const std::string& portNametoFind);
}