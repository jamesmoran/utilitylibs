#include "MMISerialProtocol.h"

namespace serialutils{

	size_t Serialize(uint16_t p_value, char* p_destBuffer, size_t p_destBufferSize){
		if(p_destBufferSize < 2){
			return 0;
		}

		p_destBuffer[0] = (p_value & 0xFF);			//low byte
		p_destBuffer[1] = ((p_value >> 8) & 0xFF);	//high byte
		return 2;
	}

	size_t Deserialize(char* p_srcBuffer, size_t p_srcBufferSize, uint16_t& p_value_out){
		if(p_srcBufferSize < 2){
			return 0;
		}

		p_value_out = (uint16_t) p_srcBuffer[1];	//read the high byte
		p_value_out = (p_value_out << 8);	//shift by 8 bits to elevate the high byte to the high position
		p_value_out += (uint16_t) p_srcBuffer[0];	//add the lowByte
		return 2;
	}

	//-----------------------------------------------------------

	MMISerialPacket::MMISerialPacket(byte p_ID, byte* p_payload, size_t p_payloadSize) :
		Checksum(0),
		ID(0),
		Size(0)
	{
		SetID(p_ID);
		SetPayload(p_payload,p_payloadSize);
	}

	MMISerialPacket::MMISerialPacket(const MMISerialPacket& packet) :
		Checksum(0),
		ID(0),
		Size(0)
	{
		SetID(packet.ID);
		SetPayload(packet.Payload,packet.Size);
	}

	byte MMISerialPacket::GetID() const{
		return ID;
	}

	byte MMISerialPacket::GetSize() const{
		return Size;
	}

	uint16_t MMISerialPacket::GetCheckSum() const{
		return Checksum;
	}

	size_t MMISerialPacket::GetSerializedLength() const{
		size_t total = 0;

		total += sizeof(uint16_t);	//Signature
		total += sizeof(uint16_t);	//Checksum
		total += sizeof(byte);		//ID
		total += sizeof(byte);		//Size
		total += Size;				//Payload
		return total;
	}

	void MMISerialPacket::SetID(byte p_ID){
		ID = p_ID;
	}

	size_t MMISerialPacket::SetPayload(const byte* p_payload, size_t p_payloadSize){
		size_t maxSize = p_payloadSize < MMISERIALPACKET_MAX_SIZE ? p_payloadSize : MMISERIALPACKET_MAX_SIZE;

		memcpy(Payload,p_payload,maxSize);

		Size = (byte)maxSize;
		Checksum = ComputeCheckSum();

		return maxSize;
	}	

	size_t MMISerialPacket::Serialize(char* p_destBuffer, size_t p_destBufferSize) const{
		size_t requiredSize = GetSerializedLength();

		if(p_destBufferSize < (int) requiredSize){
			return 0;
		}

		size_t currentIndex = 0;

		//must specify namespace because otherwise the Serialize funciton will not be resolved outside the class' scope.
		currentIndex += serialutils::Serialize((uint16_t) MMISERIALPACKET_START_SIGNATURE,(char*) ( p_destBuffer + currentIndex), (size_t) (p_destBufferSize-currentIndex) );
		currentIndex += serialutils::Serialize(Checksum, p_destBuffer + currentIndex, p_destBufferSize-currentIndex);
		
		p_destBuffer[currentIndex] = (char) ID;
		currentIndex++;
		
		p_destBuffer[currentIndex] = (char) Size;
		currentIndex++;

		memcpy(p_destBuffer + currentIndex,Payload,(size_t) Size);	
		currentIndex += (size_t) Size;

		return currentIndex;
	}

	bool MMISerialPacket::operator==(const MMISerialPacket& p) const{
		return ID == p.ID && Size == p.Size && (0 == memcmp(Payload,p.Payload,(size_t) Size));
	}

	uint16_t MMISerialPacket::ComputeCheckSum() const{
		
		uint16_t sum = 0;
		for(int i = 0; i < (int) Size; i++){
			sum += Payload[i];
		}
		return sum;
	}

	//-----------------------------------------------------------

	MMISerialProtocol::MMISerialProtocol(int p_portNumber, DWORD p_baudRate) :
		m_interface(NULL)
	{
		m_interface = new SerialPortInterface(p_portNumber,p_baudRate);
	}

	MMISerialProtocol::~MMISerialProtocol(){
		delete (m_interface);
	}

	bool MMISerialProtocol::Ready() const{
		return NULL != m_interface && m_interface->IsOpen();
	}

	SerialPortInterface::SerialPortStatus MMISerialProtocol::GetInterfaceStatus() const{
		if(NULL == m_interface){
			return SerialPortInterface::SERIAL_STATUS_NULL;
		}
		return m_interface->GetStatus();
	}

	size_t MMISerialProtocol::SendPacket(const MMISerialPacket* p_packet){
		if(NULL == p_packet){
			return 0;
		}

		size_t packetLength = p_packet->GetSerializedLength();
		char* packetBuffer = new char[packetLength];

		p_packet->Serialize(packetBuffer,packetLength);

		size_t bytesWritten = (size_t) m_interface->Write(packetBuffer,(int)packetLength);

		delete packetBuffer;
		return bytesWritten;
	}

	size_t MMISerialProtocol::SendPacket(byte p_ID, byte* p_payload, byte p_payloadSize){
	
		MMISerialPacket packet(p_ID,p_payload,p_payloadSize);

		return SendPacket(&packet);
	}

}
