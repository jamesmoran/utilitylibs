#include "SerialPortInterface.h"
#include <sstream>

namespace serialutils{

SerialPortInterface::SerialPortInterface(){
	InitializeCriticalSection(&m_accessSerializer);
	m_status = SERIAL_STATUS_UNINITIALIZED;
	m_hComm = NULL;
}

SerialPortInterface::SerialPortInterface(UINT p_portNumber, DWORD p_baudRate){
	InitializeCriticalSection(&m_accessSerializer);
	m_status = SERIAL_STATUS_UNINITIALIZED;
	InitSerialPort(p_portNumber,p_baudRate);
	m_hComm = NULL;
}

SerialPortInterface::~SerialPortInterface(){
	Close();
	DeleteCriticalSection(&m_accessSerializer);
}

bool SerialPortInterface::Open(UINT p_portNumber, DWORD p_baudRate){
	Close();
	InitSerialPort(p_portNumber,p_baudRate);
	return IsOpen();
}

void SerialPortInterface::Close(){
	
	EnterCriticalSection(&m_accessSerializer);
	
	if (m_hComm != INVALID_HANDLE_VALUE){

		CloseHandle (m_hComm);
		m_hComm = NULL;
		
	}
	m_status = SERIAL_STATUS_CLOSED;
	LeaveCriticalSection(&m_accessSerializer);
}

bool SerialPortInterface::IsOpen() const{	
	bool isOpen = m_hComm != INVALID_HANDLE_VALUE && SERIAL_STATUS_OPEN == m_status;
	return isOpen;
}

SerialPortInterface::SerialPortStatus SerialPortInterface::GetStatus() const{
	return m_status;
}

int SerialPortInterface::Read(char* p_byteArr, int p_maxBytes){
	DWORD dwBytesRead = 0;
	
	EnterCriticalSection(&m_accessSerializer);
	ClearCommError(m_hComm, &errors, &status);

	if(m_hComm != INVALID_HANDLE_VALUE)
	{
		ReadFile(m_hComm, p_byteArr, p_maxBytes, &dwBytesRead,NULL);	//NULL in overlapped means this call is blocking
	}
	LeaveCriticalSection(&m_accessSerializer);

	return (int) dwBytesRead;
}

int SerialPortInterface::Write(char* p_byteArr, int p_length){
	DWORD dwSBytesWrite = 0;

	EnterCriticalSection(&m_accessSerializer);
	if(m_hComm != INVALID_HANDLE_VALUE)
	{
		WriteFile(m_hComm,p_byteArr,p_length,&dwSBytesWrite,NULL);	//NULL in overlapped means this call is blocking
	}
	LeaveCriticalSection(&m_accessSerializer);

	return dwSBytesWrite;
}

bool SerialPortInterface::PurgeReadBuffer(){
	EnterCriticalSection(&m_accessSerializer);
	bool success = (0 != PurgeComm(m_hComm, PURGE_RXCLEAR | PURGE_TXCLEAR));
	LeaveCriticalSection(&m_accessSerializer);
	return success;
}

SerialPortInterface::SerialPortStatus SerialPortInterface::SetTimeouts(DWORD ReadIntervalTimeout, DWORD ReadTotalTimeoutConstant, DWORD ReadTotalTimeoutMultiplier, DWORD WriteTotalTimeoutConstant, DWORD WriteTotalTimeoutMultiplier){
	m_timeouts.ReadIntervalTimeout= ReadIntervalTimeout; 
	m_timeouts.ReadTotalTimeoutConstant = ReadTotalTimeoutConstant;
	m_timeouts.ReadTotalTimeoutMultiplier = ReadTotalTimeoutMultiplier;
	m_timeouts.WriteTotalTimeoutConstant = WriteTotalTimeoutConstant;
	m_timeouts.WriteTotalTimeoutMultiplier = WriteTotalTimeoutMultiplier;
	return SetTimeouts(&m_timeouts);
}

SerialPortInterface::SerialPortStatus SerialPortInterface::SetCommunicationParameters(DWORD BaudRate, BYTE ByteSize, BYTE Parity, BYTE StopBits){
	
	m_commParams.fOutxCtsFlow	 = FALSE;
	m_commParams.fRtsControl     = RTS_CONTROL_DISABLE;
	m_commParams.fDtrControl     = DTR_CONTROL_ENABLE;
	m_commParams.fOutxDsrFlow    = FALSE;
	m_commParams.fOutX           = FALSE;
	m_commParams.fInX            = FALSE;

	m_commParams.BaudRate		 = BaudRate;
	m_commParams.ByteSize        = ByteSize;
	m_commParams.Parity          = Parity;
	m_commParams.StopBits        = StopBits;

	return SetCommunicationParameters(&m_commParams);
}

//protected

SerialPortInterface::SerialPortStatus SerialPortInterface::SetTimeouts(COMMTIMEOUTS* ct){
	EnterCriticalSection(&m_accessSerializer);
	bool success = (0!= SetCommTimeouts (m_hComm, ct));
	LeaveCriticalSection(&m_accessSerializer);

	if (!success)
	{
		Close();
		m_status = SERIAL_STATUS_ERROR_SET_TIMEOUT_FAILED;
	}
	return m_status;
}

SerialPortInterface::SerialPortStatus SerialPortInterface::SetCommunicationParameters(DCB* dcb){
	EnterCriticalSection(&m_accessSerializer);
	bool success = (0!= SetCommState(m_hComm, &m_commParams));
	LeaveCriticalSection(&m_accessSerializer);
	if(!success)
	{
		Close();
		m_status = SERIAL_STATUS_ERROR_UNABLE_TO_SET_PARAMS;
	}
	return m_status;
}




SerialPortInterface::SerialPortStatus SerialPortInterface::InitSerialPort(UINT p_portNumber, DWORD p_baudrate){

	Close();


	m_portNumber = p_portNumber;
	
	//create the name string
	std::wstringstream wss;
	if (m_portNumber > 9)
	{
		wss << "\\\\.\\";
	}
	wss << "COM" << m_portNumber;
	m_portName = wss.str();

	EnterCriticalSection(&m_accessSerializer);
	//create the "File" to represent the port
	m_hComm= CreateFileW(m_portName.c_str(),GENERIC_READ | GENERIC_WRITE, 0, 0, OPEN_EXISTING,0,NULL);
	
	
	if ( m_hComm == INVALID_HANDLE_VALUE)
	{
		CloseHandle (m_hComm);
		m_status = SERIAL_STATUS_ERROR_UNABLE_TO_OPEN_PORT;
	}
	
	LeaveCriticalSection(&m_accessSerializer);

	if ( m_hComm != INVALID_HANDLE_VALUE){
		m_status = SERIAL_STATUS_OPEN;
		
		//get the initial port state to initialize the parameter struct
		m_commParams = { 0 };
		m_commParams.DCBlength = sizeof (DCB);
		if (!GetCommState(m_hComm, &m_commParams))
		{
			m_status = SERIAL_STATUS_ERROR_UNABLE_TO_GET_PARAMS;
		}
		//set the default parameters in the parameter struct, except for baud rate, which is user-specified.
		SetCommunicationParameters(p_baudrate);
		PurgeReadBuffer();

		//set the default timeouts
		SetTimeouts();
	}
	return m_status;
}

}

