#include "SerialUtilFuncs.h"

namespace serialutils{


	HMODULE LoadLibraryFromSystem32(LPCTSTR lpFileName)
	{
		//Get the Windows System32 directory
		TCHAR szFullPath[_MAX_PATH];
		szFullPath[0] = _T('\0');
		if (GetSystemDirectory(szFullPath, _countof(szFullPath)) == 0)
			return NULL;

		//Setup the full path and delegate to LoadLibrary    
#pragma warning(suppress: 6102) //There is a bug with the SAL annotation of GetSystemDirectory in the Windows 8.1 SDK
		_tcscat_s(szFullPath, _countof(szFullPath), _T("\\"));
		_tcscat_s(szFullPath, _countof(szFullPath), lpFileName);
		return LoadLibrary(szFullPath);
	}

	BOOL RegQueryValueString(HKEY kKey, LPCTSTR lpValueName, LPTSTR& pszValue)
	{
		//Initialize the output parameter
		pszValue = NULL;

		//First query for the size of the registry value 
		DWORD dwType = 0;
		DWORD dwDataSize = 0;
		LONG nError = RegQueryValueEx(kKey, lpValueName, NULL, &dwType, NULL, &dwDataSize);
		if (nError != ERROR_SUCCESS)
		{
			SetLastError(nError);
			return FALSE;
		}

		//Ensure the value is a string
		if (dwType != REG_SZ)
		{
			SetLastError(ERROR_INVALID_DATA);
			return FALSE;
		}

		//Allocate enough bytes for the return value
		DWORD dwAllocatedSize = dwDataSize + sizeof(TCHAR); //+sizeof(TCHAR) is to allow us to NULL terminate the data if it is not null terminated in the registry
		pszValue = reinterpret_cast<LPTSTR>(LocalAlloc(LMEM_FIXED, dwAllocatedSize));
		if (pszValue == NULL)
			return FALSE;

		//Recall RegQueryValueEx to return the data
		pszValue[0] = _T('\0');
		DWORD dwReturnedSize = dwAllocatedSize;
		nError = RegQueryValueEx(kKey, lpValueName, NULL, &dwType, reinterpret_cast<LPBYTE>(pszValue), &dwReturnedSize);
		if (nError != ERROR_SUCCESS)
		{
			LocalFree(pszValue);
			pszValue = NULL;
			SetLastError(nError);
			return FALSE;
		}

		//Handle the case where the data just returned is the same size as the allocated size. This could occur where the data
		//has been updated in the registry with a non null terminator between the two calls to ReqQueryValueEx above. Rather than
		//return a potentially non-null terminated block of data, just fail the method call
		if (dwReturnedSize >= dwAllocatedSize)
		{
			SetLastError(ERROR_INVALID_DATA);
			return FALSE;
		}

		//NULL terminate the data if it was not returned NULL terminated because it is not stored null terminated in the registry
		if (pszValue[dwReturnedSize / sizeof(TCHAR)-1] != _T('\0'))
			pszValue[dwReturnedSize / sizeof(TCHAR)] = _T('\0');

		return TRUE;
	}

	BOOL IsNumeric(LPCSTR pszString, BOOL bIgnoreColon)
	{
		size_t nLen = strlen(pszString);
		if (nLen == 0)
			return FALSE;

		//What will be the return value from this function (assume the best)
		BOOL bNumeric = TRUE;

		for (size_t i = 0; i < nLen && bNumeric; i++)
		{
			bNumeric = (isdigit(static_cast<int>(pszString[i])) != 0);
			if (bIgnoreColon && (pszString[i] == ':'))
				bNumeric = TRUE;
		}

		return bNumeric;
	}

	BOOL QueryRegistryPortName(HKEY hDeviceKey, int& nPort)
	{
		//What will be the return value from the method (assume the worst)
		BOOL bAdded = FALSE;

		//Read in the name of the port
		LPTSTR pszPortName = NULL;
		if (RegQueryValueString(hDeviceKey, _T("PortName"), pszPortName))
		{
			//If it looks like "COMX" then
			//add it to the array which will be returned
			size_t nLen = _tcslen(pszPortName);
			if (nLen > 3)
			{
				if ((_tcsnicmp(pszPortName, _T("COM"), 3) == 0) && IsNumeric((pszPortName + 3), FALSE))
				{
					//Work out the port number
					nPort = _ttoi(pszPortName + 3);

					bAdded = TRUE;
				}
			}
			LocalFree(pszPortName);
		}

		return bAdded;
	}

	BOOL GetPortsUsingSetupAPI2(std::vector<UINT>& ports, std::vector<std::string>& friendlyNames)
	{
		//Make sure we clear out any elements which may already be in the array(s)
		ports.clear();
		friendlyNames.clear();


		//Get the function pointers to "SetupDiGetClassDevs", "SetupDiGetClassDevs", "SetupDiEnumDeviceInfo", "SetupDiOpenDevRegKey" 
		//and "SetupDiDestroyDeviceInfoList" in setupapi.dll
		CAutoHModule setupAPI(LoadLibraryFromSystem32(_T("SETUPAPI.DLL")));
		if (setupAPI == NULL)
			return FALSE;

		SETUPDIOPENDEVREGKEY* lpfnSETUPDIOPENDEVREGKEY = reinterpret_cast<SETUPDIOPENDEVREGKEY*>(GetProcAddress(setupAPI, "SetupDiOpenDevRegKey"));
#if defined _UNICODE
		SETUPDICLASSGUIDSFROMNAME* lpfnSETUPDICLASSGUIDSFROMNAME = reinterpret_cast<SETUPDICLASSGUIDSFROMNAME*>(GetProcAddress(setupAPI, "SetupDiClassGuidsFromNameW"));
		SETUPDIGETCLASSDEVS* lpfnSETUPDIGETCLASSDEVS = reinterpret_cast<SETUPDIGETCLASSDEVS*>(GetProcAddress(setupAPI, "SetupDiGetClassDevsW"));
		SETUPDIGETDEVICEREGISTRYPROPERTY* lpfnSETUPDIGETDEVICEREGISTRYPROPERTY = reinterpret_cast<SETUPDIGETDEVICEREGISTRYPROPERTY*>(GetProcAddress(setupAPI, "SetupDiGetDeviceRegistryPropertyW"));
#else
		SETUPDICLASSGUIDSFROMNAME* lpfnSETUPDICLASSGUIDSFROMNAME = reinterpret_cast<SETUPDICLASSGUIDSFROMNAME*>(GetProcAddress(setupAPI, "SetupDiClassGuidsFromNameA"));
		SETUPDIGETCLASSDEVS* lpfnSETUPDIGETCLASSDEVS = reinterpret_cast<SETUPDIGETCLASSDEVS*>(GetProcAddress(setupAPI, "SetupDiGetClassDevsA"));
		SETUPDIGETDEVICEREGISTRYPROPERTY* lpfnSETUPDIGETDEVICEREGISTRYPROPERTY = reinterpret_cast<SETUPDIGETDEVICEREGISTRYPROPERTY*>(GetProcAddress(setupAPI, "SetupDiGetDeviceRegistryPropertyA"));
#endif
		SETUPDIDESTROYDEVICEINFOLIST* lpfnSETUPDIDESTROYDEVICEINFOLIST = reinterpret_cast<SETUPDIDESTROYDEVICEINFOLIST*>(GetProcAddress(setupAPI, "SetupDiDestroyDeviceInfoList"));
		SETUPDIENUMDEVICEINFO* lpfnSETUPDIENUMDEVICEINFO = reinterpret_cast<SETUPDIENUMDEVICEINFO*>(GetProcAddress(setupAPI, "SetupDiEnumDeviceInfo"));

		if ((lpfnSETUPDIOPENDEVREGKEY == NULL) || (lpfnSETUPDICLASSGUIDSFROMNAME == NULL) || (lpfnSETUPDIDESTROYDEVICEINFOLIST == NULL) ||
			(lpfnSETUPDIENUMDEVICEINFO == NULL) || (lpfnSETUPDIGETCLASSDEVS == NULL) || (lpfnSETUPDIGETDEVICEREGISTRYPROPERTY == NULL))
		{
			//Set the error to report
			setupAPI.m_dwError = ERROR_CALL_NOT_IMPLEMENTED;

			return FALSE;
		}

		//First need to convert the name "Ports" to a GUID using SetupDiClassGuidsFromName
		DWORD dwGuids = 0;
		lpfnSETUPDICLASSGUIDSFROMNAME(_T("Ports"), NULL, 0, &dwGuids);
		if (dwGuids == 0)
		{
			//Set the error to report
			setupAPI.m_dwError = GetLastError();

			return FALSE;
		}

		//Allocate the needed memory
		CAutoHeapAlloc guids;
		if (!guids.Allocate(dwGuids * sizeof(GUID)))
		{
			//Set the error to report
			setupAPI.m_dwError = ERROR_OUTOFMEMORY;

			return FALSE;
		}

		//Call the function again
		GUID* pGuids = static_cast<GUID*>(guids.m_pData);
		if (!lpfnSETUPDICLASSGUIDSFROMNAME(_T("Ports"), pGuids, dwGuids, &dwGuids))
		{
			//Set the error to report
			setupAPI.m_dwError = GetLastError();

			return FALSE;
		}

		//Now create a "device information set" which is required to enumerate all the ports
		HDEVINFO hDevInfoSet = lpfnSETUPDIGETCLASSDEVS(pGuids, NULL, NULL, DIGCF_PRESENT);
		if (hDevInfoSet == INVALID_HANDLE_VALUE)
		{
			//Set the error to report
			setupAPI.m_dwError = GetLastError();

			return FALSE;
		}

		//Finally do the enumeration
		BOOL bMoreItems = TRUE;
		int nIndex = 0;
		SP_DEVINFO_DATA devInfo;
		while (bMoreItems)
		{
			//Enumerate the current device
			devInfo.cbSize = sizeof(SP_DEVINFO_DATA);
			bMoreItems = lpfnSETUPDIENUMDEVICEINFO(hDevInfoSet, nIndex, &devInfo);
			if (bMoreItems)
			{
				//Did we find a serial port for this device
				BOOL bAdded = FALSE;

				//Get the registry key which stores the ports settings
				HKEY hDeviceKey = lpfnSETUPDIOPENDEVREGKEY(hDevInfoSet, &devInfo, DICS_FLAG_GLOBAL, 0, DIREG_DEV, KEY_QUERY_VALUE);
				if (hDeviceKey != INVALID_HANDLE_VALUE)
				{
					int nPort = 0;
					if (QueryRegistryPortName(hDeviceKey, nPort))
					{
						ports.push_back(nPort);
						bAdded = TRUE;
					}

					//Close the key now that we are finished with it
					RegCloseKey(hDeviceKey);
				}

				//If the port was a serial port, then also try to get its friendly name
				if (bAdded)
				{
					TCHAR szFriendlyName[1024];
					szFriendlyName[0] = _T('\0');
					DWORD dwSize = sizeof(szFriendlyName);
					DWORD dwType = 0;
					if (lpfnSETUPDIGETDEVICEREGISTRYPROPERTY(hDevInfoSet, &devInfo, SPDRP_DEVICEDESC, &dwType, reinterpret_cast<PBYTE>(szFriendlyName), dwSize, &dwSize) && (dwType == REG_SZ))
					{
						friendlyNames.push_back(szFriendlyName);
					}
					else
					{
						friendlyNames.push_back(_T(""));

					}
				}
			}

			++nIndex;
		}

		//Free up the "device information set" now that we are finished with it
		lpfnSETUPDIDESTROYDEVICEINFOLIST(hDevInfoSet);

		//Return the success indicator
		return TRUE;
	}

	bool StrContains(const std::string& str1, const std::string& str2)
	{
		size_t ret = str1.find(str2);
		return ret >= 0 && ret <= (str1.size() - str2.size());
	}

	int FindPortWithName(std::vector<UINT>& ports, std::vector<std::string>& friendlyNames, const std::string& portNametoFind)
	{
		if (ports.size() != friendlyNames.size())
		{
			return -1;
		}

		for (size_t j = 0; j < ports.size(); j++)
		{
			if (StrContains(friendlyNames[j], portNametoFind))
			{
				return ports[j];
			}
		}

		return -1;
	}

}
