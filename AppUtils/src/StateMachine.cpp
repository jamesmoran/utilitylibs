#include "StateMachine.h"

namespace apputils{

	StateMachine::StateMachine():
		m_currState(NULL)
	{

	}

	StateMachine::~StateMachine()
	{
		DestroyAllStates();
	}


	bool StateMachine::AddState(const std::string& p_name){
		
		if(p_name.length() > 0 && !StateExists(p_name)){
			State* newState = new State(p_name);
		
			m_states[p_name] = newState;

			return true;
		}
		return false;
	}

	bool StateMachine::AddTransition(const std::string& p_name, const std::string& p_srcState, const std::string& p_dstState){
		if(p_name.length() <= 0){
			return false;
		}

		State* srcState = GetState(p_srcState);
		State* dstState = GetState(p_dstState);

		if(NULL == srcState || NULL == dstState){
			return false;
		}		

		if(srcState->TransitionExists(p_name)){
			return false;
		}

		srcState->Add( new Transition(p_name,dstState) );
		return true;
	}

	bool StateMachine::AddAllPossibleTransitions(){
		
		int transitionsAdded = 0;

		//for each source state
		std::map<std::string,State*>::const_iterator src_iter;
		std::map<std::string,State*>::const_iterator dst_iter;
		for(src_iter = m_states.begin(); src_iter != m_states.end(); src_iter++){

			//for each destination state
			for(dst_iter = m_states.begin(); dst_iter != m_states.end(); dst_iter++){

				//if the destination name does not equal the source name
				if(0 != src_iter->first.compare(dst_iter->first)){

					//create the transition name from the prefix and the destination name
					std::string transitionName("goto_");
					transitionName += dst_iter->first;

					//add the transition
					if(AddTransition(transitionName,src_iter->first,dst_iter->first)){
						transitionsAdded++;
					}
				}
			}
		}
		return transitionsAdded > 0;
	}

	bool StateMachine::StateExists(const std::string& p_name) const{
		std::map<std::string,State*>::iterator iter;
		return m_states.end() != m_states.find(p_name);
	}

	bool StateMachine::GoToState( const std::string& p_state )
	{
		State* dstState = GetState(p_state);
		if(NULL == dstState){
			return false;
		}

		if(NULL == m_currState){
			m_currState = dstState;
			m_currState->OnEnter();
			return true;
		}

		Transition* transitionToDst = m_currState->GetTransitionToState(p_state);

		return InvokeTransition(transitionToDst);
	}

	bool StateMachine::UseTransition(const std::string& p_transition)
	{
		if(NULL == m_currState){
			return false;
		}

		Transition* transitionToDst = m_currState->GetTransition(p_transition);
		return InvokeTransition(transitionToDst);
	}

	bool StateMachine::InvokeTransition(Transition* p_transition){
		if(NULL == p_transition){
			return false;
		}

		m_currState->OnExit();
		m_currState = p_transition->GetDestinationState();
		p_transition->OnTransition();
		m_stateChangeSignal(m_currState->GetName());
		m_currState->OnEnter();

		return true;
	}

	State* StateMachine::GetState(const std::string& p_state)
	{
		std::map<std::string,State*>::iterator iter = m_states.find(p_state);
		if(m_states.end() == iter){
			return NULL;
		}

		return iter->second;
	}

	Transition* StateMachine::GetTransition(const std::string& p_srcStateName, const std::string& p_transition)
	{
		State* srcState = GetState(p_srcStateName);
		if(NULL == srcState){
			return NULL;
		}

		Transition* currTransition = srcState->GetTransition(p_transition);
		if(NULL != currTransition){
			return currTransition;
		} 

		return NULL;
	}

	void StateMachine::DestroyAllStates(){
		std::map<std::string,State*>::iterator iter;
		for(iter = m_states.begin(); iter != m_states.end(); iter++){
			State* currState = iter->second;
			if(NULL != currState){
				delete currState;
			}
		}
		m_states.clear();
	}


	std::string StateMachine::CurrentState() const{
		if(NULL == m_currState){
			return std::string("");
		}

		return m_currState->GetName();
	}

	bool StateMachine::IsInState(const std::string& p_queryState) const{
		return 0 == p_queryState.compare(CurrentState());
	}

	bool StateMachine::RegisterFor_OnStateEnter(const std::string& p_stateName,const StateEnterEvent::slot_type& p_slot, boost::signals2::connection& p_out_connection){
		State* currState = GetState(p_stateName);
		if(NULL == currState){
			return false;
		}
		p_out_connection = currState->RegisterFor_OnEnter(p_slot);
		return true;
	}

	bool StateMachine::RegisterFor_OnStateExit(const std::string& p_stateName,const StateExitEvent::slot_type& p_slot, boost::signals2::connection& p_out_connection){
		State* currState = GetState(p_stateName);
		if(NULL == currState){
			return false;
		}
		p_out_connection = currState->RegisterFor_OnExit(p_slot);
		return true;
	}
	
	
	bool StateMachine::RegisterFor_OnTransition(const std::string& p_srcStateName, const std::string& p_transitionName,const TransitionEvent::slot_type& p_slot, boost::signals2::connection& p_out_connection){
		Transition* currTransition = GetTransition(p_srcStateName,p_transitionName);
		if(NULL == currTransition){
			return false;
		}
		p_out_connection = currTransition->RegisterFor_OnTransition(p_slot);
		return true;
	}

	bool StateMachine::RegisterFor_OnStateChange(const StateChangeEvent::slot_type& p_slot, boost::signals2::connection& p_out_connection){
		p_out_connection = m_stateChangeSignal.connect(p_slot);
		return p_out_connection.connected();
	}
}