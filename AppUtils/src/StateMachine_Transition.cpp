#include "StateMachine_Transition.h"

#include <iostream>

namespace apputils{

	Transition::Transition(const std::string& p_name, State* p_destination){
		m_name = p_name;
		m_destination = p_destination;
	}

	std::string Transition::GetName() const{
		return m_name;
	}

	void Transition::OnTransition(){
		m_signal_transition();
	}

	State* Transition::GetDestinationState(){
		return m_destination;
	}

	boost::signals2::connection Transition::RegisterFor_OnTransition(const TransitionEvent::slot_type& p_slot){
		return m_signal_transition.connect(p_slot);
	}

}
