#include "StateMachine_State.h"

#include <iostream>

namespace apputils{

	State::State(const std::string& p_name){
		m_name = p_name;
	}

	State::~State(){
		std::map<std::string,Transition*>::iterator iter;
		for(iter = m_transitions.begin(); iter != m_transitions.end(); iter++){
			Transition* currTransition = iter->second;
			if(NULL != currTransition){
				delete currTransition;
			}
		}
		m_transitions.clear();
	}

	void State::Add(Transition* p_newTransition){
		if(NULL != p_newTransition){
			m_transitions[p_newTransition->GetName()] = p_newTransition;
		}
	}

	void State::OnEnter(){
		m_signal_enter();
	}

	void State::OnExit(){
		m_signal_exit();
	}

	const std::string& State::GetName() const{
		return m_name;
	}

	bool State::TransitionExists(const std::string& p_name) const{
		std::map<std::string,Transition*>::iterator iter;
		return m_transitions.end() != m_transitions.find(p_name);
	}

	Transition* State::GetTransition(const std::string& p_transitionName){
		std::map<std::string,Transition*>::iterator iter;
		iter = m_transitions.find(p_transitionName);
		if(iter == m_transitions.end()){
			return NULL;
		}

		return iter->second;
	}

	Transition* State::GetTransitionToState(const std::string& p_dstStateName){
		Transition* result = NULL;
		std::map<std::string,Transition*>::iterator iter;
		for(iter = m_transitions.begin(); iter != m_transitions.end(); iter++){
			if(p_dstStateName.compare(iter->second->GetDestinationState()->GetName()) == 0){
				result = iter->second;
				break;
			}
		}
		return result;
	}


	boost::signals2::connection State::RegisterFor_OnEnter(const StateEnterEvent::slot_type& p_slot){
		return m_signal_enter.connect(p_slot);
	}

	boost::signals2::connection State::RegisterFor_OnExit(const StateExitEvent::slot_type& p_slot){
		return m_signal_exit.connect(p_slot);
	}

}
