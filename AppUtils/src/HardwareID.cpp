#include "HardwareID.h"

namespace apputils{
	
	HardwareID::HardwareID()
		: lastError(ERROR_UNINITIALIZED)
	{

	}

	HardwareID HardwareID::GetHardwareID()
	{
		HardwareID result;
		HRESULT hresult;

		// Initializing COM
		hresult = CoInitializeEx(0, COINIT_MULTITHREADED);
		if (FAILED(hresult))
		{
			result.lastError = ERROR_COINIT;
			return result;
		}

		// Set COM security levels
		hresult = CoInitializeSecurity(
			NULL,
			-1,                          // COM authentication
			NULL,                        // Authentication services
			NULL,                        // Reserved
			RPC_C_AUTHN_LEVEL_DEFAULT,   // Default authentication 
			RPC_C_IMP_LEVEL_IMPERSONATE, // Default Impersonation  
			NULL,                        // Authentication info
			EOAC_NONE,                   // Additional capabilities 
			NULL                         // Reserved
			);

		if (FAILED(hresult))
		{
			result.lastError = ERROR_COINIT_SECURITY;
			CoUninitialize();
			return result;
		}

		// Obtain initial locator to WMI
		IWbemLocator *pLocator = NULL;

		hresult = CoCreateInstance(CLSID_WbemLocator, 0, CLSCTX_INPROC_SERVER, IID_IWbemLocator, (LPVOID *)&pLocator);

		if (FAILED(hresult))
		{
			result.lastError = ERROR_COCREATE_INSTANCE;
			CoUninitialize();
			return result;
		}

		// Connect to WMI
		IWbemServices *pService = NULL;

		hresult = pLocator->ConnectServer(
			_bstr_t(L"ROOT\\CIMV2"), // Object path of WMI namespace
			NULL,                    // User name. NULL = current user
			NULL,                    // User password. NULL = current
			0,                       // Locale. NULL indicates current
			NULL,                    // Security flags.
			0,                       // Authority (for example, Kerberos)
			0,                       // Context object 
			&pService                    // pointer to IWbemServices proxy
			);

		if (FAILED(hresult))
		{
			result.lastError = ERROR_CONNECT_SERVER;
			CoUninitialize();
			return result;
		}

		// Set security levels on proxy
		hresult = CoSetProxyBlanket(
			pService,                        // Indicates the proxy to set
			RPC_C_AUTHN_WINNT,           // RPC_C_AUTHN_xxx
			RPC_C_AUTHZ_NONE,            // RPC_C_AUTHZ_xxx
			NULL,                        // Server principal name 
			RPC_C_AUTHN_LEVEL_CALL,      // RPC_C_AUTHN_LEVEL_xxx 
			RPC_C_IMP_LEVEL_IMPERSONATE, // RPC_C_IMP_LEVEL_xxx
			NULL,                        // client identity
			EOAC_NONE                    // proxy capabilities 
			);

		if (FAILED(hresult))
		{
			result.lastError = ERROR_COSET_PROXY_BLANKET;
			CoUninitialize();
			return result;
		}

		// User IWbemServices to make request from WMI
		IEnumWbemClassObject *pBaseBoardEnum = NULL;			// Deactivated to focus on UUID only.
		IEnumWbemClassObject *pComputerSystemProductEnum = NULL;

		hresult = pService->ExecQuery(
			bstr_t("WQL"),
			bstr_t("SELECT SerialNumber FROM Win32_BaseBoard"),
			WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY,
			NULL,
			&pBaseBoardEnum);

		if (FAILED(hresult))
		{
			result.lastError = ERROR_EXEC_QUERY;
			CoUninitialize();
			return result;
		}

		hresult = pService->ExecQuery(
			bstr_t("WQL"),
			bstr_t("SELECT IdentifyingNumber,SKUNumber,UUID FROM Win32_ComputerSystemProduct"),
//			bstr_t("SELECT UUID FROM Win32_ComputerSystemProduct"),
			WBEM_FLAG_FORWARD_ONLY | WBEM_FLAG_RETURN_IMMEDIATELY,
			NULL,
			&pComputerSystemProductEnum);

		if (FAILED(hresult))
		{
			result.lastError = ERROR_EXEC_QUERY;
			CoUninitialize();
			return result;
		}

		// Extract data from query results

		IWbemClassObject *pComputerSystemProductObj;
		IWbemClassObject *pBaseBoardObj;
		ULONG uReturn = 0;

		// Collected unique system identifiers
		std::wstring MBSerialNumber;
		std::wstring ProductID;	
		std::wstring ProductSKU;
		std::wstring ProductUUID;

		VARIANT vtMBSerialNumber;
		VARIANT vtProductID;
		VARIANT vtProductSKU;
		VARIANT vtProductUUID;

		while (pBaseBoardEnum)
		{
			HRESULT hr = pBaseBoardEnum->Next(WBEM_INFINITE, 1, &pBaseBoardObj, &uReturn);
			if (0 == uReturn)
			{
				break;
			}

			hr = pBaseBoardObj->Get(L"SerialNumber", 0, &vtMBSerialNumber, 0, 0);
			if (vtMBSerialNumber.vt != VT_NULL)
			{
				MBSerialNumber = std::wstring(vtMBSerialNumber.bstrVal, SysStringLen(vtMBSerialNumber.bstrVal));
			}
			VariantClear(&vtMBSerialNumber);

			pBaseBoardObj->Release();
		}

		while (pComputerSystemProductEnum)
		{
			HRESULT hr = pComputerSystemProductEnum->Next(WBEM_INFINITE, 1, &pComputerSystemProductObj, &uReturn);
			if (0 == uReturn)
			{
				break;
			}

			// Deactivated to focus on UUID only.
			hr = pComputerSystemProductObj->Get(L"IdentifyingNumber", 0, &vtProductID, 0, 0);
			if (vtProductID.vt != VT_NULL)
			{
				ProductID = std::wstring(vtProductID.bstrVal, SysStringLen(vtProductID.bstrVal));
			}
			VariantClear(&vtProductID);

			// Deactivated to focus on UUID only.
			hr = pComputerSystemProductObj->Get(L"SKUNumber", 0, &vtProductSKU, 0, 0);
			if (vtProductSKU.vt != VT_NULL)
			{
				ProductSKU = std::wstring(vtProductSKU.bstrVal, SysStringLen(vtProductSKU.bstrVal));
			}
			VariantClear(&vtProductSKU);


			hr = pComputerSystemProductObj->Get(L"UUID", 0, &vtProductUUID, 0, 0);
			if (vtProductUUID.vt != VT_NULL)
			{
				ProductUUID = std::wstring(vtProductUUID.bstrVal, SysStringLen(vtProductUUID.bstrVal));
			}
			VariantClear(&vtProductUUID);

			pComputerSystemProductObj->Release();
		}

		// Display extracted data
		//wcout << "MBSerialNumber = " << MBSerialNumber << endl;
		//wcout << "ProductID = " << ProductID << endl;
		//wcout << "ProductSKU = " << ProductSKU << endl;
		//wcout << "ProductUUID = " << ProductUUID << endl;

		result.mb_serial_number_w = MBSerialNumber;
		result.product_id_w = ProductID;
		result.product_sku_w = ProductSKU;
		result.product_uuid_w = ProductUUID;
		result.lastError = ERROR_NONE;
		
		// Release memory
		pService->Release();
		pLocator->Release();
		pBaseBoardEnum->Release();
		pComputerSystemProductEnum->Release();
		if (!pBaseBoardObj) pBaseBoardObj->Release();
		if (!pComputerSystemProductObj) pComputerSystemProductObj->Release();
		CoUninitialize();

		return result;
	}



	std::string HardwareID::mb_serial_number_a() const
	{
		return std::string(mb_serial_number_w.begin(),mb_serial_number_w.end());
	}

	std::string HardwareID::product_id_a() const
	{
		return std::string(product_id_w.begin(),product_id_w.end());
	}

	std::string HardwareID::product_sku_a() const
	{
		return std::string(product_sku_w.begin(),product_sku_w.end());
	}

	std::string HardwareID::product_uuid_a() const
	{
		return std::string(product_uuid_w.begin(),product_uuid_w.end());
	}

	bool HardwareID::Valid() const
	{
		return ERROR_NONE == lastError;
	}

}
