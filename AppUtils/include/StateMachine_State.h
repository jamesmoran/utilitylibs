#pragma once

#include <string>
#include <map>

#include <StateMachine_Events.h>
#include <StateMachine_Transition.h>

namespace apputils{

	/*
		Class: State
		A class to represent an individual state in a state machine. States are identified
		using strings (names) and can trigger events when the state machine enters or exits
		this state. Each state keeps track of the Transitions that connect it to other states.
	*/
	class State{

	public:

		MMI_EXPORT State(const std::string& p_name);
		MMI_EXPORT ~State();

		MMI_EXPORT void Add(Transition* p_newTransition);

		MMI_EXPORT boost::signals2::connection RegisterFor_OnEnter(const StateEnterEvent::slot_type& p_slot);
		MMI_EXPORT boost::signals2::connection RegisterFor_OnExit(const StateExitEvent::slot_type& p_slot);

		MMI_EXPORT void OnEnter();
		MMI_EXPORT void OnExit();

		MMI_EXPORT const std::string& GetName() const;
		MMI_EXPORT bool TransitionExists(const std::string& p_name) const;

		MMI_EXPORT Transition* GetTransition(const std::string& p_transitionName);
		MMI_EXPORT Transition* GetTransitionToState(const std::string& p_dstStateName);

	protected:

		std::string m_name;
		bool m_active;
		std::map<std::string,Transition*> m_transitions;

		StateEnterEvent m_signal_enter;
		StateExitEvent m_signal_exit;
		
	};


}
