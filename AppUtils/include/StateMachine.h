#pragma once

#include <map>
#include <string>
#include <StateMachine_Events.h>
#include <StateMachine_State.h>
#include <StateMachine_Transition.h>
#include "CoreDefines.h"

/*
	namespace: apputils
	Namespace to contain generic utilities related to the structure and organization of an
	application, such as a finite state machine.
*/
namespace apputils{

	/*	Class: StateMachine
		A class to represent a Finite State Machine.
	
		This class permits creation of a set of unique states and transitions between them.
		It has a singular "current" state that can be altered using the GoToState or UseTransition
		functions. All state transitions respect the structure of the state machine and only transitions
		that exist between states are permitted. The aforementioned functions will return false if 
		the requested transition is not permitted. Each state has two events: OnEnter and OnExit. Each
		Transition has a single event: OnTransition. These events can be bound to arbitrary functions
		using boost signals/slots. See StateMachineTest.cpp for examples of usage of all functionality.
	*/
	class StateMachine{
		
	public:
		
		/*	Constructor: StateMachine
			Creates a new state machine. Upon creation, the state machine is in a NULL state, and
			must be placed in its initial state with a call to GoToState()
		*/
		MMI_EXPORT StateMachine();

		/*	Destructor: ~StateMachine
			Cleans up all dynamically-allocated elements.
		*/
		MMI_EXPORT ~StateMachine();

		/*	Function: AddState
			Add a new state with a specified name. Duplicate state names are not permitted and will cause
			this function to return false.
		*/
		MMI_EXPORT bool AddState(const std::string& p_name);

		/*	Function: AddTransition
			Add a new transition to the state machine.
			Parameters:
			  - p_name The name of this transition. Duplicates are permitted if they have different source states.
				For example, you could add a transition called "Exit" to every state, but no state may have more than one
				transition called "Exit".
			  - p_srcState The name of the source state. The state machine must be currently in this state to invoke
				this transition.
			  - p_dstState The name of the destination state. The state machine will be in this state after a successful
				transition.
			 Return: Returns true if p_srcState and p_dstState refer to existing states in the state machine, and no transition
				of name p_name already exists with source p_srcState. False otherwise.
		*/
		MMI_EXPORT bool AddTransition(const std::string& p_name, const std::string& p_srcState, const std::string& p_dstState);

		/*	Function: AddAllPossibleTransitions
			Adds a transition from each state to each other state. TransitionNames are created by prefixing the destination
			state name with the value of the string "goto_"
		*/
		MMI_EXPORT bool AddAllPossibleTransitions();

		/*	Function: RegisterFor_OnStateEnter
			Connects a function (slot) p_slot to the OnEnter Event of state p_stateName. The function will be called
			whenever the state machine enters the state in question.
			Parameters"
			  - p_stateName the name of the state in question
			  - p_slot A function with return type void and no parameters. This can be a global function, or a member
				of a class. If using a non-static class member function you must use boost::bind() to create the slot.
			Returns: Returns true if p_stateName refers to a valid state.
		*/
		MMI_EXPORT bool RegisterFor_OnStateEnter(const std::string& p_stateName,const StateEnterEvent::slot_type& p_slot, boost::signals2::connection& p_out_connection);

		/*	Function: RegisterFor_OnStateExit
			Connects a function (slot) p_slot to the OnExit Event of state p_stateName. The function will be called
			whenever the state machine exits the state in question.
			Parameters:
			  - p_stateName the name of the state in question
			  - p_slot A function with return type void and no parameters. This can be a global function, or a member
				of a class. If using a non-static class member function you must use boost::bind() to create the slot.
			Return: Returns true if p_stateName refers to a valid state.
		*/
		MMI_EXPORT bool RegisterFor_OnStateExit(const std::string& p_stateName,const StateExitEvent::slot_type& p_slot, boost::signals2::connection& p_out_connection);

		/*	Function: RegisterFor_OnTransition
			Connects a function (slot) p_slot to the OnTrasnition Event of transition p_transitionName. The function will be called
			whenever the state machine executes the transition in question.
			Parameters:
			  - p_srcStateName the name of the state from which the transition emanates. Used to disambiguate between
				multiple transitions of the same name.
			  - p_transitionName the name of the transition in question
			  - p_slot A function with return type void and no parameters. This can be a global function, or a member
				of a class. If using a non-static class member function you must use boost::bind() to create the slot.
			Return: Returns true if p_stateName refers to a valid state.
		*/
		MMI_EXPORT bool RegisterFor_OnTransition(const std::string& p_srcStateName, const std::string& p_transitionName, const TransitionEvent::slot_type& p_slot, boost::signals2::connection& p_out_connection);

		/*	Function: RegisterFor_OnStateChange
			Connects a function (slot) p_slot to the OnStateChange Event the state machine. The function will be called
			whenever the state machine changes state
			  - p_slot A function with return type void and no parameters. This can be a global function, or a member
				of a class. If using a non-static class member function you must use boost::bind() to create the slot.
			Return: Returns true if successful
		*/
		MMI_EXPORT bool RegisterFor_OnStateChange(const StateChangeEvent::slot_type& p_slot, boost::signals2::connection& p_out_connection);

		/*	Function: StateExists
			Return: Returns true if a state of the given name exists
		*/
		MMI_EXPORT bool StateExists(const std::string& p_name) const;

		/*	Function: GoToState
			Attempt to transition to the desired state. Returns false if no transition exists between the current state 
			and the desired state.
		*/
		MMI_EXPORT bool GoToState(const std::string& p_state);

		/*	Function: UseTransition
			Attempt to invoke the desired transition. Return false if no transition exists from the current state by the 
			specified name
		*/
		MMI_EXPORT bool UseTransition(const std::string& p_transition);

		/*	Function: CurrentState
			Get the name of the current state
		*/
		MMI_EXPORT std::string CurrentState() const;

		/*	Function: IsInState
			Check if the current state is equal to the parameter
		*/
		MMI_EXPORT bool IsInState(const std::string& p_queryState) const;
		
	protected:
	
		State* GetState(const std::string& p_state);
		Transition* GetTransition(const std::string& p_srcStateName, const std::string& p_transition);

		bool InvokeTransition(Transition* p_transition);

		void DestroyAllStates();

		std::map<std::string,State*> m_states;

		State* m_currState;

		StateChangeEvent m_stateChangeSignal;

	};


}
