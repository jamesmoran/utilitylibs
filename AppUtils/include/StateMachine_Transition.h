#pragma once

#include <string>

#include <StateMachine_Events.h>
#include "CoreDefines.h"

namespace apputils{
	class State;

	/*
		Class: Transition
		Transition class for a state machine. Keep strack of a Transition (edge) between states (nodes).
		Each transition has a name and a destination. Transitions are maintained by their source states
		and refer to their destination states.
	*/
	class Transition{
		
	public:

		MMI_EXPORT Transition(const std::string& p_name, State* p_destination);

		MMI_EXPORT boost::signals2::connection RegisterFor_OnTransition(const TransitionEvent::slot_type& p_slot);

		MMI_EXPORT void OnTransition();

		MMI_EXPORT std::string GetName() const;

		MMI_EXPORT State* GetDestinationState();

	protected:

		std::string m_name;

		State* m_destination;

		TransitionEvent m_signal_transition;

	};

}
