#pragma once

#include <boost/signals2/signal.hpp>

namespace apputils{

	///The event type signaled by the state machine when it enters a state
	typedef boost::signals2::signal<void ()> StateEnterEvent;

	///The event type signaled by the state machine when it exits a state
	typedef boost::signals2::signal<void ()> StateExitEvent;

	///The event type signaled by the state machine when it transitions between two states
	typedef boost::signals2::signal<void ()> TransitionEvent;

	///The event type signaled by the state machine when it enters a new state
	typedef boost::signals2::signal<void (std::string)> StateChangeEvent;

}