#pragma once

#include <string>

#define _WIN32_DCOM
#include <comdef.h>
#include <WbemIdl.h>
#include "CoreDefines.h"
# pragma comment(lib, "wbemuuid.lib")

namespace apputils{

	/*	Class: HardwareID
		The HardwareID class contains a representation of the motherboard identifying information
		available via the OS. This includes the product ID, SKU, UUID (http://en.wikipedia.org/wiki/Universally_unique_identifier)
		and motherboard serial number.

		Example:
			(start code)
				apputils::HardwareID myID = GetHardwareID();
				if(myID.Valid()){
					std::cout << myID.mb_serial_number_a() << std::endl;	//multi-byte character string
					std::wcout << myID.mb_serial_number_w << std::endl;		//wide char character string
				}else{
					std::cerr << "Error: unable to query motherboard info. Error code " << myID.lastError << std::endl;
				}
			(end)
	*/
	class HardwareID{
	public:

		/*	Enum: HardwareIDError
			Enumerates all of the possible error codes. ERROR_NONE indicates success, and ERROR_UNINITIALIZED indicates
			that the constructor was called, but the static query function <GetHardwareID> was not used to populate it.
		*/
		enum HardwareIDError{
			ERROR_NONE,
			ERROR_UNINITIALIZED,
			ERROR_COINIT,
			ERROR_COINIT_SECURITY,
			ERROR_COCREATE_INSTANCE,
			ERROR_CONNECT_SERVER,
			ERROR_COSET_PROXY_BLANKET,
			ERROR_EXEC_QUERY
		};

		/*	Method: GetHardwareID
			This static method is the means by which the system queries for the values in HardwareID, thus producing a 
			valid object. The returned object contains the results (if successful) and an error code <lastError> indicating
			success or the nature of the failure. The intended usage is for the return value to be assigned to a local
			HardwareID variable, and then checked for validity using the <Valid> predicate before use. (See the example in
			the class documentation).
	
			Return:
				apputils::HardwareID -
		*/
		static HardwareID GetHardwareID();

		/*	Method: HardwareID
			Constructor, creates an invalid empty HardwareID object.
		*/
		MMI_EXPORT HardwareID();

		/*	Variable: mb_serial_number_w
			The motherboard serial number, as a wide character string.
		*/
		std::wstring mb_serial_number_w;

		/*	Variable: product_id_w
			The product ID number, as a wide character string.
		*/
		std::wstring product_id_w;

		/*	Variable: product_sku_w
			The product SKU number, as a wide character string.
		*/
		std::wstring product_sku_w;

		/*	Variable: product_uuid_w
			The product Universally Unique Identifier (UUID), as a wide character string. For more information on UUIDs, see
			http://en.wikipedia.org/wiki/Universally_unique_identifier
		*/
		std::wstring product_uuid_w;

		/*	Variable: lastError
			The last error encountered by the query. ERROR_NONE indicates success, and ERROR_UNINITIALIZED indicates
			that the constructor was called, but the static query function <GetHardwareID> was not used to populate it.
		*/
		HardwareIDError lastError;

		/*	Method: mb_serial_number_a
			Returns the motherboard serial number, as a multi-byte character string.
		*/
		MMI_EXPORT std::string mb_serial_number_a() const;

		/*	Method: product_id_a
			Returns the product ID, as a multi-byte character string.
		*/
		MMI_EXPORT std::string product_id_a() const;

		/*	Method: product_sku_a
			Returns the product sku, as a multi-byte character string.
		*/
		MMI_EXPORT std::string product_sku_a() const;

		/*	Method: product_uuid_a
			Returns the motherboard UUID, as a multi-byte character string.
		*/
		MMI_EXPORT std::string product_uuid_a() const;

		/*	Method: Valid
			Returns true if lastError is ERROR_NONE, false otherwise.
		*/
		MMI_EXPORT bool Valid() const;

	};
}