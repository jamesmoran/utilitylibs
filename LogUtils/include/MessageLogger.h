#pragma once

#include <string>
#include <time.h>
#include <memory>
#include <deque>
#include <map>
#include <windows.h>
#include <ActiveObject.h>
#include <boost/regex.hpp>
#include "CoreDefines.h"

namespace logutils{

	class MessageBody{
	public:
		MMI_EXPORT MessageBody(const std::string& prefix, const std::string& text);
		MMI_EXPORT MessageBody(time_t timestamp, const std::string& prefix, const std::string& text);

		time_t timestamp;
		std::string prefix;
		std::string text;
	}; 

	typedef std::shared_ptr<MessageBody> Message;

	class MessageConsumerBase{
	public:
		MMI_EXPORT MessageConsumerBase();

		MMI_EXPORT virtual bool ProcessMessage(const Message& m) = 0;

	private:
		/*	Constructor: MessageConsumerBase
			Private copy constructor prevents copying.
		*/
		MessageConsumerBase(const MessageConsumerBase& c);

		/*	Method: operator=
			Private operator= prevents copying
		*/
		void operator=(const MessageConsumerBase& c);

	};

	class MessageConsumerFiltered : public MessageConsumerBase{
	public:

		MMI_EXPORT MessageConsumerFiltered(const boost::regex& regex);
		MMI_EXPORT ~MessageConsumerFiltered();

		MMI_EXPORT void SetFilter(const boost::regex& regex);

	protected:
		virtual bool ProcessMessage(const Message& m);
		virtual void OnMessagePassedFilter(const Message& m);

		bool Filter(const std::string prefix);

		mutable CRITICAL_SECTION m_regexAccessSerializer;
		boost::regex m_regex;
	};

	class MessageConsumerStdout : public MessageConsumerFiltered{
	public:
		MMI_EXPORT MessageConsumerStdout(const boost::regex& regex);

		MMI_EXPORT virtual void OnMessagePassedFilter(const Message& m);
	};


	/*
		Class: MessageLogger
		A MessageLogger is an externally thread-safe message pumping system. It exposes an interface:
		the Log() function, which permits external entities to provide "messages", which are pairs of
		strings. The strings are a prefix string indicating content source/semantics, and a text string
		containing content.

		The MessageLogger inherits privately from threadutils::ActiveObject, because it has a controllable
		internal thread that processes the messages asynchronously. The thread provides the message to 
		any and all attached Message Consumers, which are objects derived from the MessageConsumerBase
		abstract class. They may an any arbitrary operation with the messages they receive.

		The intent is that client developers may pass strings to the MessageLogger much as they would
		to a text console for debugging and monitoring purposes, but that multiple, arbitrary consumers
		can now receive the messages. This will potentially enable output to the console, a log file, 
		a network socket, or other message sinks of the user's choosing.
	*/
	class MessageLogger : private threadutils::ActiveObject{
	public:

		MMI_EXPORT MessageLogger(int maxQueuedMsgs = 10000);
		MMI_EXPORT ~MessageLogger();

		MMI_EXPORT void Start();

		MMI_EXPORT void Log(const std::string& prefix, const std::string& message);

		MMI_EXPORT bool AttachConsumer(const std::string& name, MessageConsumerBase* consumer);
		MMI_EXPORT bool AttachConsumer(const std::string& name, std::shared_ptr<MessageConsumerBase> consumer);
		MMI_EXPORT bool DetachConsumer(const std::string& name);
		MMI_EXPORT void DetachAllConsumers();

		MMI_EXPORT __int64 MessagesProcessed() const;
		MMI_EXPORT __int64 MessageBatchesProcessed() const;
		bool m_thread_exited;

	protected:
		virtual void InitThread();
		virtual void Run();
		virtual void FlushThread();

	private:

		bool ConsumerExists(const std::string& name) const;

		mutable CRITICAL_SECTION m_msgQueueAccessSerializer;
		std::deque<Message> m_msgQueue;

		int m_maxQueuedMsgs;

		mutable CRITICAL_SECTION m_consumerMapAccessSerializer;
		std::map<std::string,std::shared_ptr<MessageConsumerBase>> m_consumerMap;

		threadutils::Event m_newDataAvailable;

		__int64 m_messagesProcessed;
		__int64 m_messageBatchesProcessed;
		__int64 m_messagesDropped;
	};
}