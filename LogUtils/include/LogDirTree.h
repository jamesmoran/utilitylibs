#pragma once

#include <time.h>
#include <string>
#include <boost/filesystem.hpp>
#include <DirTree.h>
#include "CoreDefines.h"

namespace fs = boost::filesystem;

namespace logutils{

	/*	Class: LogDirTreeNode 
		Node class corresponding to a directory in the Windows file system. The purpose of this class is to create a
		representation of a subtree of the file system. The tree is self-creating, so usage would appear like this:

		
	*/
	class LogDirTreeNode : public DirTreeNode{
	public:

		/*	Enumeration: DirType
			Each level of the tree has a specific meaning. The root of the tree corresponds to a particular type of log file.
			The immediate children of the root (depth 1) represent individual years of data. Depth 2 directories represent
			months, and depth 3 directories represent days. This enumeration provides labels for each level.
		*/
		enum DirType{
			UNKNOWN,
			ROOT,
			YEAR,
			MONTH,
			DAY
		};

		/*	Constructor: LogDirTreeNode
			Constructor for a root node with a specified directory. The resulting tree will 
			begin at this node and consider only subdirectories.

			Parameters:
			  rootdir - The boost::filesystem::path object specifying the root directory.
		*/
		MMI_EXPORT LogDirTreeNode(const fs::path& rootdir);
		
		/*	Destructor: ~LogDirTreeNode
			Destructor, cleans up the entire tree.
		*/
		MMI_EXPORT ~LogDirTreeNode();

		/*	Method: Enumerate
			Clears the current representation of the tree and re-enumerates the filesystem recursively, starting
			at the root directory. This may be an expensive call if the file tree is large.
		*/
		MMI_EXPORT virtual bool Enumerate(bool recursive);

		/*	Method: PrettyPrint
			Creates an indented, multi-line string representation of the tree as a string. Directory names are
			displayed along with cumulative bytes, number of files and number of subdirectories. Print is recursive
			if specified.
		*/
		MMI_EXPORT std::string PrettyPrint(bool recursive) const;

		MMI_EXPORT virtual __int64 SortIndex() const;

		MMI_EXPORT time_t TimeStamp() const;

		MMI_EXPORT time_t OldestDirTime() const;
		MMI_EXPORT time_t NewestDirTime() const;

		MMI_EXPORT fs::path RootDir() const;

		MMI_EXPORT fs::path TodayDir() const;

		MMI_EXPORT fs::path OldestDayDir() const;

		MMI_EXPORT time_t RemoveOldestDay();
		MMI_EXPORT time_t RemoveOldestDay(fs::path& p_pathRemoved_out);

		MMI_EXPORT int PruneEmptyChildDirectories();

	protected:

		LogDirTreeNode(LogDirTreeNode* parent, const fs::path& dir);

		LogDirTreeNode* OldestDayNode() const;

		bool RemoveChildNode(LogDirTreeNode* targetNode);

		static time_t InferTimestamp(DirType p_type,const std::string dirName);

		DirType m_type;
		time_t m_timestamp;
	
	
	};


	


}
