#ifndef UTILITYLIBS_BASICBINARYLOGGER_H_
#define UTILITYLIBS_BASICBINARYLOGGER_H_

#include <windows.h>
#include <list>
#include <string>
#include <fstream>
#include "CoreDefines.h"

// Namespace: logutils
namespace logutils
{
/*
Define:
BLOCKSIGNATURE - default BlockSignature for each BlockHeader
*/
#define BLOCKSIGNATURE 8594125124872206148	// (0x7744774477447744)

class BinaryBuffer
{
public:

	MMI_EXPORT BinaryBuffer(size_t Capacity);
	MMI_EXPORT ~BinaryBuffer();

	MMI_EXPORT size_t Capacity() const {return BufferCapacity;}
	MMI_EXPORT size_t Size() const {return BufferSize;}
	MMI_EXPORT unsigned char* DataBuffer() const {return Buffer;}

	MMI_EXPORT void Clear();

	MMI_EXPORT bool AddData(const void* data, size_t length);
		
	template<typename T>
	bool AddObject(const T& object)
	{
		return AddData((const void*)&object, sizeof(T));
	}
private:
	unsigned char* Buffer;
	size_t BufferCapacity;
	size_t BufferSize;
};


class BasicBinaryLogger
{
public:
	
	/*
	Struct: BlockHeader
	
	Function:	blockHeader 
	Default Constructor

	Variables:
	blockSignature - starting signature for each block
	numberOfDataBytes - number of bytes of data written following the header
	checkSum - CRC for data written. it is calculated by byte.
	*/
	struct blockHeader{
		MMI_EXPORT blockHeader();
		const unsigned long long blockSignature ;
		unsigned int numberOfDataBytes;
		unsigned int checkSum;
	};

	/* 
	Class: BasicBinaryLogger
	*/

	/*
	Function: BasicBinaryLogger
	Default Constructor

	Parameters:
		FilePath - location where log file will be saved
		MaxSizeKB - max size of the file before it is archived
		MemoryBufferSize - max size of log buffer on memory before it is written to disk
		AddBlockHeader - option to select to have a block header added to the beginning of each log buffer
	
	See Also:
		<~BasicBinaryLogger>

	*/
	MMI_EXPORT BasicBinaryLogger(const std::string& FilePath, size_t MaxSizeKB, size_t MemoryBufferSize = 2048);
	MMI_EXPORT BasicBinaryLogger(const std::string& FilePath, size_t MaxSizeKB, size_t MemoryBufferSize, bool AddBlockHeader);
	
	/*
	Function: ~BasicBinaryLogger
	Default Destructor

	See Also:
		<BasicBinaryLogger>
	*/
	MMI_EXPORT ~BasicBinaryLogger();

	/*
	Function: Exit
	Clears out any remaining logs
	*/
	MMI_EXPORT void Exit();

	template<typename T>
	/*
	Function: AddData[T]
	
	Templatetized function for adding logger generic structures/data.
	
	Parameters:
		data - data of any type to be added to the log buffer
		flushThisTime - flag to initiate writing to disk

	See Also: 
		<AddData>
	*/
	void AddData(const T& data, bool flushThisTime = false)
	{
		if (Buffer.Size() + sizeof(T) > Buffer.Capacity())
			QueueLogOperation(); // Send the current log off to disk.
		Buffer.AddObject(data);
		if (flushThisTime)
			QueueLogOperation();
	}

	/*
	Function: AddData
	
	Function for adding logger generic structures/data.
	
	Parameters:
		data - data pointer to be added to the log buffer
		length - size of data being added to the log buffer
		flushThisTime - flag to initiate writing to disk

	See Also: 
		<AddData[T]>
	*/
	MMI_EXPORT void AddData(void* data, size_t length, bool FlushThisTime = false);


	MMI_EXPORT bool AtNewFile() const;

	MMI_EXPORT static int CRC( void* number, int lengthInBits);

	MMI_EXPORT bool ChangeBlockHeader(unsigned long long newBlockHeader);

private:

	struct LogOperationInfo
	{
		unsigned char* Buffer;
		size_t BufferSize;
	};

	typedef std::list<LogOperationInfo> logQueueType; 
	logQueueType LogQueue;

	void QueueLogOperation();

	void LoggerOperation();
	bool WriteLog(const LogOperationInfo& log);
	static void WorkerThreadProc(void* lpVoid);

	size_t GetFileSize(std::ofstream& ofs) const;
	bool ArchiveFile() const;
	void CheckFileSize();
	std::string Path;
	BinaryBuffer Buffer;

	size_t MaxFileSizeKB;
	bool UseBlockHeader;
	HANDLE ExitEvent, NewLogEvent, ThreadHandle;
	mutable CRITICAL_SECTION LogQueueLock;
		
	bool NextOperationOnNewFile;
};
}


#endif