/*
Requirements:
- Reads any binary file with a header as implemented in BasicBinaryLogger
- Accepts any data structure the user is expecting
- Will verify the Check Sum after reading the data
- Only pass the new data back to the user if the Check Sum passes
- Discards the data when the Check Sum fails

*/

#pragma once

#include <windows.h>
#include <list>
#include <string>
#include "BasicBinaryLogger.h"
#include "CoreDefines.h"

using namespace std;

//Namespace: logutils
namespace logutils
{

	//Class: BasicBinaryLogReader
	class BasicBinaryLogReader
	{
	public:
		/*
		Function: BasicBinaryLogReader
		default constructor
		*/
		MMI_EXPORT BasicBinaryLogReader();


		/*
		Function: ~BasicBinaryLogReader
		default destructor which deletes all dynamically allocated memory
		*/
		MMI_EXPORT ~BasicBinaryLogReader();

		/*
		Function: OpenFile
		Opens the file and initializes the private member variables

		Parameters:
			filename - The location of the file
			fileBufferSignature - The BlockSignature that the program should look for
			dataStruct - The type of data being read in the file
			readBufferLength - The maximum length of bytes to read and load into the buffer when ReadNextBlock

		Returns:
			If the file is opened successfully

		See Also:
			<ReadNextBlock>
		*/
		template<typename T>
		bool OpenFile(const char * filename, unsigned long long fileBufferSignature, T dataStruct, unsigned int readBufferLength = 2048)
		{
			if(ifs.is_open()) //checks if file is open already
			{
				ifs.close();
				return false;
			}
			ifs.open(filename,ifstream::in | ifstream::binary);//very important to define this as a binary file else the read function ends at the new line ascii code
			if(!ifs.is_open()) //checks if file is opened properly
				return false;

			//find the size of the entire file
			ifs.seekg (0, ios::end);
			fileLength = ifs.tellg();
			ifs.seekg (0, ios::beg);
			//printf("length = %d\n",fileLength);


			//initialize the program variables
			bufferLength = readBufferLength;
			CreateBuffer(readBufferLength);

			bufferSignature = fileBufferSignature;
			
			bufferSize = 0;
			bufferIndex = bufferSize;

			flagInitialized = 1;
			return true;
		}

		/*
		Function: CloseFile
		Closes the file and clears buffers

		Parameters:
		filename - The location of the file

		Returns:
		True if file was closed successfully
		*/
		MMI_EXPORT bool CloseFile(const char * filename);


		/*
		Function: ReadNextBlock

		Searches for the start of the next block header and reads exactly one block of data. If it passes the CRC test, the list is filled

		Parameters:
		dataList - The list where data will be stored

		Returns:
		True if next block exists 
		*/
		template<typename T> bool ReadNextBlock(std::list<T>&dataList)
		{
			std::list<T>tempList;

			if(!flagInitialized) //quits without reading
				return false;

			if(fileLength == 0 && bufferIndex >= bufferSize )
				return false;

			if(SearchBlockHeader(buffer,bufferIndex,BLOCKSIGNATURE))
			{
				ReadBlockHeader(buffer, bufferIndex, refNumberOfDataBytes, refCheckSum);
				ReadNextDataSet(buffer, bufferIndex, refNumberOfDataBytes, refCheckSum, dataCheckSum, tempList);
				VerifyCheckSum(refCheckSum, dataCheckSum, tempList, dataList);
			}
			else
			{
				return false;
			}

			return true;
		}
		/*
		Function: ReadNextBlock

		Searches for the start of the next block header and reads exactly one block of data. If it passes the CRC test, the char array is filled

		Parameters:
		charData - The array where data will be stored
		charSize - The length of charData

		Returns:
		True if next block exists 
		*/
		MMI_EXPORT bool ReadNextBlock(char* &charData, unsigned int &charSize);
		
	private:
		BasicBinaryLogger::blockHeader theBlockHeader;
		size_t bufferIndex;
		size_t bufferLength;
		size_t bufferSize;
		unsigned int refNumberOfDataBytes;
		unsigned int refCheckSum;
		unsigned int dataCheckSum;
		unsigned long long bufferSignature;

		ifstream ifs;
		size_t fileLength;

		bool flagInitialized;

		char* buffer;

	
		void CreateBuffer(size_t p_size );
		
	
		void DestroyBuffer();

		void ResizeBuffer(size_t p_size );
		

		bool FillBuffer(char* sourceBuffer, size_t& bufferPosition);


		bool SearchBlockHeader(char* sourceBuffer, size_t& bufferPosition, unsigned long long blockSignature);
		

		void ReadBlockHeader(char* sourceBuffer, size_t& bufferPosition, unsigned int& dataBytes, unsigned int& checkSum);


		unsigned int CharArrayToUnsignedInt(char* charArray){	return *((unsigned int*)charArray);}
		

		static unsigned long long CharArrayToUnsignedLongLong(char* charArray){	return *((unsigned long long*)charArray);}

		template<typename T> bool ReadNextDataSet(char* sourceBuffer, size_t& bufferPosition,unsigned int refDataBytes, unsigned int referenceCheckSum, unsigned int& calculatedCheckSum, std::list<T>&dataList)
		{
			char* dataBuffer = new char [ sizeof(T)];
			unsigned int dataCheckSum = 0;
			unsigned int dataBytesRead = 0;

			while(dataBytesRead < refDataBytes)
			{
				ReadBytesInBuffer(sourceBuffer, bufferPosition, dataBuffer, sizeof(T));
				dataCheckSum += BasicBinaryLogger::CRC(dataBuffer, sizeof(T));
				dataList.push_back(*(T*)dataBuffer);
				dataBytesRead += sizeof(T);
			}

			calculatedCheckSum = dataCheckSum;
			delete[] dataBuffer;

			if (calculatedCheckSum == referenceCheckSum)
				return true;
			else
				return false;
		}

		bool ReadNextDataSet(char* sourceBuffer, size_t& bufferPosition,unsigned int refDataBytes, unsigned int referenceCheckSum, unsigned int& calculatedCheckSum, char* charData);
		void ReadBytesInBuffer(char* sourceBuffer, size_t& bufferPosition, char* destBuffer, int numberOfBytes);
		template<typename T> static bool VerifyCheckSum(unsigned int referenceCRC, unsigned int calculatedCRC, std::list<T>& tempList, std::list<T>& dataList)
		{
			std::list<T>::iterator listIterator;

			if (calculatedCRC == referenceCRC)//verify the checkSum
			{
				for(listIterator=tempList.begin();listIterator!=tempList.end();listIterator++)
				{
					dataList.push_back(*listIterator);//data is put into the dataList
				}
				tempList.clear();
				return 1;
			}
			else
				return 0;
		}

	};
}