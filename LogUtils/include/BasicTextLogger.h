#ifndef UTILITYLIBS_BASICTEXTLOGGER_H_
#define UTILITYLIBS_BASICTEXTLOGGER_H_

#include <windows.h>
#include <string>
#include <list>

namespace logutils
{
	class BasicTextLogger
	{
	public:
		enum OperationStatus
		{
			NoOperation,
			Success,
			FileNotFound,
			Failed
		};

		MMI_EXPORT BasicTextLogger(const std::string& FilePath, const std::string& VersionString, size_t MaxSizeKB);
		MMI_EXPORT ~BasicTextLogger();

		MMI_EXPORT void Exit();
		MMI_EXPORT void AddText(const std::string& text);
		MMI_EXPORT OperationStatus GetLastOperationStatus() const;

	private:
		void LoggerOperation();
		static void WorkerThreadProc(void* lpVoid);
		OperationStatus DumpLogList();
		size_t GetFileSize(std::ofstream& ofs) const;
		bool ArchiveFile() const;

		std::list<std::string> LogList;
		bool ThreadRunning;
		std::string Path;
		std::string Version;
		size_t MaxFileSizeKB;
		OperationStatus LastOperationStatus;
		HANDLE ExitEvent, NewLogEvent, ThreadDoneEvent;
		mutable CRITICAL_SECTION LogListLock;
	};
}

#endif 