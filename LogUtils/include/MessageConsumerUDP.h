#pragma once

#include <Address.h>
#include <UDPSocket.h>
#include <MessageLogger.h>
#include "CoreDefines.h"


namespace logutils{

	class MessageConsumerUDP : public MessageConsumerFiltered{
	public:
		
		static const char SEPARATOR;
		
		MMI_EXPORT MessageConsumerUDP(const networkutils::Address& addr, const boost::regex& filter = boost::regex(".*"));
		MMI_EXPORT ~MessageConsumerUDP();

		MMI_EXPORT bool IsBound() const;

	protected:

		virtual void OnMessagePassedFilter(const Message& m);
	
		networkutils::UDPSocket m_udpSocket;
		networkutils::Address addr;

	};


}
