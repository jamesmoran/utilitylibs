#pragma once

#include <time.h>
#include <string>
#include <boost/filesystem.hpp>
#include "CoreDefines.h"

namespace fs = boost::filesystem;

namespace logutils{

	/*	Function: TimestampToDirPath
		Takes a boost::filesystem::path object describing a directory and appends three subdirectories 
		corresponding to the year, month, and day as specified in the supplied timestamp. 

		Parameters:
		  root - The path of the folder to which the three additional folders are to be appended (see boost/filesystem.hpp)
		  timestamp - The UNIX timestamp (in seconds) corresponding to the current time (see time.h)

		Example:
		  If root is C:\temp\, and the timestamp corresponds to Aug 21st, 2013 (17:18:05), then the resulting path
		  object will correspond to C:\temp\2013\08\21\
	*/
	MMI_EXPORT fs::path TimestampToDirPath(const fs::path& root, time_t timestamp);

	/*	Class: DirTreeNode 
		Node class corresponding to a directory in the Windows file system. The purpose of this class is to create a
		representation of a subtree of the file system. The tree is self-creating, so usage would appear like this:

		Example:
		(start code)
			//create the root path.
			boost::filesystem::path rootDirectory("C:\temp");

			//create a root node, specifying the directory.
			logutils::DirTreeNode* rootNode = new logutils::DirTreeNode(rootDirectory);

			//run enumeration, which generates a tree based on the filesystem.
			rootNode->Enumerate();

			//print a formatted representation of the file tree.
			std::cout << rootNode->PrettyPrint(true) << std::endl;

			//print the total bytes used by all files in the tree.
			std::cout << "Total bytes: " << rootNode->DiskUsageBytes(true) << std::endl;

			//delete the root node, cleanly destroying the tree.
			delete rootNode;
		(end)
	*/
	class DirTreeNode{
	public:

		/*	Constructor: DirTreeNode
			Constructor for a root node with a specified directory. The resulting tree will 
			begin at this node and consider only subdirectories.

			Parameters:
			  rootdir - The boost::filesystem::path object specifying the root directory.
		*/
		MMI_EXPORT DirTreeNode(const fs::path& rootdir);
		
		/*	Destructor: ~DirTreeNode
			Destructor, cleans up the entire tree.
		*/
		MMI_EXPORT ~DirTreeNode();

		/*	Method: Directory
			Returns the path fragment corresponding to the directory name of a given node.
		*/
		MMI_EXPORT fs::path Directory() const;

		/*	Method: PathInTree
			Returns the path from the tree root to the current node.
		*/
		MMI_EXPORT fs::path PathInTree() const;

		/*	Method: FullPath
			Gives the full path including the root directory path of the current node.
		*/
		MMI_EXPORT fs::path FullPath() const;

		/*	Method: DiskUsageBytes
			Gets the disk usage of this node in bytes. If recursive, the value includes all files inside
			subdirectories of this node.
		*/
		MMI_EXPORT __int64 DiskUsageBytes(bool recursive) const;

		/*	Method: FileCount
			Get the number of regular files contained in this directory. If recursive, the count includes
			all files inside subdirectories too.
		*/
		MMI_EXPORT __int64 FileCount(bool recursive) const;

		/*	Method: SubDirCount
			Gets the number of subdirectories. If recursive, the count includes the number of subdirectories in 
			all subdirectories as well.
		*/
		MMI_EXPORT __int64 SubDirCount(bool recursive) const;

		/*	Method: Depth
			Evaluates the number of levels below the tree root that this node is. The root node has a depth of 0.
		*/
		MMI_EXPORT int Depth() const;

		/*	Method: Enumerate
			Clears the current representation of the tree and re-enumerates the filesystem recursively, starting
			at the root directory. This may be an expensive call if the file tree is large.
		*/
		MMI_EXPORT virtual bool Enumerate(bool recursive);

		/*	Method: PrettyPrint
			Creates an indented, multi-line string representation of the tree as a string. Directory names are
			displayed along with cumulative bytes, number of files and number of subdirectories. Print is recursive
			if specified.
		*/
		MMI_EXPORT virtual std::string PrettyPrint(bool recursive) const;

		/*	Method: SortIndex
			A virtual method intended to provide a value used in sorting of child nodes. Nodes will be sorted to
			arrange this value in ascending order.
		*/
		MMI_EXPORT virtual __int64 SortIndex() const;

	protected:

		DirTreeNode(DirTreeNode* parent, const fs::path& dir);

		void Clear();

		DirTreeNode* m_parent;
		std::vector<DirTreeNode*> m_children;

		fs::path m_dirName;

		__int64 m_diskUsageBytes;
		__int64 m_fileCount;



	};


	bool CompareForSort_DirTreeNode(const DirTreeNode* A, const DirTreeNode* B);


}
