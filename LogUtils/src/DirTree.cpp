#include "DirTree.h"

#include <sstream>
#include <assert.h>
#include <StringUtils.h>

namespace fs = boost::filesystem;

namespace logutils{
	
	fs::path TimestampToDirPath(const fs::path& root, time_t timestamp){
		
		fs::path dirpath = root;

		struct tm time_struct;
		if(0 == localtime_s(&time_struct,&timestamp)){
			
			using namespace stringutils;

			dirpath = dirpath / fs::path(str(1900 + time_struct.tm_year));
			dirpath = dirpath / fs::path(str(1+time_struct.tm_mon,2,'0'));
			dirpath = dirpath / fs::path(str(time_struct.tm_mday,2,'0'));

		}

		return dirpath;
	}

	DirTreeNode::DirTreeNode(const fs::path& rootdir)
		: m_parent(NULL)
		, m_diskUsageBytes(0)
		, m_fileCount(0)
	{
		m_dirName = rootdir;
		if(!fs::is_directory(m_dirName)){
			m_dirName.remove_filename();
		}
	}

	DirTreeNode::DirTreeNode(DirTreeNode* parent, const fs::path& dir)
		: m_parent(parent)
		, m_dirName(dir)
		, m_diskUsageBytes(0)
		, m_fileCount(0)
	{
		assert(NULL != m_parent);
	}

	DirTreeNode::~DirTreeNode(){
		Clear();
	}

	fs::path DirTreeNode::Directory() const{
		return m_dirName;
	}

	fs::path DirTreeNode::PathInTree() const{
		if(NULL != m_parent){
			return m_parent->PathInTree() / m_dirName;
		}
		return fs::path();
	}

	fs::path DirTreeNode::FullPath() const{
		if(NULL != m_parent){
			return m_parent->FullPath() / m_dirName;
		}
		return m_dirName;
	}

	__int64 DirTreeNode::DiskUsageBytes(bool recursive) const{
		__int64 total = m_diskUsageBytes;
		if(recursive){
			std::vector<DirTreeNode*>::const_iterator iter;
			for(iter = m_children.begin(); iter != m_children.end(); iter++){
				total += (*iter)->DiskUsageBytes(recursive);
			}
		}
		return total;
	}

	__int64 DirTreeNode::FileCount(bool recursive) const{
		__int64 total = m_fileCount;
		if(recursive){
			std::vector<DirTreeNode*>::const_iterator iter;
			for(iter = m_children.begin(); iter != m_children.end(); iter++){
				total += (*iter)->FileCount(recursive);
			}
		}
		return total;
	}

	__int64 DirTreeNode::SubDirCount(bool recursive) const{
		__int64 total = (__int64)m_children.size();
		if(recursive){
			std::vector<DirTreeNode*>::const_iterator iter;
			for(iter = m_children.begin(); iter != m_children.end(); iter++){
				total += (*iter)->SubDirCount(recursive);
			}
		}
		return total;
	}

	int DirTreeNode::Depth() const{
		if(NULL != m_parent){
			return m_parent->Depth()+1;
		}
		return 0;
	}

	bool DirTreeNode::Enumerate(bool recursive){
		
		Clear();

		for(fs::directory_iterator dir_iter(FullPath()); dir_iter != fs::directory_iterator(); dir_iter++){
			if(fs::is_regular_file(dir_iter->path())){
				m_fileCount++;
				uintmax_t fileSize = fs::file_size(dir_iter->path());
				m_diskUsageBytes += fileSize;
			}else if(fs::is_directory(dir_iter->path())){
				m_children.push_back(new DirTreeNode(this,dir_iter->path().leaf()));
			}
		}

		std::sort(m_children.begin(),m_children.end(),CompareForSort_DirTreeNode);
		
		if(recursive){
			std::vector<DirTreeNode*>::const_iterator iter;
			for(iter = m_children.begin(); iter != m_children.end(); iter++){
				(*iter)->Enumerate(recursive);
			}
		}

		return true;
	}

	std::string DirTreeNode::PrettyPrint(bool recursive) const{
		int depth = Depth();
		std::stringstream ss;
		for(int i = 0; i < depth; i++){
			ss << ' ';
		}
		ss << m_dirName.string() << ' ' << stringutils::FormatDataQty(DiskUsageBytes(recursive))<< ' ' << FileCount(false) << " files, " << SubDirCount(false) << " subdirs"<< std::endl;
		if(recursive){
			std::vector<DirTreeNode*>::const_iterator iter;
			for(iter = m_children.begin(); iter != m_children.end(); iter++){
				ss << (*iter)->PrettyPrint(recursive);
			}
		}

		return ss.str();
	}

	void DirTreeNode::Clear(){
		std::vector<DirTreeNode*>::iterator iter;
		for(iter = m_children.begin(); iter != m_children.end(); iter++){
			delete *iter;
		}
		m_children.clear();

		m_fileCount = 0;
		m_diskUsageBytes = 0;
	}


	__int64 DirTreeNode::SortIndex() const{
		return 0;
	}


	bool CompareForSort_DirTreeNode(const DirTreeNode* A, const DirTreeNode* B){
		assert(NULL != A && NULL != B);
		return A->SortIndex() < B->SortIndex();
	}
}