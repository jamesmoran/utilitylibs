#include "LogDirTree.h"

#include <sstream>
#include <assert.h>
#include <StringUtils.h>
#include <MathUtils.h>

namespace fs = boost::filesystem;

namespace logutils{
	
	LogDirTreeNode::LogDirTreeNode(const fs::path& rootdir)
		: DirTreeNode(rootdir)
		, m_type (UNKNOWN)
		, m_timestamp(0)
	{
		
	}

	LogDirTreeNode::LogDirTreeNode(LogDirTreeNode* parent, const fs::path& dir)
		: DirTreeNode(parent,dir)
		, m_type (UNKNOWN)
		, m_timestamp(0)
	{
		
	}

	LogDirTreeNode::~LogDirTreeNode(){
		Clear();
	}

	time_t LogDirTreeNode::TimeStamp() const{
		
		time_t result = 0;
		
		switch(m_type){
		default:
		case UNKNOWN:
		case ROOT:
			result = 0;
			break;
		case YEAR:
			result = m_timestamp;
			break;
		case MONTH:
			result = m_timestamp - mathutils::MakeTimestamp(1970,1,1,0,0,0,false) + ((LogDirTreeNode*) m_parent)->TimeStamp();
			break;
		case DAY:
			result = m_timestamp - mathutils::MakeTimestamp(1970,1,1,0,0,0,false) + ((LogDirTreeNode*) m_parent)->TimeStamp();
			break;
		}

		return result;
	}

	bool LogDirTreeNode::Enumerate(bool recursive){
		
		Clear();

		switch(Depth()){
		default:
			m_type = UNKNOWN;
			break;
		case 0:
			m_type = ROOT;
			break;
		case 1:
			m_type = YEAR;
			break;
		case 2:
			m_type = MONTH;
			break;
		case 3:
			m_type = DAY;
			break;
		}


		if(m_type != UNKNOWN){

			m_timestamp = InferTimestamp(m_type,Directory().stem().string());
			if(m_timestamp > 0 || ROOT == m_type ){

				for(fs::directory_iterator dir_iter(FullPath()); dir_iter != fs::directory_iterator(); dir_iter++){
					if(fs::is_regular_file(dir_iter->path())){
						m_fileCount++;
						uintmax_t fileSize = fs::file_size(dir_iter->path());
						m_diskUsageBytes += fileSize;

					}else if(fs::is_directory(dir_iter->path())){
						m_children.push_back((DirTreeNode*) new LogDirTreeNode(this,dir_iter->path().leaf()));
					}
				}

				std::sort(m_children.begin(),m_children.end(),CompareForSort_DirTreeNode);

				if(recursive){
					std::vector<DirTreeNode*>::const_iterator iter;
					for(iter = m_children.begin(); iter != m_children.end(); iter++){
						LogDirTreeNode* node = (LogDirTreeNode*) *iter;
						node->Enumerate(recursive);
					}
				}

			}
		}

		return true;
	}

	std::string LogDirTreeNode::PrettyPrint(bool recursive) const{
		int depth = Depth();
		std::stringstream ss;
		for(int i = 0; i < depth; i++){
			ss << ' ';
		}
		ss << m_dirName.string() << ' ' << DiskUsageBytes(recursive) << " Bytes(r), " << FileCount(false) << " files, " << SubDirCount(false) << " subdirs, type="<< (int)m_type << " DateRange: [" << stringutils::FormatTimestamp(OldestDirTime()) << ',' << stringutils::FormatTimestamp(NewestDirTime()) << "]"<< std::endl;
		if(recursive){
			std::vector<DirTreeNode*>::const_iterator iter;
			for(iter = m_children.begin(); iter != m_children.end(); iter++){
				ss << (*iter)->PrettyPrint(recursive);
			}
		}

		return ss.str();
	}

	__int64 LogDirTreeNode::SortIndex() const{
		return (__int64) m_timestamp;
	}


	time_t LogDirTreeNode::OldestDirTime() const{
		time_t result = 0;
		
		if((int)m_children.size() > 0){
			return ((LogDirTreeNode*) m_children[0])->OldestDirTime();
		}
		
		return TimeStamp();
	}

	time_t LogDirTreeNode::NewestDirTime() const{
		time_t result = 0;

		if((int)m_children.size() > 0){
			return ((LogDirTreeNode*) m_children[m_children.size()-1])->NewestDirTime();
		}

		return TimeStamp();
	}

	fs::path LogDirTreeNode::RootDir() const{
		if(NULL == m_parent){
			return Directory();
		}
		return ((LogDirTreeNode*) m_parent)->RootDir();
	}

	fs::path LogDirTreeNode::TodayDir() const{
		return TimestampToDirPath(RootDir(),time(NULL));
	}

	fs::path LogDirTreeNode::OldestDayDir() const{
		return TimestampToDirPath(RootDir(),OldestDirTime());
	}

	time_t LogDirTreeNode::RemoveOldestDay(fs::path& p_pathRemoved_out){
		time_t timestamp = 0;
		fs::path pathRemoved("");
		
		LogDirTreeNode* oldestNode = OldestDayNode();
		if(NULL == oldestNode){
			return timestamp;
		}
		if(DAY != oldestNode->m_type){
			return timestamp;
		}

		if(oldestNode == this){
			//can't delete yourself
			return timestamp;
		}

		p_pathRemoved_out = oldestNode->FullPath();

		LogDirTreeNode* parent = (LogDirTreeNode*) oldestNode->m_parent;
		if(NULL != parent){
			timestamp = oldestNode->TimeStamp();
		}


		if(parent->RemoveChildNode(oldestNode)){
			return timestamp;
		}
		return 0;
	}

	time_t LogDirTreeNode::RemoveOldestDay()
	{
		fs::path temp;
		return RemoveOldestDay(temp);
	}

	int LogDirTreeNode::PruneEmptyChildDirectories(){
		int removalCount = 0;

		//create a "hit list" of nodes to delete
		std::vector<DirTreeNode*> nodes_to_delete;

		std::vector<DirTreeNode*>::iterator iter;
		for(iter = m_children.begin(); iter != m_children.end(); iter++){
			LogDirTreeNode* node = (LogDirTreeNode*) *iter;
			if(0 == node->FileCount(false) && 0 == node->SubDirCount(false)){
				//if the node is empty...
				//can't delete them here because it would invalidate the iterator to edit m_children
				nodes_to_delete.push_back(node);
			}
		}

		for(iter = nodes_to_delete.begin(); iter != nodes_to_delete.end(); iter++){
			
			//we can delete from m_children while iterating on temporary vector nodes_to_delete
			if(RemoveChildNode((LogDirTreeNode*) *iter)){
				removalCount++;
			}
		}

		for(iter = m_children.begin(); iter != m_children.end(); iter++){
			removalCount += ((LogDirTreeNode*) *iter)->PruneEmptyChildDirectories();
		}

		return removalCount;
	}

	bool LogDirTreeNode::RemoveChildNode(LogDirTreeNode* targetNode){
		bool success = false;
		
		std::vector<DirTreeNode*>::iterator iter;
		for(iter = m_children.begin(); iter != m_children.end(); iter++){
			LogDirTreeNode* nodePtr = (LogDirTreeNode*) *iter;
			if(nodePtr == targetNode){
				boost::system::error_code errCode;
				fs::remove_all(nodePtr->FullPath(),errCode);
				delete nodePtr;
				m_children.erase(iter);
				//iter is now invalid, break.
				success = true;
				break;
			}
		}
		return success;
	}

	LogDirTreeNode* LogDirTreeNode::OldestDayNode() const{
		if(MONTH == m_type && (int) m_children.size() > 0){
			return (LogDirTreeNode*) m_children[0];
		}
		
		if(DAY == m_type || 0 >= m_children.size()){
			return NULL;
		}

		std::vector<DirTreeNode*>::const_iterator iter;
		for(iter = m_children.begin(); iter != m_children.end(); iter++){
			LogDirTreeNode* result = ((LogDirTreeNode*)*iter)->OldestDayNode();
			if(NULL != result){
				return result;
			}
		}

		return NULL;
	}



	time_t LogDirTreeNode::InferTimestamp(DirType p_type,const std::string dirName){
		
		time_t result = 0;
		
		std::stringstream ss(dirName);
		int temp = 0;

		switch(p_type){
		case YEAR:
			if(stringutils::StringManip::lexical_cast<int>(dirName,temp) && temp > 1970 && temp < 3000){
				result = mathutils::MakeTimestamp(temp,1,1,0,0,0,false);
			}
			break;
		case MONTH:
			if(stringutils::StringManip::lexical_cast<int>(dirName,temp) && temp > 0 && temp < 13){
				result = mathutils::MakeTimestamp(1970,temp,1,0,0,0,false);
			}
			break;
		case DAY:
			if(stringutils::StringManip::lexical_cast<int>(dirName,temp) && temp > 0 && temp < 32){
				result = mathutils::MakeTimestamp(1970,1,temp,0,0,0,false);
			}
			break;
		}

		return result;
	}
}