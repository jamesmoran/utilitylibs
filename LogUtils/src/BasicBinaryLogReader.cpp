#include <iostream>
#include <process.h>
#include <sstream>

#include "BasicBinaryLogReader.h"

namespace logutils
{
	BasicBinaryLogReader::BasicBinaryLogReader():buffer(NULL)
	{

	}
	

	BasicBinaryLogReader::~BasicBinaryLogReader()
	{
		if(ifs.is_open())
			ifs.close();
 		DestroyBuffer();
	}

	bool BasicBinaryLogReader::CloseFile( const char * filename )
	{
		if(ifs.is_open()) //checks if file is open already
		{
			ifs.close();
		}

		if (ifs.is_open())
			return false; //file did not close
		else
			return true; //file closed
	}


	bool BasicBinaryLogReader::ReadNextBlock( char* &charData, unsigned int &charSize)
	{
		char* tempCharData;

		if(!flagInitialized) //quits without reading
			return false;

		if(fileLength == 0 && bufferIndex >= bufferSize )
			return false;

		if(SearchBlockHeader(buffer,bufferIndex,BLOCKSIGNATURE))
		{
			ReadBlockHeader(buffer, bufferIndex, refNumberOfDataBytes, refCheckSum);
			tempCharData = new char [refNumberOfDataBytes];
			if (ReadNextDataSet(buffer, bufferIndex, refNumberOfDataBytes, refCheckSum, dataCheckSum, tempCharData))
			{
				charData = new char[refNumberOfDataBytes];
				charSize = refNumberOfDataBytes;
				memcpy(charData, tempCharData, refNumberOfDataBytes);
				delete tempCharData;
			}
		}
		else
		{
			return false;
		}

		return true;
	}

	void BasicBinaryLogReader::CreateBuffer(size_t p_size )
	{
		buffer = new char[p_size];
	}

	void BasicBinaryLogReader::DestroyBuffer()
	{
		delete[] buffer;
	}

	void BasicBinaryLogReader::ResizeBuffer(size_t p_size )
	{
		DestroyBuffer();
		CreateBuffer(p_size);
	}
	
	void BasicBinaryLogReader::ReadBytesInBuffer( char* sourceBuffer, size_t& bufferPosition, char* destBuffer, int numberOfBytes )
	{
		for(int i = 0; i < numberOfBytes; i ++)
		{
			destBuffer[i] = sourceBuffer[bufferPosition];
			bufferPosition++;
			if (bufferPosition >= bufferSize)
			{
				if (!FillBuffer(sourceBuffer, bufferPosition))
					break;
			}
		}
	}

	bool BasicBinaryLogReader::FillBuffer( char* sourceBuffer, size_t& bufferPosition )
	{
		if (fileLength == 0)
			return false;

		else
		{
			if(fileLength > bufferLength)
			bufferSize=bufferLength;
			else
			{
				bufferSize = fileLength;
				ResizeBuffer(bufferSize);
			}
			ifs.read(buffer,bufferSize);
			bufferPosition = 0;
			fileLength -= bufferSize;
			return true;
		}
	}

	bool BasicBinaryLogReader::SearchBlockHeader( char* sourceBuffer, size_t& bufferPosition, unsigned long long blockSignature )
	{
		char* blockSignatureCheck = new char[sizeof(blockSignature)];

		while(CharArrayToUnsignedLongLong(blockSignatureCheck) != bufferSignature)
		{
			for(int i = 0; i< sizeof(blockSignature)-1; i++)
			{
				blockSignatureCheck[i] = blockSignatureCheck[i+1];
			}
			blockSignatureCheck[sizeof(blockSignature)-1] = sourceBuffer[bufferPosition];
			bufferPosition++;
			if (bufferPosition >= bufferSize)
			{
 				if (!FillBuffer(sourceBuffer, bufferPosition))//fill the buffer with the next section of the file
 				{
					delete[]blockSignatureCheck; 
					return false;
				}
			}
		}
		delete[] blockSignatureCheck;

		return true;
	}

	void BasicBinaryLogReader::ReadBlockHeader( char* sourceBuffer, size_t& bufferPosition, unsigned int& dataBytes, unsigned int& checkSum )
	{

		BasicBinaryLogger::blockHeader tempHeader;
		char* blockHeaderBuffer = new char [sizeof(tempHeader)];

		bufferPosition = bufferPosition-sizeof(tempHeader.blockSignature);

		ReadBytesInBuffer(sourceBuffer, bufferPosition, blockHeaderBuffer, sizeof(tempHeader));
		dataBytes = ((BasicBinaryLogger::blockHeader*)blockHeaderBuffer)->numberOfDataBytes;
		checkSum = ((BasicBinaryLogger::blockHeader*)blockHeaderBuffer)->checkSum;
		delete[] blockHeaderBuffer;
		
	}

	bool BasicBinaryLogReader::ReadNextDataSet( char* sourceBuffer, size_t& bufferPosition,unsigned int refDataBytes, unsigned int referenceCheckSum, unsigned int& calculatedCheckSum, char* charData )
	{
		calculatedCheckSum = 0;
		ReadBytesInBuffer(sourceBuffer, bufferPosition, charData, refDataBytes);
		calculatedCheckSum += BasicBinaryLogger::CRC(charData, refDataBytes);

		if (calculatedCheckSum == referenceCheckSum)
			return true;
		else
			return false;
	}

}