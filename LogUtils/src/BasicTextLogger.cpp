#include <cstdio>
#include <sstream>
#include <fstream>
#include <process.h>
#include <ScopedLock.h>
#include "BasicTextLogger.h"
#include <boost/filesystem.hpp>
#include <iostream>

namespace fs = boost::filesystem;

namespace logutils
{

	BasicTextLogger::BasicTextLogger(const std::string& FilePath, const std::string& VersionString, size_t MaxSizeKB)
		: Path(FilePath)
		, MaxFileSizeKB(MaxSizeKB)
		, ThreadRunning(false)
		, LastOperationStatus(NoOperation)
		, Version(VersionString)
		
	{
		ExitEvent = ::CreateEvent(0, TRUE, FALSE, 0);
		NewLogEvent = ::CreateEvent(0, FALSE, FALSE, 0);
		ThreadDoneEvent = ::CreateEvent(0, TRUE, FALSE, 0);
		::InitializeCriticalSection(&LogListLock);
		if (ExitEvent == 0 || NewLogEvent == 0)
			return;

		fs::path boostFilePath(FilePath);
		//boostFilePath.remove_filename();
		boostFilePath = boostFilePath.parent_path();
		try{
			fs::create_directories(boostFilePath);
		}catch(std::exception e){
			
		}

		if (_beginthread(WorkerThreadProc, 0, (void*)this) >= 0)
			ThreadRunning = true;
	}


	BasicTextLogger::~BasicTextLogger()
	{
		if (ThreadRunning)
			Exit();
		::DeleteCriticalSection(&LogListLock);
		::CloseHandle(ExitEvent);
		::CloseHandle(NewLogEvent);
		::CloseHandle(ThreadDoneEvent);
	}

	void BasicTextLogger::Exit()
	{
		std::cout << this->Path << "\n";
		if (!ThreadRunning)
			return;
		::SetEvent(ExitEvent);
		::WaitForSingleObject(ThreadDoneEvent, INFINITE);
	}

	void BasicTextLogger::AddText(const std::string& text)
	{
		if (!ThreadRunning)
			return;
		threadutils::ScopedLock Lock(&LogListLock);
		LogList.push_back(text);
		::SetEvent(NewLogEvent);
	}

	BasicTextLogger::OperationStatus BasicTextLogger::GetLastOperationStatus() const
	{
		threadutils::ScopedLock Lock(&LogListLock);
		return LastOperationStatus;
	}

	void BasicTextLogger::LoggerOperation()
	{
		HANDLE WakeupEvents[] = {ExitEvent, NewLogEvent};
		size_t NumEvents = 2;

		while (true)
		{
			bool Done = false;
			DWORD EventID = ::WaitForMultipleObjects(static_cast<DWORD>(NumEvents), WakeupEvents, FALSE, INFINITE);
			if (EventID == 0)
				Done = true;
			LastOperationStatus = DumpLogList();
			if (Done)
				break;
		}	
		::SetEvent(ThreadDoneEvent);
	}

	void BasicTextLogger::WorkerThreadProc(void* lpVoid)
	{
		BasicTextLogger* pLogger = (BasicTextLogger*)lpVoid;
		pLogger->ThreadRunning = true;
		pLogger->LoggerOperation();
		pLogger->ThreadRunning = false;
	}

	BasicTextLogger::OperationStatus BasicTextLogger::DumpLogList()
	{
		// Attempt to open the file.
		std::ofstream ofs;
		ofs.open(Path.c_str(), std::ios::out | std::ios::app);
		if (!ofs.is_open())
			return FileNotFound;

		size_t Length = GetFileSize(ofs);
		if (Length > MaxFileSizeKB * 1024)
		{
			ofs.close();
			if (!ArchiveFile())
				return Failed;
			ofs.open(Path.c_str(), std::ios::out);
			Length = 0;
		}

		if (Length == 0 && !Version.empty())
			ofs << Version << std::endl;

		threadutils::ScopedLock Lock(&LogListLock);
		std::list<std::string>::iterator it = LogList.begin();
		for (; it != LogList.end() ; ++it)
			ofs << *it;
		LogList.clear();
		ofs.close();
		return Success;
	}

	size_t BasicTextLogger::GetFileSize( std::ofstream& ofs ) const
	{
		std::streamoff begin, end;
		ofs.seekp(0, std::ios::beg);
		begin = ofs.tellp();
		ofs.seekp(0, std::ios::end);
		end = ofs.tellp();
		return (size_t)(end - begin);
	}

	bool BasicTextLogger::ArchiveFile() const
	{
		size_t index = Path.find_last_of('.');
		if (index == std::string::npos)
			index = Path.size();
		std::stringstream ss;
		ss << Path.substr(0, index) << "_archive" << Path.substr(index, Path.size() - index);
		std::string ArchivePath = ss.str();

		std::ofstream ArchivedFileStream;
		ArchivedFileStream.open(ArchivePath);
		if (ArchivedFileStream.is_open())
		{
			ArchivedFileStream.close();
			remove(ArchivePath.c_str());
		}
		rename(Path.c_str(), ArchivePath.c_str());
		return true;
	}

}
