#include "MessageConsumerUDP.h"

#include <sstream>

namespace logutils{

	const char MessageConsumerUDP::SEPARATOR = ';';

	MessageConsumerUDP::MessageConsumerUDP(const networkutils::Address& addr, const boost::regex& filter)
		: MessageConsumerFiltered( filter)
		, addr(addr)
	{
		m_udpSocket.Bind(addr.Port());
	}

	MessageConsumerUDP::~MessageConsumerUDP(){
		m_udpSocket.Close();
	}

	bool MessageConsumerUDP::IsBound() const{
		return m_udpSocket.IsBound();
	}

	void MessageConsumerUDP::OnMessagePassedFilter(const Message& m){
		std::stringstream ss;

		ss << "<MSG timestamp=\"" << m->timestamp << "\"";
		ss << " prefix=\"" << m->prefix << "\"";
		ss << " >";
		ss << m->text;
		ss << "</MSG>";
		
		m_udpSocket.Send(addr,ss.str().c_str(),ss.str().length()+1);
	}


}