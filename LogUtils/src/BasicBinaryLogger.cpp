#include <iostream>
#include <process.h>
#include <sstream>
#include "BasicBinaryLogger.h"
#include <ScopedLock.h>

namespace logutils
{



BasicBinaryLogger::BasicBinaryLogger(const std::string& FilePath, size_t MaxSizeKB, size_t MemoryBufferSize)
	: Path(FilePath)
	, MaxFileSizeKB(MaxSizeKB)
	, Buffer(MemoryBufferSize)
	, ThreadHandle(0)
	, UseBlockHeader(false)
{
	::InitializeCriticalSection(&LogQueueLock);
	ExitEvent = ::CreateEvent(0, TRUE, FALSE, 0);
	NewLogEvent = ::CreateEvent(0, FALSE, FALSE, 0);
	CheckFileSize();
	ThreadHandle = (HANDLE)_beginthread(WorkerThreadProc, 0, (void*)this);
}

BasicBinaryLogger::BasicBinaryLogger(const std::string& FilePath, size_t MaxSizeKB, size_t MemoryBufferSize, bool AddBlockHeader)
	: Path(FilePath)
	, MaxFileSizeKB(MaxSizeKB)
	, Buffer(MemoryBufferSize)
	, ThreadHandle(0)
	, UseBlockHeader(AddBlockHeader)
{
	::InitializeCriticalSection(&LogQueueLock);
	ExitEvent = ::CreateEvent(0, TRUE, FALSE, 0);
	NewLogEvent = ::CreateEvent(0, FALSE, FALSE, 0);
	CheckFileSize();
	ThreadHandle = (HANDLE)_beginthread(WorkerThreadProc, 0, (void*)this);
}


BasicBinaryLogger::~BasicBinaryLogger(void)
{
	// Queue up the current log buffer:
	QueueLogOperation();

	Exit();
	// Clear out any remaining logs:
	std::list<LogOperationInfo>::iterator it = LogQueue.begin();
	for (; it != LogQueue.end() ; ++it)
		delete [] (it->Buffer);
	LogQueue.clear();

}

BasicBinaryLogger::blockHeader::blockHeader():blockSignature(BLOCKSIGNATURE)
{

}

void BasicBinaryLogger::LoggerOperation()
{
	HANDLE WakeupEvents[] = { ExitEvent, NewLogEvent };
	size_t NumEvents = 2;

	while (true)
	{
		bool Done = false;
		DWORD EventID = ::WaitForMultipleObjects(static_cast<DWORD>(NumEvents), WakeupEvents, FALSE, INFINITE);
		if (EventID == 0)
			Done = true;

		//assign the block header 
		blockHeader theBlockHeader;
		LogOperationInfo ourHeader;
				
		theBlockHeader.checkSum = 0;
		theBlockHeader.numberOfDataBytes=0;
			
		// Drain the log queue:
		while (true)
		{
			threadutils::ScopedLock Lock(&LogQueueLock);
			if (LogQueue.empty())
				break;
			LogOperationInfo Log = LogQueue.front();

			if (UseBlockHeader)
			{
				theBlockHeader.checkSum = CRC(Log.Buffer, static_cast<int>(Log.BufferSize));
				theBlockHeader.numberOfDataBytes = static_cast<unsigned int>(Log.BufferSize);
				ourHeader.Buffer = (unsigned char*)&theBlockHeader;
				ourHeader.BufferSize = sizeof(blockHeader);
				WriteLog(ourHeader);
				//printf("logHeader: %llu, %u, %u\n\n",theBlockHeader.blockSignature, theBlockHeader.numberOfDataBytes, theBlockHeader.checkSum);
			}
				
			LogQueue.pop_front();
			WriteLog(Log);
			delete [] Log.Buffer;
		}
			
		// Check on the file size:
		CheckFileSize();
		if (Done)
			break;
	}		
}

void BasicBinaryLogger::WorkerThreadProc( void* lpVoid )
{
	BasicBinaryLogger* pLogger = (BasicBinaryLogger*)lpVoid;
	pLogger->LoggerOperation();
}

void BasicBinaryLogger::QueueLogOperation()
{
	// Allocate a temporary buffer
	LogOperationInfo LogInfo;
	LogInfo.Buffer = new unsigned char[Buffer.Size()];
	LogInfo.BufferSize = Buffer.Size();
	memcpy(LogInfo.Buffer, Buffer.DataBuffer(), Buffer.Size());
	EnterCriticalSection(&LogQueueLock);
	LogQueue.push_back(LogInfo);
	LeaveCriticalSection(&LogQueueLock);

	::SetEvent(NewLogEvent);

	Buffer.Clear();
}

bool BasicBinaryLogger::WriteLog(const LogOperationInfo& log)
{		
	// Attempt to open the file.
	std::ofstream ofs;
	ofs.open(Path.c_str(), std::ios::out | std::ios::binary | std::ios::app);
	if (!ofs.is_open())
		return false;	
	// Write our data:
	ofs.write((const char*)log.Buffer, log.BufferSize);
	ofs.flush();
	ofs.close();		
	return true;
}

size_t BasicBinaryLogger::GetFileSize( std::ofstream& ofs ) const
{
	std::streamoff begin, end;
	ofs.seekp(0, std::ios::beg);
	begin = ofs.tellp();
	ofs.seekp(0, std::ios::end);
	end = ofs.tellp();
	return (size_t)(end - begin);
}

bool BasicBinaryLogger::ArchiveFile() const
{
	printf("Archiving File...\n");
	size_t index = Path.find_last_of('.');
	if (index == std::string::npos)
		index = Path.size();
	std::stringstream ss;
	ss << Path.substr(0, index) << "_archive" << Path.substr(index, Path.size() - index);
	std::string ArchivePath = ss.str();

	std::ofstream ArchivedFileStream;
	ArchivedFileStream.open(ArchivePath);
	if (ArchivedFileStream.is_open())
	{
		ArchivedFileStream.close();
		remove(ArchivePath.c_str());
	}
	rename(Path.c_str(), ArchivePath.c_str());
	return true;
}

void BasicBinaryLogger::Exit()
{
	if (!ThreadHandle)
		return;
	QueueLogOperation();
	::SetEvent(ExitEvent);	
	::WaitForSingleObject(ThreadHandle, INFINITE);
	ThreadHandle = 0;
}

void BasicBinaryLogger::AddData( void* data, size_t length, bool FlushThisTime /*= false*/ )
{
	if (Buffer.Size() + length > Buffer.Capacity())
		QueueLogOperation(); // Send the current log off to disk.
	Buffer.AddData(data, length);
	if (FlushThisTime)
		QueueLogOperation();
	NextOperationOnNewFile = false;
}

bool BasicBinaryLogger::AtNewFile() const
{
	return NextOperationOnNewFile;
}

void BasicBinaryLogger::CheckFileSize()
{
	// Attempt to open the file.
	std::ofstream ofs;
	ofs.open(Path.c_str(), std::ios::out | std::ios::binary| std::ios::app);
	if (!ofs.is_open())
		return;	
	// Check on the file size:
	size_t Length = GetFileSize(ofs);
	if (Length > MaxFileSizeKB * 1024)
	{
		ofs.close();
		if (!ArchiveFile())
			return;
		NextOperationOnNewFile = true;
	}
	else if (Length == 0)
		NextOperationOnNewFile = true;
	else
		NextOperationOnNewFile = false;
}


//checksum algorithm
int BasicBinaryLogger::CRC( void* number, int lengthInBits)
{
	int sum = 0;
	unsigned char* dataPointer = (unsigned char*) number;

	for (int i=0; i<lengthInBits; i++)
	{
		sum += dataPointer[i];	
	}

	return sum;
}


bool BinaryBuffer::AddData(const void* data, size_t length )
{
	if (BufferSize + length > BufferCapacity)
		return false;
	memcpy(Buffer + BufferSize, data, length);
	BufferSize += length;
	return true;
}

BinaryBuffer::BinaryBuffer( size_t Capacity )
	: BufferCapacity(Capacity)
	, BufferSize(0)
{
	Buffer = new unsigned char[Capacity];
}

BinaryBuffer::~BinaryBuffer()
{
	delete [] Buffer;
}

void BinaryBuffer::Clear()
{
	BufferSize = 0;
}

}
