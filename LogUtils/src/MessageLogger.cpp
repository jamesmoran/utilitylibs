#include "MessageLogger.h"

#include <ScopedLock.h>
#include <assert.h>
#include <iostream>
#include <StringUtils.h>
#include <HighResTimer.h>

namespace logutils{

	MessageBody::MessageBody(const std::string& prefix, const std::string& text)
		: timestamp(time(NULL))
		, prefix(prefix)
		, text(text)
	{
	
	}

	MessageBody::MessageBody(time_t timestamp, const std::string& prefix, const std::string& text)
		: timestamp(timestamp)
		, prefix(prefix)
		, text(text)
	{

	}

	MessageConsumerBase::MessageConsumerBase()
	{

	}

	MessageConsumerBase::MessageConsumerBase( const MessageConsumerBase& c )
	{
		//this function intentionally does nothing.
	}

	void MessageConsumerBase::operator=( const MessageConsumerBase& c )
	{
		//this function intentionally does nothing.
	}


	MessageConsumerFiltered::MessageConsumerFiltered(const boost::regex& regex){
		::InitializeCriticalSection(&m_regexAccessSerializer);
		SetFilter(regex);	
	}

	MessageConsumerFiltered::~MessageConsumerFiltered(){
		::DeleteCriticalSection(&m_regexAccessSerializer);
	}

	void MessageConsumerFiltered::SetFilter(const boost::regex& regex){
		threadutils::ScopedLock lock(&m_regexAccessSerializer);
		m_regex = regex;
	}

	bool MessageConsumerFiltered::ProcessMessage(const Message& m){
		if(Filter(m->prefix)){
			OnMessagePassedFilter(m);
			return true;
		}
		return false;
	}

	void MessageConsumerFiltered::OnMessagePassedFilter(const Message& m){
		
	}

	bool MessageConsumerFiltered::Filter(const std::string prefix){
		threadutils::ScopedLock lock(&m_regexAccessSerializer);
		return boost::regex_match(prefix,m_regex);
	}

	MessageConsumerStdout::MessageConsumerStdout(const boost::regex& regex)
		: MessageConsumerFiltered(regex)
	{
	
	}

	void MessageConsumerStdout::OnMessagePassedFilter(const Message& m){
		std::cout << stringutils::FormatTimestamp(m->timestamp) << '[' << m->prefix << ']' << m->text << std::endl;
	}

	MessageLogger::MessageLogger(int maxQueuedMsgs)
		: m_newDataAvailable(false,false)
		, m_messagesProcessed(0)
		, m_messageBatchesProcessed(0)
		, m_messagesDropped(0)
		, m_maxQueuedMsgs(maxQueuedMsgs)
		, m_thread_exited(true)
	{
		::InitializeCriticalSection(&m_msgQueueAccessSerializer);
		::InitializeCriticalSection(&m_consumerMapAccessSerializer);
	}

	MessageLogger::~MessageLogger(){
		
		//kill the thread and wait for it to die: it's not safe to destroy the Crit. Sections below until the thread is done.
		//HANDLE h = gethandle();
		if (!_isDying)
			_isDying++;
		m_newDataAvailable.SetEvent();
		WaitForDeath(1000);
		/*
		if (!_isDying){
			m_newDataAvailable.SetEvent();
			Kill(1000);	
		}else{
			m_newDataAvailable.SetEvent();
			WaitForDeath(1000);
		}
		*/

		//if (!m_thread_exited)
		//	WaitForSingleObject(h, INFINITE);

		//::DeleteCriticalSection(&m_msgQueueAccessSerializer);
		//::DeleteCriticalSection(&m_consumerMapAccessSerializer);

		std::cout << "MessageLogger shutdown: " << m_messagesProcessed << " messages processed, " << m_messagesDropped << " messages dropped\n";
	}

	void MessageLogger::Start(){
		m_thread_exited = false;
		_thread.Resume();
	}

	void MessageLogger::Log(const std::string& prefix, const std::string& text){
		
		//Create a new Message
		Message newMessage(new MessageBody(prefix,text));

		//lock the message queue
		::EnterCriticalSection(&m_msgQueueAccessSerializer);

		//guard against queue overflow
		if((int) m_msgQueue.size() < m_maxQueuedMsgs){
			//enqueue the new message
			m_msgQueue.push_back(newMessage);

			//signal that new data is available
			m_newDataAvailable.SetEvent();
		}else{
			m_messagesDropped++;
		}

		//unlock the message queue
		::LeaveCriticalSection(&m_msgQueueAccessSerializer);

	}

	bool MessageLogger::AttachConsumer(const std::string& name, MessageConsumerBase* consumer)
	{
		return AttachConsumer(name, std::shared_ptr<MessageConsumerBase>(consumer));
	}

	bool MessageLogger::AttachConsumer(const std::string& name, std::shared_ptr<MessageConsumerBase> consumer)
	{
		assert(NULL != consumer);

		threadutils::ScopedLock lock(&m_consumerMapAccessSerializer);

		std::map<std::string, std::shared_ptr<MessageConsumerBase>>::const_iterator result = m_consumerMap.find(name);
		if(m_consumerMap.end() != result){
			//consumer by that name already exists, return false;
			return false;
		}

		m_consumerMap[name] = consumer;

		return true;
	}

	bool MessageLogger::DetachConsumer(const std::string& name){
		
		threadutils::ScopedLock lock(&m_consumerMapAccessSerializer);

		std::map<std::string, std::shared_ptr<MessageConsumerBase>>::const_iterator result = m_consumerMap.find(name);
		if(m_consumerMap.end() == result){
			//consumer by that name does not exist, return false
			return false;
		}

		m_consumerMap.erase(result);

		return true;
	}
	
	void MessageLogger::DetachAllConsumers(){
		threadutils::ScopedLock lock(&m_consumerMapAccessSerializer);
		m_consumerMap.clear();
	}
	

	bool MessageLogger::ConsumerExists(const std::string& name) const{
		threadutils::ScopedLock lock(&m_consumerMapAccessSerializer);
		std::map<std::string, std::shared_ptr<MessageConsumerBase>>::const_iterator result = m_consumerMap.find(name);
		return m_consumerMap.end() != result;
	}


	__int64 MessageLogger::MessagesProcessed() const{
		return m_messagesProcessed;
	}

	__int64 MessageLogger::MessageBatchesProcessed() const{
		return m_messageBatchesProcessed;
	}

	void MessageLogger::InitThread(){
		
	}

	void MessageLogger::Run(){
		m_thread_exited = false;
		//create a local queue that will exchange data with the main message queue.
		std::deque<Message> localMessageQueue;
		
		//create a high-res timer that will keep tabs on the load level of the logger
		threadutils::high_res_timer timer;

		//run while the _isDying flag is zero
		while(!_isDying){

			//wait to see if new data is available
			WaitForSingleObject(m_newDataAvailable,INFINITE);
			if (_isDying){
				//DetachAllConsumers();
				m_thread_exited = true;
				return;
			}
			//get the time spent idle (waiting) 
			double timeIdle = timer.get_lap_msecs();

			//lock the message queue
			::EnterCriticalSection(&m_msgQueueAccessSerializer);

			//swap the local (empty) queue with the (full) message queue
			localMessageQueue.swap(m_msgQueue);

			//unlock the message queue
			::LeaveCriticalSection(&m_msgQueueAccessSerializer);


			//lock the consumer map
			::EnterCriticalSection(&m_consumerMapAccessSerializer);

			//iterate over the messages
			std::deque<Message>::const_iterator msg_iter;
			for(msg_iter = localMessageQueue.begin(); msg_iter != localMessageQueue.end() && !_isDying; msg_iter++){
				//iterate over the consumers
				std::map<std::string, std::shared_ptr<MessageConsumerBase>>::iterator consumer_iter;
				for(consumer_iter = m_consumerMap.begin(); consumer_iter != m_consumerMap.end(); consumer_iter++){
					//pass the message to the consumer
					consumer_iter->second->ProcessMessage(*msg_iter);
				}

				m_messagesProcessed++;
			}

			//unlock the consumer map
			::LeaveCriticalSection(&m_consumerMapAccessSerializer);

			//clear the local queue
			localMessageQueue.clear();

			m_messageBatchesProcessed++;

			double timeWorking = timer.get_lap_msecs();
			double loadLevel = timeWorking / (timeWorking + timeIdle);
		}

		//DetachAllConsumers();
		m_thread_exited = true;
	}

	void MessageLogger::FlushThread(){
	
	}


}
