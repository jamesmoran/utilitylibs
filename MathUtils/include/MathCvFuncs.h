#pragma once

#include <opencv/cv.h>
#include "CoreDefines.h"

namespace mathutils{

	MMI_EXPORT CvPoint2D32f Normalize(const CvPoint2D32f& p_vector);
	MMI_EXPORT double Norm2(const CvPoint2D32f& p_vector);

	MMI_EXPORT double Dot(const CvPoint2D32f& p_vector1, const CvPoint2D32f& p_vector2);

}
