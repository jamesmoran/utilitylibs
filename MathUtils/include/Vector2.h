#pragma once

namespace mathutils{

template<typename T>
struct Vector2
{
public:
	T X;
	T Y;

	Vector2()
	{
		X = Y = (T)0;
	}

	Vector2(T x, T y)
		: X(x)
		, Y(y)
	{
	}

	static Vector2<T> FromAngle(const T& Length, const T& AngleRad)
	{
		return Vector2<T>(Length * cos(AngleRad), Length * sin(AngleRad));
	}
    
	T Dot(const Vector2<T>& right) const
	{
		return X * right.X + Y * right.Y;
	}

	T Determinant(const Vector2<T>& vec) const
	{
		return X * vec.Y - Y * vec.X;
	}

	T Length() const
	{
		return sqrt(X*X + Y*Y);
	}

	Vector2<T> Unit() const
	{
		return (*this) / this->Length();
	}

	Vector2<T> Perpendicular() const
	{
		return Vector2<T>(-Y, X);
	}

	Vector2<T> operator*(const Vector2<T>& right) const
	{
		return Vector2<T>(X * right.X, Y * right.Y);
	}

	Vector2<T> operator*(T scalar) const
	{
		return Vector2<T>(scalar * X, scalar * Y);
	}

	Vector2<T> operator/(const Vector2<T>& right) const
	{
		return Vector2<T>(X / right.X, Y / right.Y);
	}

	Vector2<T> operator/(T scalar) const
	{
		return Vector2<T>(X / scalar, Y / scalar);
	}

	Vector2<T> operator+(const Vector2<T>& right) const
	{
		return Vector2<T>(this->X + right.X, this->Y + right.Y);
	}

	Vector2<T> operator-(const Vector2<T>& right) const
	{
		return Vector2<T>(this->X - right.X, this->Y - right.Y);
	}

	bool operator==(const Vector2<T>& right) const
	{
		return X == right.X && Y == right.Y;
	}

	bool operator!=(const Vector2<T>& right) const
	{
		return X != right.X || Y != right.Y;
	}
};

typedef Vector2<float> Vector2F;
typedef Vector2<int> Vector2I;
typedef Vector2<double> Vector2D;

}