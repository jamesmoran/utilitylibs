#pragma once

#include <utility>
#include <vector>
#include "CoreDefines.h"

namespace mathutils{

	class GaussianDist{
	public:

		MMI_EXPORT GaussianDist(double mu, double sigma);

		MMI_EXPORT double Mu() const;
		MMI_EXPORT double Sigma() const;
		MMI_EXPORT double Var() const;
		MMI_EXPORT double P(double x) const;
		
		MMI_EXPORT static double PGaussian(double x, double mu, double sigma);

	protected:

		double m_mu;
		double m_sigma;
	
	};

	template<class T> 
	MMI_EXPORT std::pair<double,double> GaussianConsensus( const T& p_data , double sigma){
		if(p_data.size() <= 0){
			return std::pair<double,double>(0,0);
		}

		auto minIndex = p_data.begin();
		double minSum = DBL_MAX;
		auto maxIndex = p_data.begin();
		double maxSum = 0;

		for(auto x=p_data.begin(); x != p_data.end(); x++){

			double sum = 0;
			for(auto mu = p_data.begin(); mu != p_data.end(); mu++){
				sum += GaussianDist::PGaussian(*x,*mu,sigma);
			}

			if(sum >= maxSum){
				maxSum = sum;
				maxIndex = x;
			}

			if(sum <= minSum){
				minSum = sum;
				minIndex = x;
			}
	
		}

		double spread = (maxSum - minSum) / (double) p_data.size();

		return std::pair<double,double>(*maxIndex,spread);
	}

}