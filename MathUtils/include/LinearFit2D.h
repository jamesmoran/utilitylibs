#pragma once

#include "Vector2.h"
#include <vector>
#include "CoreDefines.h"

namespace mathutils{

	/*	Class: LinearFit2D
		A class representing a linear y = ax+b relationship, complete with 
		a simple linear regression function Fit() for an arbitrary collection of
		XY points (size >= 2).
	*/
	class LinearFit2D{
	
	public:

		/*	typedef: LinearFit2DData
			An STL vector container of Vector2F XY points.
		*/
		typedef std::vector<Vector2F> LinearFit2DData;

		/*	Method: LinearFit2D
			Constructor, creates an initially invalid LinearFit object, awaiting a manual
			call to the <Fit> method.
		*/
		MMI_EXPORT LinearFit2D();

		/*	Method: LinearFit2D
			Constructor, computes the linear fit immediately against the supplied data in
			the same manner as the <Fit> method.
			
			Parameters:
				p_data - The collection of Vector2F XY points to which to fit. Valid fits
				require at least 2 points.
		*/
		MMI_EXPORT LinearFit2D(const LinearFit2DData& p_data);

		/*	Method: ~LinearFit2D
			Destructor
		*/
		MMI_EXPORT ~LinearFit2D();

		/*	Method: Fit
			Attempts to fit the linear model y = ax+b to the supplied data using a closed-form least squares
			solution.
		
			Parameters:
				p_data - The collection of Vector2F XY points to which to fit. Valid fits
				require at least 2 points.

			Return:
				bool - Returns true if a valid fit was computed. Returns false if the data have
				a range in x of 0, or fewer than 2 points were supplied.
		*/
		MMI_EXPORT bool Fit(const LinearFit2DData& p_data);

		/*	Method: A
			Return the slope coefficient A.
		*/
		MMI_EXPORT double A() const;

		/*	Method: B
			Return the y-intercept value B.
		*/
		MMI_EXPORT double B() const;

		/*	Method: R
			Return the coefficient of determination R. Values are in the range [0,1] where a value of 1 indicates
			a perfect linear relationship between the fitted points. Lower values indicate increasing error w.r.t.
			a linear model.
		*/
		MMI_EXPORT double R() const;

		/*	Method: Y
			Returns the value of Y corresponding to the specified X value. 
		*/
		MMI_EXPORT double Y(double x) const;

		/*	Method: XMin
			Returns the minimum value of X observed in the data set.
		*/
		MMI_EXPORT double XMin() const;

		/*	Method: XMax
			Returns the maximum value of X observed in the data set.
		*/
		MMI_EXPORT double XMax() const;

		/*	Method: Valid
			Returns true if <Fit> has been successfully run, and thus the <A> and <B> values
			represent a fitted line.
		*/
		MMI_EXPORT bool Valid() const;
	
	protected:
		double a;
		double b;
		double r;
		bool valid;
		double xMin;
		double xMax;
	};
}