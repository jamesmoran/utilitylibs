#pragma once

#include <vector>

#include "Vector2.h"
#include "CoreDefines.h"

namespace mathutils{

template<typename T>
struct Polygon2
{
private:
	std::vector<Vector2<T> > m_Points;
	mutable Vector2<T> m_Centroid;
	mutable T m_Radius;
	mutable bool m_Updated;
public:

	template<typename T>
	MMI_EXPORT static Polygon2<T> ClippedRectangle(const Vector2<T>& center, T width, T height, T clip)
	{
		Rectangle<T> rect(center.X - width / (T)2, center.Y - height / (T)2, width, height);
		Polygon2<T> clipped;
		clipped.AddPoint(Vector2<T>(rect.Left, rect.Top + clip));
		clipped.AddPoint(Vector2<T>(rect.Left + clip, rect.Top));
		clipped.AddPoint(Vector2<T>(rect.Right() - clip, rect.Top));
		clipped.AddPoint(Vector2<T>(rect.Right(), rect.Top + clip));
		clipped.AddPoint(Vector2<T>(rect.Right(), rect.Bottom() - clip));
		clipped.AddPoint(Vector2<T>(rect.Right() - clip, rect.Bottom()));
		clipped.AddPoint(Vector2<T>(rect.Left + clip, rect.Bottom()));
		clipped.AddPoint(Vector2<T>(rect.Left, rect.Bottom() - clip));
		return clipped;
	}		

	MMI_EXPORT Polygon2()
		: m_Updated(false)
	{
	}

	MMI_EXPORT Polygon2<T> Translate(const Vector2<T>& translation) const
	{
		Polygon2<T> result;
		for (size_t n=0 ; n<m_Points.size() ; ++n)
			result.m_Points.push_back(m_Points[n] + translation);
		return result;
	}

	MMI_EXPORT void AddPoint(const Vector2<T>& point)
	{
		m_Points.push_back(point);
		m_Updated = false;
	}

	MMI_EXPORT const Vector2<T>& operator()(size_t index) const
	{
		return m_Points[index];
	}

	MMI_EXPORT Vector2<T>& operator()(size_t index)
	{
		return m_Points[index];
	}

	MMI_EXPORT size_t PointCount() const
	{
		return m_Points.size();
	}

	MMI_EXPORT size_t EdgeCount() const
	{
		return m_Points.size();
	}

	MMI_EXPORT Vector2<T> Edge(size_t n) const
	{
		size_t n1 = n;
		size_t n2 = (n + 1) % m_Points.size();
		return Vector2<T>(m_Points[n2].X - m_Points[n1].X, m_Points[n2].Y - m_Points[n1].Y);
	}

	MMI_EXPORT std::pair<T, T> Project(const Vector2<T>& axis) const
	{
		std::pair<T, T> result;
		if (m_Points.empty())
			return result;
		T dot = m_Points[0].Dot(axis);
		result.first = dot;
		result.second = dot;
		for (size_t n=1 ; n<m_Points.size() ; ++n)
		{
			dot = m_Points[n].Dot(axis);
			if (dot < result.first)
				result.first = dot;
			if (dot > result.second)
				result.second = dot;
		}
		return result;
	}

	MMI_EXPORT const T& Radius() const
	{
		UpdateInternals();
		return m_Radius;
	}

	MMI_EXPORT Vector2<T> Centroid() const
	{
		UpdateInternals();
		return m_Centroid;
	}

private:
	void UpdateInternals() const
	{
		if (m_Updated)
			return;
		m_Radius = (T)0;
		m_Centroid = Vector2<T>((T)0, (T)0);
		m_Updated = true;
		if (m_Points.size() < 3)
			return;
		T area = (T)0;
		size_t n;
		for (n=0 ; n<m_Points.size() - 1 ; ++n)
		{
			double a = (m_Points[n].X * m_Points[n + 1].Y) - (m_Points[n].Y * m_Points[n + 1].X);
			area += a;
			m_Centroid.X += (m_Points[n].X + m_Points[n + 1].X) * a;
			m_Centroid.Y += (m_Points[n].Y + m_Points[n + 1].Y) * a;
		}

		double a = (m_Points[n].X * m_Points[0].Y) - (m_Points[n].Y * m_Points[0].X);
		area += a;
		m_Centroid.X += (m_Points[n].X + m_Points[0].X) * a;
		m_Centroid.Y += (m_Points[n].Y + m_Points[0].Y) * a;
		area = 0.5 * area;
		m_Centroid = m_Centroid / (6 * area);

		for (n=0 ; n<m_Points.size() ; ++n)
		{
			double dist = (m_Points[n] - m_Centroid).Length();
			if (dist > m_Radius)
				m_Radius = dist;
		}
	}
};

typedef Polygon2<float> Polygon2F;
typedef Polygon2<int> Polygon2I;

}