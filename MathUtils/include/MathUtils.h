#pragma once

#include <MathConsts.h>
#include <Vector2.h>
#include <Polygon.h>
#include <Rectangle.h>
#include <LineSegment.h>
#include <MathUtilFuncs.h>
#include <spline2D.h>
#include <MathUtilFuncs.h>
#include <GaussianDist.h>


/*
	namespace: mathutils 
	Namespace for in-house math utility functions.	
*/