#pragma once

#include <opencv/cv.h>
#include <vector>
#include "CoreDefines.h"

namespace mathutils{

	typedef std::vector<int>::const_iterator iterInt;

	struct ordering 
	{
		MMI_EXPORT bool operator ()(std::pair<size_t, iterInt> const& a, std::pair<size_t, iterInt> const& b) ; 
	};

	struct pairOrdering 
	{
		MMI_EXPORT bool operator ()(std::pair<float, float> const& a, std::pair<float, float> const& b) ; 
	};

	struct compareRect 
	{
		MMI_EXPORT bool operator ()(CvRect const& a, CvRect const& b) ; 
	};


	class MathNet{
	public:

		MMI_EXPORT float Tanh( float a );

		MMI_EXPORT float HardSigmoid( float x ) ;

		MMI_EXPORT float Sigmoid( float x ) ;

		MMI_EXPORT float Sigmoid2( float x ) ;

		MMI_EXPORT float FastSigmoid( float x ) ;

		// MMI_EXPORT ramp function (Might be used for ReLU activations).
		MMI_EXPORT float Ramp(float x) ;

		template <typename T>
		std::vector<T> sort_from_ref( std::vector<T> const& in, std::vector<std::pair<size_t, iterInt> > const& reference);



	};

}