#pragma once

#include "Vector2.h"

namespace mathutils{

template<typename T>
struct Rectangle
{
public:
	T Left;
	T Top;
	T Width;
	T Height;

	Rectangle()
	{
		Left = Top = Width = Height = (T)0;
	}

	Rectangle(T left, T top, T width, T height)
		: Left(left)
		, Top(top)
		, Width(width)
		, Height(height)
	{
	}

	Rectangle(const Rectangle<T>& other)
	{
		Copy(other);
	}

	const Rectangle<T>& operator=(const Rectangle<T>& other)
	{
		if (&other != this)
		{
			Copy(other);
		}
		return *this;
	}

	bool ContainsPoint(const Vector2<T>& point) const
	{
		return point.X > Left && point.X < Right() &&
			point.Y > Top && point.Y < Bottom();
	}

	bool Intersects(const Rectangle<T>& other) const
	{
		return !(other.Left > this->Right() || 
			other.Right() < this->Left || 
			other.Top > this->Bottom() ||
			other.Bottom() < this->Top);
	}

	Vector2<T> TopLeft() const
	{
		return Vector2<T>(Left, Top);
	}

	Vector2<T> TopRight() const
	{
		return Vector2<T>(Right(), Top);
	}

	Vector2<T> BottomRight() const
	{
		return Vector2<T>(Right(), Bottom());
	}

	Vector2<T> BottomLeft() const
	{
		return Vector2<T>(Left, Bottom());
	}

	Vector2<T> Center() const
	{
		return Vector2<T>(Left + Width / (T)2, Top + Height / (T)2);
	}

	T Right() const
	{
		return Left + Width;
	}

	T Bottom() const
	{
		return Top + Height;
	}

	Rectangle<T> Translate(const Vector2<T>& translation)
	{
		return Rectangle<T>(this->Left + translation.X, this->Top + translation.Y, this->Width, this->Height);
	}

	static Rectangle<T> BoundingBox(const Rectangle<T>& r1, const Rectangle<T>& r2)
	{
		// OPTIMIZE
		T l, r, t, b;
		l = r1.Left < r2.Left ? r1.Left : r2.Left;
		r = r1.Right() > r2.Right() ? r1.Right() : r2.Right();
		t = r1.Top < r2.Top ? r1.Top : r2.Top;
		b = r1.Bottom() > r2.Bottom() ? r1.Bottom() : r2.Bottom();
		return Rectangle<T>(l, t, r - l, b - t);
	}

private:
	void Copy(const Rectangle<T>& other)
	{
		this->Left = other.Left;
		this->Top = other.Top;
		this->Width = other.Width;
		this->Height = other.Height;
	}
};

typedef Rectangle<float> RectangleF;
typedef Rectangle<int> RectangleI;

}