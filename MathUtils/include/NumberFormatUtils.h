#pragma once
#include <iomanip>
#include <locale>
#include "CoreDefines.h"

namespace mathutils{

	template<class T>
	MMI_EXPORT std::string numberToString(T value, unsigned int nPrecision)
	{
		std::stringstream ss;
		ss << std::setprecision(nPrecision) << std::fixed << value;
		return ss.str();
	}

	struct comma_numpunct : public std::numpunct<char>
	{
	protected:
		virtual char do_thousands_sep() const
		{
			return ',';
		}

		virtual std::string do_grouping() const
		{
			return "\03";
		}
	};

	struct space_numpunct : public std::numpunct<char>
	{
	protected:
		virtual char do_thousands_sep() const
		{
			return ' ';
		}

		virtual std::string do_grouping() const
		{
			return "\03";
		}
	};

	template<class T>
	MMI_EXPORT std::string numberFormatWithDefaultLocale(T value, unsigned int nPrecision)
	{
		std::stringstream ss;
		ss.imbue(std::locale(""));
		ss << std::setprecision(nPrecision) << std::fixed << value;
		return ss.str();
	};

	template<class T>
	MMI_EXPORT std::string numberFormatWithCommas(T value, unsigned int nPrecision)
	{
		struct Numpunct: public std::numpunct<char>{
		protected:
			virtual char do_thousands_sep() const{return ',';}
			virtual std::string do_grouping() const{return "\03";}
		};
		//std::locale comma_locale(std::locale(), new comma_numpunct());
		std::stringstream ss;
		std::locale comma_locale(std::locale(), new Numpunct());
		ss.imbue(comma_locale);
		ss << std::setprecision(nPrecision) << std::fixed << value;
		return ss.str();
	};

	template<class T>
	MMI_EXPORT std::string numberFormatWithSpace(T value, unsigned int nPrecision)
	{
		struct Numpunct: public std::numpunct<char>
		{
		protected:
			virtual char do_thousands_sep() const{return ' ';}
			virtual std::string do_grouping() const{return "\03";}
		};

		std::stringstream ss;
		std::locale space_locale(std::locale(), new Numpunct());
		ss.imbue(space_locale);
		ss << std::setprecision(nPrecision) << std::fixed << value;
		return ss.str();
	};
}