#pragma once

#include <vector>
#include <time.h>
#include "CoreDefines.h"

namespace mathutils{

	/*	Method: MakeTimestamp
		Create a time_t timestamp to reflect the date and time. This function does not correct for
		time zones.

		Parameters:
			year - The gregorian calendar year. E.g. 2013
			month - The month, starting at 1 for January
			day - The day of the month, starting at 1.
			hour - The hour, in 24 hour notation, starting at 0 for the hour after midnight.
			minute - The minute.
			second - The second.
			is_dst - Boolean flag indicating if the values entered represent a Daylight Savings Time
			corrected value or not. This information is necessary to invoke the appropriate conversion.
	
		Return:
			time_t - The timestamp (seconds since the unix epoch) corresponding to the entered date and time.
	*/
	MMI_EXPORT time_t MakeTimestamp(int year, int month, int day, int hour, int minute, int second, bool is_dst);


	/*
		Function: clamp
		Forces a value into a specified range. valMin must be <= valMax.
		Return: a value that is equal to val unless val is not in the range [valMin,valMax] inclusive.
		If val is outside the range, the closest in-range value is returned.
	*/
	template <class T>
	MMI_EXPORT T clamp(T val, T valMin, T valMax){
		if(val < valMin){return valMin;}
		if(val > valMax){return valMax;}
		return val;
	}

	MMI_EXPORT double Terabytes(__int64 bytes);
	MMI_EXPORT double Gigabytes(__int64 bytes);
	MMI_EXPORT double Megabytes(__int64 bytes);
	MMI_EXPORT double Kilobytes(__int64 bytes);


	MMI_EXPORT unsigned int log2( unsigned int zvalue );

	/*
		Function: round
		Rounds val using standard 0.5-pivoted rounding, returning an integer.
	*/
	MMI_EXPORT int round(double val);

	/*
		Function: linear_interpolate
		Interpolates between (or beyond) values [y0,y1], according to position of
		reference x in range [x0,x1]. Interpolation is linear and continues outside of
		range [y0,y1]. x0 must not equal x1.
	*/
	MMI_EXPORT double linear_interpolate(double x, double x0, double x1, double y0, double y1);
	
	/*
		Function: linear_saturation
		Interpolates strictly between values [y0,y1], according to position of
		reference x in range [x0,x1]. Interpolation is linear and results are clamped
		into range [y0,y1]. x0 must not equal x1.
	*/
	MMI_EXPORT double linear_saturation(double x, double x0, double x1, double y0, double y1);

	/*
		Function: sine_saturation
		Interpolates strictly between values [y0,y1], according to position of
		reference x in range [x0,x1]. Interpolation uses a sinusoidal function and 
		results are clamped into range [y0,y1]. x0 must not equal x1.
	*/
	MMI_EXPORT double sine_saturation(double x, double x0, double x1, double y0, double y1);

	/*
		Function: array_mean
		Computes the mean of an array of integers.
	*/
	MMI_EXPORT double array_mean(int* values, int count);

	/*
		Function: array_stddev
		Computes the standard deviation of an array of integers. Computes the mean internally.
	*/
	MMI_EXPORT double array_stddev(int* values, int count);
	
	/*
		Function: array_stddev
		Computes the standard deviation of an array of integers relative to a user-supplied
		mean. This permits efficient computation of both mean and stddev by the user.
	*/
	MMI_EXPORT double array_stddev(int* values, int count, double mean);

	/*
		Function: vector_mean
		Computes the mean of a vector of double.
	*/
	MMI_EXPORT double vector_mean(std::vector<double>& values);
	
	/*
		Function: vector_stddev
		Computes the std deviation of a vector of double
	*/
	MMI_EXPORT double vector_stddev(std::vector<double>& values);
	
	/*
		Function: vector_stddev
		Computes the standard deviation of a vector of double relative to a user-supplied
		mean. This permits efficient computation of both mean and stddev by the user.
	*/
	MMI_EXPORT double vector_stddev(std::vector<double>& values, double mean);

	/*
		Function: vector_com
		Computes the center of mass of a vector of double
	*/
	MMI_EXPORT double vector_com(std::vector<double>& values);					//center of mass of a distribution
	
	/*
		Function: vector_spread
		Computes the squared difference between the values and the center of mass.
	*/
	MMI_EXPORT double vector_spread(std::vector<double>& values);				//volume spread of a distribution
	
	/*
		Function: vector_spread
		Computes the squared difference between the values and the user-supplied center of mass.
	*/
	MMI_EXPORT double vector_spread(std::vector<double>& values,double com);
	
	/*
		Function: vector_minmax
		Computes the minimum and maximum of the vector, and optionally supplies the index of the 
		minimum and maximum.
		Parameters:
		  - values: the vector of values
		  - minval: a pointer to a double that is filled with the  minimum value from the vector. Ignored if NULL.
		  - maxval: a pointer to a double that is filled with the minimum value from the vector. Ignored if NULL.
		  - minpos: a pointer to an int that is filled with the index of the minimum value in the vector. Ignored if NULL.
		  - maxpos: a pointer to an int that is filled with the index of the maximum value in the vector. Ignored if NULL.
	*/
	MMI_EXPORT void vector_minmax(std::vector<double>& values, double* minval, double* maxval, int* minpos, int* maxpos);
	
	/*
		Function: vector_median
		Computes the unweighted median of a vector of double.
	*/
	MMI_EXPORT double vector_median(std::vector<double>& values);

	/*
		Function: overLine
		Predicate: computes if a point x,y is above or below a line defined by y=ax+b
	*/
	MMI_EXPORT bool overLine(double x,double y,double a,double b);

	/*
	 Function: getSumAlongDimension

		Sums up a matrix based on the dimension.

	 Parameters:

		matrix - The matrix.
		dimension - The dimension. A 0 means we sum the whole matrix, a 1 means we sum up the rows, a 2 means we sum up the columns. Default 1.
		&result - A vector with the sum of each row or column.
		&result2 - A scalar with the sum of the whole matrix, if dimension = 0.

	*/
	template <class T>
	bool getSumAlongDimension(std::vector<std::vector<T>> matrix, std::vector<T> & result, T & result2, int dimension = 1)
	{
		if(matrix.size() < 1)
		{
			return false;
		}

		int rowlength = matrix[0].size();
		for(int i = 0; i < matrix.size(); ++i)
		{
			if(matrix[i].size() != rowlength)
			{
				return false;
			}
		}

		T sum = 0;
		result.clear();

		if(dimension == 1 || dimension == 0)
		{

			for(int i = 0; i < matrix.size(); ++i)
			{
				if(dimension == 1)
				{
					sum = 0;
				}
				for(int j = 0; j < matrix[i].size(); ++j)
				{
					sum += matrix[i][j];
				}
				if(dimension == 1)
				{
					result.push_back(sum);
				}
			}
		}
		
		if(dimension == 2 || dimension == 0)
		{

			for(int i = 0; i < matrix[0].size(); ++i)
			{
				if(dimension == 2)
				{
					sum = 0;
				}
				for(int j = 0; j < matrix.size(); ++j)
				{
					sum += matrix[j][i];
				}
				if(dimension == 2)
				{
					result.push_back(sum);
				}
			}
		}
		if(dimension == 0)
		{
			result2 = sum;
		}
		return true;
	}

	template <class T>
	bool getSumAlongDimension(T ** matrix, int row, int col, T * result, T & result2, int dimension = 1)
	{
		if(row < 1 || col < 1)
		{
			return false;
		}

		T sum = 0;
		if(dimension == 1)
		{
			result = new T[row];
		}
		else
		{
			result = new T[col];
		}
		if(dimension == 1 || dimension == 0)
		{

			for(int i = 0; i < row; ++i)
			{
				if(dimension == 1)
				{
					sum = 0;
				}
				for(int j = 0; j < col; ++j)
				{
					sum += matrix[i][j];
				}
				if(dimension == 1)
				{
					result[i] = sum;
				}
			}
		}

		if(dimension == 2 || dimension == 0)
		{

			for(int i = 0; i < col; ++i)
			{
				if(dimension == 2)
				{
					sum = 0;
				}
				for(int j = 0; j < row; ++j)
				{
					sum += matrix[j][i];
				}
				if(dimension == 2)
				{
					result[i] = sum;
				}
			}
		}
		if(dimension == 0)
		{
			result2 = sum;
		}
		return true;
	}

	/*
	 Function: getMax

		Gets the maximum value in a matrix and its location.

	 Parameters:

		matrix - The matrix.
		&points - A vector with the index pair(s) of each maximum point.
		&max - The maximum value of the matrix.

	*/
	template <class T>
	bool getMax(std::vector<std::vector<T>> matrix, std::vector<std::vector<int>> & points, T & max)
	{
		if(matrix.size() < 1)
		{
			return false;
		}

		int rowlength = matrix[0].size();
		for(int i = 0; i < matrix.size(); ++i)
		{
			if(matrix[i].size() != rowlength)
			{
				return false;
			}
		}

		max = INT_MIN;
		points.clear();

		for(int i = 0; i < matrix.size(); ++i)
		{
			for(int j = 0; j < matrix[i].size(); ++j)
			{
				if(matrix[i][j] > max)
				{
					max = matrix[i][j];
					points.clear();
					std::vector<int> temp (2);
					temp[0] = i;
					temp[1] = j;
					points.push_back(temp);
				}
				else if(matrix[i][j] == max)
				{
					std::vector<int> temp (2);
					temp[0] = i;
					temp[1] = j;
					points.push_back(temp);
				}
			}
		}
	}

	template <class T>
	bool getMax(T ** matrix, int row, int col, std::vector<std::vector<int>> & points, T & maxval)
	{
		if(row < 1 || col < 1)
		{
			return false;
		}

		maxval = INT_MIN;
		points.clear();

		for(int i = 0; i < row; ++i)
		{
			for(int j = 0; j < col; ++j)
			{
				if(matrix[i][j] == maxval)
				{	
					std::vector<int> temp (2);
					temp[0] = i;
					temp[1] = j;
					points.push_back(temp);
				}
				else if(matrix[i][j] > maxval)
				{
					points.clear();
					std::vector<int> temp (2);
					temp[0] = i;
					temp[1] = j;
					points.push_back(temp);
					maxval = matrix[i][j];
				}
			}
		}
	}
	/*
	 Function: getMin

		Gets the minimum value in a matrix and its location.

	 Parameters:

		matrix - The matrix.
		&points - A vector with the index pair(s) of each minimum point.
		&min - The minimum value of the matrix.

	*/
	template <class T>
	bool getMin(std::vector<std::vector<T>> matrix, std::vector<std::vector<int>> & points, T & minval)
	{
		if(matrix.size() < 1)
		{
			return false;
		}

		int rowlength = matrix[0].size();
		for(int i = 0; i < matrix.size(); ++i)
		{
			if(matrix[i].size() != rowlength)
			{
				return false;
			}
		}

		minval = INT_MAX;
		points.clear();

		for(int i = 0; i < matrix.size(); ++i)
		{
			for(int j = 0; j < matrix[i].size(); ++j)
			{
				if(matrix[i][j] < minval)
				{
					minval = matrix[i][j];
					points.clear();
					std::vector<int> temp (2);
					temp[0] = i;
					temp[1] = j;
					points.push_back(temp);
				}
				else if(matrix[i][j] == minval)
				{
					std::vector<int> temp (2);
					temp[0] = i;
					temp[1] = j;
					points.push_back(temp);
				}
			}
		}
	}
	template <class T>
	bool getMin(T ** matrix, int row, int col, std::vector<std::vector<int>> & points, T & minval)
	{
		if(row < 1 || col < 1)
		{
			return false;
		}

		minval = INT_MAX;
		points.clear();
			
		for(int i = 0; i < row; ++i)
		{
			for(int j = 0; j < col; ++j)
			{
				if(matrix[i][j] == minval)
				{	
					std::vector<int> temp (2);
					temp[0] = i;
					temp[1] = j;
					points.push_back(temp);
				}
				else if(matrix[i][j] < minval)
				{
					points.clear();
					std::vector<int> temp (2);
					temp[0] = i;
					temp[1] = j;
					points.push_back(temp);
					minval = matrix[i][j];
				}
			}
		}
	}
	/*
	 Function: find

		Gets the location of the point.

	 Parameters:

		matrix - The matrix.
		&points - A vector with the index pair(s) of where the points are.

	*/
	template <class T>
	bool find(std::vector<std::vector<T>> matrix, std::vector<std::vector<int>> & points, T val)
	{
		if(matrix.size() < 1)
		{
			return false;
		}

		int rowlength = matrix[0].size();
		for(int i = 0; i < matrix.size(); ++i)
		{
			if(matrix[i].size() != rowlength)
			{
				return false;
			}
		}

		points.clear();

		for(int i = 0; i < matrix.size(); ++i)
		{
			for(int j = 0; j < matrix[i].size(); ++j)
			{
				if(matrix[i][j] == val)
				{	
					std::vector<int> temp (2);
					temp[0] = i;
					temp[1] = j;
					points.push_back(temp);
				}
			}
		}
	}

	template <class T>
	bool find(T ** matrix, int row, int col, std::vector<std::vector<int>> & points, T val)
	{
		if(row < 1 || col < 1)
		{
			return false;
		}

		points.clear();

		for(int i = 0; i < row; ++i)
		{
			for(int j = 0; j < col; ++j)
			{
				if(matrix[i][j] == val)
				{	
					std::vector<int> temp (2);
					temp[0] = i;
					temp[1] = j;
					points.push_back(temp);
				}
			}
		}
	}

	/*
	 Function: add

		Adds two matrices together.

	 Parameters:

		matrix,matrix2 - The matrices to be added.
		&result - The result of the addition.

	Returns:
		True if the addition was successful.

	*/
	template <class T>
	bool add(std::vector<std::vector<T>> matrix, std::vector<std::vector<T>> matrix2, std::vector<std::vector<T>> & result)
	{
		if(matrix.size() < 1 || matrix2.size() < 1 || (matrix.size() != matrix2.size()))
		{
			return false;
		}

		if(matrix[0].size() != matrix2[0].size())
		{
			return false;
		}

		int rowlength = matrix[0].size();
		for(int i = 0; i < matrix.size(); ++i)
		{
			if(matrix[i].size() != rowlength || matrix2[i].size() != rowlength)
			{
				return false;
			}
		}

		result.resize(matrix.size());
		for(int i = 0; i < result.size(); ++i)
		{
			result[i].resize(rowlength);
		}

		for(int i = 0; i < matrix.size(); ++i)
		{
			for(int j = 0; j < matrix[i].size(); ++j)
			{
				result[i][j] = matrix[i][j] + matrix2[i][j];		
			}
		}
	}
	
	template <class T>
	bool add(T ** matrix, int row, int col, T ** matrix2, T ** result)
	{
		if(row < 1 || col < 1)
		{
			return false;
		}

		for(int i = 0; i < row; ++i)
		{
			for(int j = 0; j < col; ++j)
			{
				result[i][j] = matrix[i][j] + matrix2[i][j];		
			}
		}
	}

	template <class T>
	T minimum(const T& A, const T& B){
		if (A < B){
			return A;
		}
		return B;
	}

	template <class T>
	T maximum(const T& A, const T& B){
		if (A > B){
			return A;
		}
		return B;
	}
}