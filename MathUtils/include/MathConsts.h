#pragma once
#include "CoreDefines.h"

#if defined _WIN32 
	#ifdef MATHUTILS_PROJECT
		#define EXPORTIMPORT __declspec(dllexport)
	#else
		#define EXPORTIMPORT __declspec(dllimport)
	#endif
#else
	#define EXPORTIMPORT
#endif


namespace mathutils{
	EXPORTIMPORT extern const double MATH_PI;

	EXPORTIMPORT extern const double MATH_E;
	EXPORTIMPORT extern const __int64 KILOBYTE;
	EXPORTIMPORT extern const __int64 MEGABYTE;
	EXPORTIMPORT extern const __int64 GIGABYTE;
	EXPORTIMPORT extern const __int64 TERABYTE;
	EXPORTIMPORT extern const __int64 PETABYTE;
	EXPORTIMPORT extern const __int64 EXABYTE;

	EXPORTIMPORT extern const unsigned int uint_infinity;
}
