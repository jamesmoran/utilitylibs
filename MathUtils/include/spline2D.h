#pragma once

#include <opencv/cv.h> 

namespace mathutils{

	class spline2D3pt{
	public:
		MMI_EXPORT spline2D3pt();
		MMI_EXPORT spline2D3pt(const spline2D3pt& s);		//copy constructor
		MMI_EXPORT spline2D3pt(CvPoint2D32f& pt0,CvPoint2D32f& pt1,CvPoint2D32f& pt2);	//construct spline from 3 known points
		MMI_EXPORT spline2D3pt(CvPoint2D32f* points, int nPoints, double* weights = NULL);	//compute spline to match data set.
		MMI_EXPORT ~spline2D3pt();

		MMI_EXPORT spline2D3pt operator=(const spline2D3pt& s);

		MMI_EXPORT CvScalar getParams() const;

		MMI_EXPORT double y(double x) const;
		MMI_EXPORT double f(double x) const;

		MMI_EXPORT double slope(double x) const;
		MMI_EXPORT CvPoint2D32f normal(double x) const;

		MMI_EXPORT CvPoint2D32f getXBounds() const;

		MMI_EXPORT spline2D3pt makeDisplacedCopy(CvPoint2D32f displacement) const;
		MMI_EXPORT spline2D3pt makeScaledCopy(CvPoint2D32f scaleFactors) const;

		MMI_EXPORT double minX() const;
		MMI_EXPORT double maxX() const;
		MMI_EXPORT double getWidth() const;

		MMI_EXPORT void setPoints(CvPoint2D32f& pt0,CvPoint2D32f& pt1,CvPoint2D32f& pt2);
		MMI_EXPORT void fitToPoints(CvPoint2D32f* points, int nPoints, double* weights = NULL);

		MMI_EXPORT void drawOnImage(IplImage* img, int segments, CvScalar color, bool showNormals = false) const;

	private:
		static CvScalar compute_abc(CvPoint2D32f pt0,CvPoint2D32f pt1,CvPoint2D32f pt2);

		double error(CvPoint2D32f* points, int nPoints, double* weights = NULL) const;

		CvPoint2D32f knots[3];
		double abc[3];	//parameters
		double xmin;
		double xmax;
	};

	double houghTransformSpline(CvPoint2D32f* points, int nPoints, spline2D3pt& spline, CvPoint& minDisplacement, CvPoint& maxDisplacement, double* weights = NULL, int xRange = 1, int yRange = 1, double minVotesThresh = 0);
}
