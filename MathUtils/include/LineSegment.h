#pragma once

#include <opencv/cv.h>
#include "CoreDefines.h"

namespace mathutils{

	class LineSegment{
	public:

		MMI_EXPORT LineSegment();
		MMI_EXPORT LineSegment(double p_x1, double p_y1, double p_x2, double p_y2);
		MMI_EXPORT LineSegment(const CvPoint2D32f& p_point1, const CvPoint2D32f& p_point2);
		MMI_EXPORT LineSegment(const LineSegment& p_lineSegment);

		MMI_EXPORT double Length() const;
		MMI_EXPORT CvPoint2D32f Normal() const;
		MMI_EXPORT CvPoint2D32f Ray() const;
		MMI_EXPORT CvPoint2D32f RayNormalized() const;

		MMI_EXPORT LineSegment& operator=(const LineSegment& p_lineSegment);

		MMI_EXPORT const CvPoint2D32f& P1() const;
		MMI_EXPORT const CvPoint2D32f& P2() const;

		MMI_EXPORT CvPoint2D32f& P1();
		MMI_EXPORT CvPoint2D32f& P2();

		MMI_EXPORT double Theta() const;

	protected:
	
		void Copy(const LineSegment& p_lineSegment);
	
		CvPoint2D32f m_point1;
		CvPoint2D32f m_point2;

	};

}
