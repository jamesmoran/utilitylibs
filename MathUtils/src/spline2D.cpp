#include "MathUtils.h"

#include <algorithm>
#include <MathCvFuncs.h>

namespace mathutils{

spline2D3pt::spline2D3pt(){
	abc[0] = 0;
	abc[1] = 0;
	abc[2] = 0;

	knots[0] = cvPoint2D32f(0,0);
	knots[1] = cvPoint2D32f(0,0);
	knots[2] = cvPoint2D32f(0,0);
	
	xmin = 0;
	xmax = 0;
}

spline2D3pt::spline2D3pt(const spline2D3pt& s){
	abc[0] = s.abc[0];
	abc[1] = s.abc[1];
	abc[2] = s.abc[2];

	knots[0] = s.knots[0];
	knots[1] = s.knots[1];
	knots[2] = s.knots[2];

	xmin = s.xmin;
	xmax = s.xmax;
}

spline2D3pt::spline2D3pt(CvPoint2D32f& pt0,CvPoint2D32f& pt1,CvPoint2D32f& pt2){
	setPoints(pt0,pt1,pt2);
}

spline2D3pt::spline2D3pt(CvPoint2D32f* points, int nPoints, double* weights){
	fitToPoints(points, nPoints, weights);
}

spline2D3pt::~spline2D3pt(){


}

spline2D3pt spline2D3pt::operator=(const spline2D3pt& s){
	abc[0] = s.abc[0];
	abc[1] = s.abc[1];
	abc[2] = s.abc[2];
	
	knots[0] = s.knots[0];
	knots[1] = s.knots[1];
	knots[2] = s.knots[2];
	
	xmin = s.xmin;
	xmax = s.xmax;

	return *this;
}


CvScalar spline2D3pt::getParams() const{
	return cvScalar(abc[0],abc[1],abc[2]);
}

double spline2D3pt::y(double x) const{
	//y = ax^2 + bx + c
	return abc[0]*x*x + abc[1]*x + abc[2];
}

double spline2D3pt::f(double x) const{
	return y(x);
}

double spline2D3pt::slope(double x) const{
	//derivative y' = 2ax + b
	return 2*abc[0]*x + abc[1];
}

CvPoint2D32f spline2D3pt::normal(double x) const{
	double theta = atan(slope(x));

	return cvPoint2D32f(sin(theta),-cos(theta));
}

CvPoint2D32f spline2D3pt::getXBounds() const{
	return cvPoint2D32f(xmin,xmax);
}

spline2D3pt spline2D3pt::makeDisplacedCopy(CvPoint2D32f displacement) const{
	return spline2D3pt(cvPoint2D32f( knots[0].x + displacement.x , knots[0].y + displacement.y), cvPoint2D32f( knots[1].x + displacement.x , knots[1].y + displacement.y), cvPoint2D32f( knots[2].x + displacement.x , knots[2].y + displacement.y) );
}

spline2D3pt spline2D3pt::makeScaledCopy(CvPoint2D32f scaleFactors) const{
	return spline2D3pt(cvPoint2D32f( knots[0].x * scaleFactors.x , knots[0].y * scaleFactors.y), cvPoint2D32f( knots[1].x * scaleFactors.x , knots[1].y * scaleFactors.y), cvPoint2D32f( knots[2].x * scaleFactors.x , knots[2].y * scaleFactors.y) );
}

double spline2D3pt::minX() const{
	return xmin;
}

double spline2D3pt::maxX() const{
	return xmax;
}

double spline2D3pt::getWidth() const{
	//printf("Computing width: %f-%f = %f\n",xmax,xmin,xmax-xmin);
	return xmax-xmin;
}

void spline2D3pt::setPoints(CvPoint2D32f& pt0,CvPoint2D32f& pt1,CvPoint2D32f& pt2){
	knots[0] = pt0;
	knots[1] = pt1;
	knots[2] = pt2;

	//printf("Points are: [%#1.2f,%#1.2f],[%#1.2f,%#1.2f],[%#1.2f,%#1.2f]\n",pt0.x,pt0.y,pt1.x,pt1.y,pt2.x,pt2.y);

	xmin = std::min(pt0.x,std::min(pt1.x,pt2.x));
	xmax = std::max(pt0.x,std::max(pt1.x,pt2.x));

	CvScalar params = compute_abc(pt0,pt1,pt2);
	abc[0] = params.val[0];
	abc[1] = params.val[1];
	abc[2] = params.val[2];

	//printf("params are: [%#1.2f,%#1.2f,%#1.2f]\n",abc[0],abc[1],abc[2]);
}

void spline2D3pt::fitToPoints(CvPoint2D32f* points, int nPoints, double* weights){
	//fit the quadratic to the data using least-squares regression.
	//see http://www.personal.psu.edu/jhm/f90/lectures/lsq2.html for forumlation
	
	//for now, weights ignored

	//initialize x range.
	xmin = points[0].x;
	xmax = points[0].x;

	//X* = argmin_x || A*X-B ||
	CvMat* A = cvCreateMat(3,3,CV_32FC1);		//source matrix
	CvMat* B = cvCreateMat(3,1,CV_32FC1);		//right-hand part of linear system
	CvMat* X_star = cvCreateMat(3,1,CV_32FC1);	//output variable: will be filled with optimal parameters

	//compute the data power series
	double n = (double) nPoints;
	double sum_x = 0;
	double sum_x2 = 0;
	double sum_x3 = 0;
	double sum_x4 = 0;
	double sum_y = 0;
	double sum_yx = 0;
	double sum_yx2 = 0;

	for(int i = 0; i < nPoints; i++){
		double x = points[i].x;
		double y = points[i].y;
		sum_x += x;
		sum_x2 += x*x;
		sum_x3 += x*x*x;
		sum_x4 += x*x*x*x;
		sum_y += y;
		sum_yx += y*x;
		sum_yx2 += y*x*x;

		if(x < xmin){xmin = x;}
		if(x > xmax){xmax = x;}
	}

	//construct the source matrix A
	cvSetReal2D(A,0,0,n);
	cvSetReal2D(A,0,1,sum_x);
	cvSetReal2D(A,0,2,sum_x2);
	cvSetReal2D(A,1,0,sum_x);
	cvSetReal2D(A,1,1,sum_x2);
	cvSetReal2D(A,1,2,sum_x3);
	cvSetReal2D(A,2,0,sum_x2);
	cvSetReal2D(A,2,1,sum_x3);
	cvSetReal2D(A,2,2,sum_x4);

	//construct the right-hand vector B
	cvSetReal2D(B,0,0,sum_y);
	cvSetReal2D(B,1,0,sum_yx);
	cvSetReal2D(B,2,0,sum_yx2);

	cvSolve(A,B,X_star,CV_LU);	//solve the linear system using the LU-decomposition

	//NOTE! reverse order!  -> this formulation uses X = [c,b,a]'
	abc[0] = cvGetReal2D(X_star,2,0);
	abc[1] = cvGetReal2D(X_star,1,0);
	abc[2] = cvGetReal2D(X_star,0,0);

	//printf("Solved system: new parameters are: [%#1.2f,%#1.2f,%#1.2f]\n",abc[0],abc[1],abc[2]);
	cvReleaseMat(&A);
	cvReleaseMat(&B);
	cvReleaseMat(&X_star);

	//create new control points
	knots[0].x = (float) xmin;
	knots[0].y = (float) y(knots[0].x);
	knots[1].x = (float)(xmax + xmin)/2.0f;
	knots[1].y = (float) y(knots[1].x);
	knots[2].x = (float) xmax;
	knots[2].y = (float) y(knots[2].x);
	//printf("Knot 0: [%#1.2f,%#1.2f]\n",knots[0].x,knots[0].y);
	//printf("Knot 1: [%#1.2f,%#1.2f]\n",knots[1].x,knots[1].y);
	//printf("Knot 2: [%#1.2f,%#1.2f]\n",knots[2].x,knots[2].y);
}

void spline2D3pt::drawOnImage(IplImage* img, int segments, CvScalar color, bool showNormals) const{
	//for each segment
	double range = xmax-xmin;
	double step = range/segments;

	for(int i = 0; i < segments; i++){
		double xLeft = xmin + (double)i*step;
		double yLeft = y(xLeft);
		CvPoint left = cvPoint((int) xLeft, (int) yLeft);

		double xRight = xmin + (double)(i+1)*step;
		double yRight = y(xRight);
		CvPoint right = cvPoint((int) xRight, (int) yRight);

		cvLine(img,left,right,color,2);
		//printf("Point: [%d,%d]\n",left.x,left.y);


		if(showNormals){
			//draw the normal
			CvPoint2D32f normLeft = normal(xLeft);
			CvPoint normTip = cvPoint(left.x + mathutils::round(normLeft.x * step),left.y + mathutils::round(normLeft.y * step));
			cvLine(img,left,normTip,color,1);
		}
	}

	cvCircle(img,cvPoint((int) knots[0].x, (int) knots[0].y),3,color,CV_FILLED);
	cvCircle(img,cvPoint((int) knots[1].x, (int) knots[1].y),3,color,CV_FILLED);
	cvCircle(img,cvPoint((int) knots[2].x, (int) knots[2].y),3,color,CV_FILLED);
	//printf("Knot 0: [%#1.2f,%#1.2f]\n",knots[0].x,knots[0].y);
	//printf("Knot 1: [%#1.2f,%#1.2f]\n",knots[1].x,knots[1].y);
	//printf("Knot 2: [%#1.2f,%#1.2f]\n",knots[2].x,knots[2].y);
	//printf("drew points\n");

}

CvScalar spline2D3pt::compute_abc(CvPoint2D32f pt0,CvPoint2D32f pt1,CvPoint2D32f pt2){
	if(pt0.x == pt1.x || pt1.x == pt2.x || pt2.x == pt0.x){
		//if any points overlap, we'll have a singularity, so return a flat line.
		return cvScalarAll(0);
	}
	
	double x0 = pt0.x;
	double y0 = pt0.y;
	double x1 = pt1.x;
	double y1 = pt1.y;
	double x2 = pt2.x;
	double y2 = pt2.y;

	double a = (x0*(y1-y2) + y0*(x2-x1) + x1*y2 - x2*y1) / ( (x1-x0)*(x2-x0)*(x2-x1) );
	double b = (y2-y0)/(x2-x0) - (x2+x0)*a;
	double c = y0 - a*x0*x0 - b*x0;

	return cvScalar(a,b,c);
}


double spline2D3pt::error(CvPoint2D32f* points, int nPoints, double* weights) const{
	
	double totalError = 0;
	for(int i = 0; i < nPoints; i++){
		double px = points[i].x;
		double err = points[i].y - y(px);
		if(weights != NULL){
			err *= weights[i];
		}
		totalError += err;
	}
	return totalError;
}

//********************************************************************************************

double houghTransformSpline(CvPoint2D32f* points, int nPoints, spline2D3pt& spline, CvPoint& minDisplacement, CvPoint& maxDisplacement, double* weights, int xRange, int yRange, double minVotesThresh){

	//check that the range values are strictly positive and odd
	int xR = xRange;
	int yR = yRange;

	if(xR < 1){xR = 1;}
	if(yR < 1){yR = 1;}
	if(xR % 2 == 0){xR++;}
	if(yR % 2 == 0){yR++;}

	//create the vote histogram
	IplImage* votes = cvCreateImage(cvSize(maxDisplacement.x-minDisplacement.x,maxDisplacement.y-minDisplacement.y),IPL_DEPTH_32F,1);
		//the votes image is a histogram.  Each cell represents the number of votes for that x,y displacement of the spline.
		//points will vote for all displacements that place the spline on the point.
	cvZero(votes);

	//printf("Hough transform on spline: creating %dx%d histogram over displacements range sx[%d,%d] sy[%d,%d]\n",
	//		maxDisplacement.x-minDisplacement.x,
	//		maxDisplacement.y-minDisplacement.y,
	//		minDisplacement.x,maxDisplacement.x,
	//		minDisplacement.y,maxDisplacement.y);

	//for each point
	for(int p = 0; p < nPoints; p++){
		//get the x-position of the point.
		int px = (int) points[p].x;
		int py = (int) points[p].y;

		//if the spline cannot be displaced in x enough to include that point, ignore it
		if(px < spline.minX() + minDisplacement.x || px > spline.maxX() + maxDisplacement.x ){
			//if the spline can't be shifted enough to cover this point, then we don't want to include it.
			continue;
		}

		//for each value of x-displacement
		for(int sx = minDisplacement.x; sx < maxDisplacement.x; sx++){

			//compute the amount the spline must be displaced in y-axis so it still passes over the point px,py
			int sy = (int) (spline.y(px + (double)sx) - py);
			
			//determine the row and column indices in the voting histogram
			int sx_colIndex = sx-minDisplacement.x;
			int sy_rowIndex = sy-minDisplacement.y;

			//vote in a region around the actual values
			for(int c = sx_colIndex-xR/2; c <= sx_colIndex+xR/2; c++){
				for(int r = sy_rowIndex-yR/2; r <=sy_rowIndex+yR/2; r++){
					//check that this is a valid index
					if(r >= 0 && r < (maxDisplacement.y - minDisplacement.y) && c >=0 && c < (maxDisplacement.x - minDisplacement.x)){
						//compute the number of votes (decays by Manhattan distance from center)
						int nVotes = (xR/2+1) - abs(sx_colIndex-c) - abs(sy_rowIndex-r);
						if(nVotes > 0){
							cvSetReal2D(votes,r,c,cvGetReal2D(votes,r,c)+nVotes);
						}
					}
				}
			}

		}
		
	}

	//find the maximum cell in the displacement histogram
	double minVotes;
	double maxVotes;

	CvPoint minLoc;
	CvPoint maxLoc;
	
	//find minimum and maximum in votes histogram
	cvMinMaxLoc(votes,&minVotes,&maxVotes,&minLoc,&maxLoc);

	//if the votes are sufficiently high
	if(maxVotes >= minVotesThresh){
		//compute the corresponding displacement
		CvPoint2D32f displacement = cvPoint2D32f(-(maxLoc.x + minDisplacement.x),-(maxLoc.y + minDisplacement.y));

		//displace the spline by the computed values
		spline = spline.makeDisplacedCopy(displacement);
	}

	cvReleaseImage(&votes);

	//return the number of votes
	return maxVotes;
}

}