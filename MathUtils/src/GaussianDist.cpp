#include "GaussianDist.h"
#include <math.h>
#include <assert.h>
#include <boost/math/constants/constants.hpp>

namespace mathutils{

	GaussianDist::GaussianDist( double mu, double sigma)
		: m_mu(mu)
		, m_sigma(sigma)
	{
		assert(sigma > 0);
	}

	double GaussianDist::Mu() const
	{
		return m_mu;
	}

	double GaussianDist::Sigma() const
	{
		return m_sigma;
	}

	double GaussianDist::Var() const
	{
		return m_sigma*m_sigma;
	}

	double GaussianDist::P( double x ) const
	{
		return PGaussian(x,m_mu,m_sigma);
	}

	double GaussianDist::PGaussian( double x, double mu, double sigma )
	{
		assert(sigma > 0);

		using namespace boost::math::constants;

		double diff = x-mu;
		double exponent = -( diff*diff / (2*sigma*sigma) );
		double coeff = 1.0 / (sigma * sqrt( 2 * pi<double>()));
		return  coeff * pow( e<double>() , exponent );
	}

}
