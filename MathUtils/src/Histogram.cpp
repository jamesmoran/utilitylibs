#include "Histogram.h"

#include <sstream>
#include <cfloat>
#include <math.h>

namespace mathutils{

Histogram::Histogram(const Histogram& h){
	binBounds = h.binBounds;
	bins = h.bins;
	totalEntries = h.totalEntries;
	minVal = h.minVal;
	maxVal = h.maxVal;
	modeCount = h.modeCount;
	modeBin = h.modeBin;
}
Histogram::Histogram(double minVal, double maxVal, int nBins){
	binBounds = uniformBins(minVal,maxVal,nBins);
	bins.resize(binBounds.size()+1,0);
	totalEntries = 0;
	minVal = DBL_MAX;
	maxVal = -DBL_MAX;
	modeCount = 0;
	modeBin = 0;
}

Histogram::Histogram(std::vector<double>& binBounds){
	this->binBounds = binBounds;
	bins.resize(binBounds.size()+1,0);
	totalEntries = 0;
	minVal = DBL_MAX;
	maxVal = -DBL_MAX;
	modeCount = 0;
	modeBin = 0;
}

int Histogram::add(double val){
	//printf("add(%f)\n",val);
	int targetBin = nearestBin(val);
	//printf("targetBin = %d\n",targetBin);
	if(targetBin == 0){
		//printf("\tvalue %f is UNDERSIZE\n",val);
	}else if(targetBin == bins.size()-1){
		//printf("\tvalue %f is OVERSIZE\n",val);
	}else if(targetBin > 0 && targetBin < (int) bins.size()-1){
		//printf("\tvalue %f is in range %f,%f\n",val,binBounds[targetBin-1],binBounds[targetBin]);
	}else{
		//printf("\tError: targetBin is %d\n",targetBin);
	}
	bins[targetBin]++;
	totalEntries++;

	if(val < minVal){ minVal = val;}
	if(val > maxVal){ maxVal = val;}
	if(bins[targetBin] > modeCount){
		modeCount = bins[targetBin];
		modeBin = targetBin;
	}

	return targetBin;
}

int Histogram::nBins() const{
	return (int)bins.size() - 2;
}

int Histogram::nearestBin(double val) const{

	if(val < binBounds[0]){
		return 0;
	}else if(val >= binBounds[binBounds.size()-1]){
		return (int)binBounds.size();
	}else{
		return binaryFind(val,binBounds,0,(int)binBounds.size()-1);
	}
}

double Histogram::lowerBound(int bin) const{
	if(bin <= 0){
		if(minVal < binBounds[0]){
			return minVal;
		}else{
			return binBounds[0];
		}
	}else if(bin > (int) bins.size()-2){
		return binBounds[binBounds.size()-1];
	}else{
		return binBounds[bin-1];
	}
}

double Histogram::upperBound(int bin) const{
	if(bin < 0){
		return binBounds[0];
	}else if(bin > (int) bins.size()-2){
		if(maxVal > binBounds[binBounds.size()-1]){
			return maxVal;
		}else{
			return binBounds[binBounds.size()-1];
		}
	}else{
		return binBounds[bin];
	}
}
double Histogram::nearestLowerBound(double val) const{
	return lowerBound(nearestBin(val));
}
double Histogram::nearestUpperBound(double val) const{
	return upperBound(nearestBin(val));
}
double Histogram::binCenter(int bin) const{
	return (lowerBound(bin) + upperBound(bin))/2.0;
}
double Histogram::nearestBinCenter(double val) const{
	int bin = nearestBin(val);
	return binCenter(bin);
}
double Histogram::minValue() const{
	return minVal;
}
double Histogram::maxValue() const{
	return maxVal;
}

__int64 Histogram::nUnder() const{
	return bins[0];
}
__int64 Histogram::nOver() const{
	return bins[bins.size()-1];
}
__int64 Histogram::nInRange() const{
	return nTotal() - nUnder() - nOver();
}
__int64 Histogram::nTotal() const{
	return totalEntries;
}

double Histogram::mean() const{

	double sum = 0;
	double weightSum = 0;

	for(int bin = 0; bin < (int) bins.size(); bin++){
		//printf("binCenter(%d)=%f\n",bin,binCenter(bin));
		sum += (double) count(bin) * binCenter(bin);
		weightSum += (double) count(bin);
	}

	//printf("sum=%f\n",sum);
	//printf("weightSum=%f\n",weightSum);

	if(weightSum > 0){
		return sum/weightSum;
	}
	return 0;
}

double Histogram::stddev() const{
	return stddev(mean());
}

double Histogram::stddev(double meanVal) const{
	
	double ssd = 0;
	double weightSum = 0;
	for(int bin = 0; bin < (int) bins.size(); bin++){
		double diff = meanVal - binCenter(bin);
		ssd += (double) count(bin) * diff*diff;
		weightSum += (double) count(bin);
	}

	if(weightSum > 0){
		return sqrt( ssd / weightSum );
	}
	
	return 0;
}

double Histogram::median() const{

	__int64 halfTotal = totalEntries / 2;
	std::vector<__int64>::const_iterator iter;
	__int64 sum = 0;
	int index = 0;
	for(iter = bins.begin(); iter != bins.end(); iter++){
		sum += *iter;
		if(sum > halfTotal){break;}
		index++;
	}

	if(index == 0){
		//special case: median is undersized
		double ratio = (double) halfTotal / (double) sum;
		return linear_interpolate(ratio,0,1.0,minVal,upperBound(0));
	}else if(index == bins.size()-1){
		//special case: median is oversized
		__int64 notOverSize = totalEntries - nOver();
		double ratio = (double) (halfTotal - notOverSize) / (double) nOver();
		return linear_interpolate(ratio,0,1.0,lowerBound(static_cast<int>(bins.size()-1)),maxVal);
	}else{
		double ratio = (double) (halfTotal - sum) / (double) count(index);
		return linear_interpolate(ratio,0,1.0,lowerBound(index),upperBound(index));
	}
}

__int64 Histogram::count(int bin) const{
	if(bin < 0){ return bins[0];}
	if(bin > (int) bins.size()-1){return bins[(int) bins.size()-1];}
	return bins[bin];
}
__int64 Histogram::nearestBinCount(double val) const{
	return count(nearestBin(val));
}

std::string Histogram::toText() const{
	std::stringstream text;

	text << "Histogram: nBins = " << nBins() << std::endl;
	text << " min = " << minValue() << ", max = " << maxValue() << std::endl;
	text << " Entries "<<(int) nUnder()<<"/"<<(int) nTotal()<<" undersize, "<<(int) nInRange()<<"/"<<(int) nTotal()<<" in range, "<<(int) nOver()<<"/"<<(int) nTotal()<<" oversize" << std::endl;
	text << " mean = "<<mean()<<", stddev = "<<stddev()<<", median = " <<median()<< std::endl;
	text << " (Under " << upperBound(0) << ") : "<< (int) bins[0] << std::endl;
	for(int b = 1; b <= (int) bins.size()-2; b++){
		text << " ("<<lowerBound(b) << " -> "<< upperBound(b) <<") : "<< (int) bins[b] << std::endl;
	}
	text << " (Over "<<lowerBound(static_cast<int>(bins.size())-1)<<") : "<< (int) bins[bins.size()-1] << std::endl;

	return text.str();
}

std::vector<double> Histogram::uniformBins(double minVal, double maxVal, int nBins){
	std::vector<double> binBounds;

	if(maxVal <= minVal){
		return binBounds;
	}

	double range = maxVal-minVal;
	double step = range/nBins;

	for(int bin = 0; bin <= nBins; bin++){
		binBounds.push_back(minVal + (double) bin*step);
	}

	return binBounds;
}

int Histogram::binaryFind(double targetVal, const std::vector<double> binBounds, int start, int end) const{

	//printf("binaryFind(%f,%d/%d,%d/%d)\n",targetVal,start,(int) binBounds.size(),end,(int) binBounds.size());

	if(start == end){
		//printf("start==end\n");
		return start+1l;
	}

	if(start + 1 == end){
		//printf("start+1==end\n");
		return start+1;
	}

	int pivotBound = start + (end-start)/2;
	double pivot = binBounds[pivotBound];
	//printf("pivotBound = %d\n",pivotBound);
	//printf("pivot = %f\n",pivot);

	if(targetVal < pivot){
		//printf("\tLower\n");
		return binaryFind(targetVal,binBounds,start,pivotBound);
	}else{
		//printf("\tUpper\n");
		return binaryFind(targetVal,binBounds,pivotBound,end);
	}
}

double Histogram::linear_interpolate(double x, double x0, double x1, double y0, double y1){
	return y0 + (y1-y0)*(x - x0)/(x1-x0);
}

}
