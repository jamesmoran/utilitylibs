#include "LineSegment.h"

#include <math.h>
#include "MathCvFuncs.h"

namespace mathutils{

	LineSegment::LineSegment(){
		m_point1 = cvPoint2D32f(0,0);
		m_point2 = cvPoint2D32f(0,0);
	}

	LineSegment::LineSegment(double p_x1, double p_y1, double p_x2, double p_y2){
		m_point1 = cvPoint2D32f(p_x1,p_y1);
		m_point2 = cvPoint2D32f(p_x2,p_y2);
	}

	LineSegment::LineSegment(const CvPoint2D32f& p_point1, const CvPoint2D32f& p_point2){
		m_point1 = p_point1;
		m_point2 = p_point2;
	}

	LineSegment::LineSegment(const LineSegment& p_lineSegment){
		Copy(p_lineSegment);
	}

	double LineSegment::Length() const{
		double dx = m_point2.x - m_point1.x;
		double dy = m_point2.y - m_point1.y;
		return sqrt(dx*dx + dy*dy);
	}

	CvPoint2D32f LineSegment::Normal() const{
		CvPoint2D32f ray_normalized = RayNormalized();
		return cvPoint2D32f(-ray_normalized.y,ray_normalized.x);
	}

	CvPoint2D32f LineSegment::Ray() const{
		double dx = m_point2.x - m_point1.x;
		double dy = m_point2.y - m_point1.y;
		return cvPoint2D32f(dx,dy);
	}

	CvPoint2D32f LineSegment::RayNormalized() const{
		return Normalize(Ray());
	}

	LineSegment& LineSegment::operator=(const LineSegment& p_lineSegment){
		Copy(p_lineSegment);
		return *this;
	}

	const CvPoint2D32f& LineSegment::P1() const{
		return m_point1;
	}

	const CvPoint2D32f& LineSegment::P2() const{
		return m_point2;
	}

	CvPoint2D32f& LineSegment::P1(){
		return m_point1;
	}

	CvPoint2D32f& LineSegment::P2(){
		return m_point2;
	}

	double LineSegment::Theta() const{
		CvPoint2D32f ray = Ray();
		return atan2(ray.y,ray.x);
	}

	void LineSegment::Copy(const LineSegment& p_lineSegment){
		m_point1 = p_lineSegment.m_point1;
		m_point2 = p_lineSegment.m_point2;
	}

}
