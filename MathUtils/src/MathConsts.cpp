#define MATHUTILS_PROJECT
#include "MathConsts.h"

namespace mathutils{

	const double MATH_PI = 3.14159265359;
	const double MATH_E = 2.71828182846;

	const __int64 KILOBYTE = 1024;
	const __int64 MEGABYTE = KILOBYTE*1024;
	const __int64 GIGABYTE = MEGABYTE*1024;
	const __int64 TERABYTE = GIGABYTE*1024;
	const __int64 PETABYTE = TERABYTE*1024;
	const __int64 EXABYTE = PETABYTE*1024;

	const unsigned int uint_infinity = ~0;
}
