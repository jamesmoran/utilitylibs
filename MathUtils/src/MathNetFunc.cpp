#include "MathNetFunc.h"
#include <math.h>
#include <assert.h>

namespace mathutils{


	float MathNet::Tanh( float a )
	{
		return std::tanh(a) ;
	}

	float MathNet::HardSigmoid( float x )
	{
		x = (x*0.2f) + 0.5f ;
		if (x > 1.f)
			x = 1.f;
		if (x < 0)
			x = 0.f;
		return x;
	}

	float MathNet::Sigmoid( float x )
	{
		errno = 0 ;
		float ans = 1 / (1 + std::exp((-1)*x)) ;
		if (errno)
		{

		}
		return ans ;

	}

	float MathNet::Sigmoid2( float x )
	{
		float expX = std::exp(x) ;
		return expX / (1.f + expX) ;
	}

	float MathNet::FastSigmoid( float x )
	{
		return 1 / (1 + std::abs(x) )  ;
	}

	// ramp function (Might be used for ReLU activations).
	float MathNet::Ramp(float x)
	{
		if (x > 0)
			return x;
		else 
			return 0;
	}
	bool ordering::operator ()(std::pair<size_t, iterInt> const& a, std::pair<size_t, iterInt> const& b) 
	{
		return *(a.second) < *(b.second);
	}

	bool pairOrdering::operator ()(std::pair<float, float> const& a, std::pair<float, float> const& b) 
	{
		return a.first < b.first;
	}

	bool compareRect::operator ()(CvRect const& a, CvRect const& b) 
	{
		return a.y < b.y ;
	}

	template <typename T>
	std::vector<T> MathNet::sort_from_ref( std::vector<T> const& in, std::vector<std::pair<size_t, iterInt> > const& reference) 
	{
		std::vector<T> ret(in.size());
		size_t const size = in.size();
		for (size_t i = 0; i < size; ++i)
			ret[i] = in[reference[i].first];
		return ret;
	}

}
