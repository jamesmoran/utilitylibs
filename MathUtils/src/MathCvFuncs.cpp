#include "MathCvFuncs.h"

namespace mathutils{

	CvPoint2D32f Normalize(const CvPoint2D32f& p_vector){
		double length = Norm2(p_vector);
		if(length > 0){
			return cvPoint2D32f(p_vector.x / length, p_vector.y / length);
		}
		return p_vector;
	}

	double Norm2(const CvPoint2D32f& p_vector){
		return sqrt(p_vector.x * p_vector.x + p_vector.y * p_vector.y);
	}

	double Dot(const CvPoint2D32f& p_vector1, const CvPoint2D32f& p_vector2){
		return p_vector1.x*p_vector2.x + p_vector1.y*p_vector2.y;
	}


}