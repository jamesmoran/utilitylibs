#include "MathUtilFuncs.h"
#include "MathConsts.h"
#include "UtilityLibsError.h"
#include <algorithm>

namespace mathutils{


	time_t MakeTimestamp(int year, int month, int day, int hour, int minute, int second, bool is_dst){
		struct tm timestruct;
		timestruct.tm_year = year -1900;
		timestruct.tm_mon = month - 1;
		timestruct.tm_mday = day;
		timestruct.tm_hour = hour;
		timestruct.tm_min = minute;
		timestruct.tm_sec = second;
		timestruct.tm_isdst = is_dst ? 1 : 0;

		return mktime(&timestruct);
	}

	double Terabytes(__int64 bytes){
		return (double) bytes / (double) TERABYTE;
	}

	double Gigabytes(__int64 bytes){
		return (double) bytes / (double) GIGABYTE;
	}

	double Megabytes(__int64 bytes){
		return (double) bytes / (double) MEGABYTE;
	}

	double Kilobytes(__int64 bytes){
		return (double) bytes / (double) KILOBYTE;
	}


	int round(double val){
		return (int) floor(val+0.5);
	}

	double linear_interpolate(double x, double x0, double x1, double y0, double y1){
		return y0 + (x-x0) * (y1-y0) / (x1-x0);
	}

	double linear_saturation(double x, double x0, double x1, double y0, double y1){
		if(x0 < x1){
			if(x < x0){
				return y0;
			}else if(x > x1){
				return y1;
			}else{
				return linear_interpolate(x,x0,x1,y0,y1);
			}
		}else{
			if(x > x0){
				return y0;
			}else if(x < x1){
				return y1;
			}else{
				return linear_interpolate(x,x0,x1,y0,y1);
			}
		}
	}


	double sine_saturation(double x, double x0, double x1, double y0, double y1){
		if(x0 < x1){
			if(x < x0){
				return y0;
			}else if(x > x1){
				return y1;
			}else{
				return y0 + (y1-y0) * sin(linear_interpolate(x,x0,x1,-MATH_PI/2.0,MATH_PI/2.0));
			}
		}else{
			if(x > x0){
				return y0;
			}else if(x < x1){
				return y1;
			}else{
				return y0 + (y1-y0) * sin(linear_interpolate(x,x0,x1,-MATH_PI/2.0,MATH_PI/2.0));
			}
		}
	}

	double array_mean(int* values, int count){
		if(count <= 0){return 0;}

		double sum = 0;
		for(int i = 0; i < count; i++){
			sum += (double) values[i];
		}
		return sum / (double) count;
	}

	double array_stddev(int* values, int count){
		if(count <= 0){return 0;}

		return array_stddev(values, count, array_mean(values,count));
	}

	double array_stddev(int* values, int count, double mean){
		if(count <= 0){return 0;}

		double sum = 0;
		for(int i = 0; i < count; i++){
			double diff = ((double) values[i] - mean);
			sum += diff * diff;
		}
		return sqrt(sum / (double) count);
	}

	double vector_mean(std::vector<double>& values){
		if(0 >= values.size()){return 0;}

		double sum = 0;
		std::vector<double>::iterator iter;
		for(iter = values.begin(); iter != values.end(); iter++){
			sum += *iter;
		}

		return sum / (double) values.size();
	}


	double vector_stddev(std::vector<double>& values){
		return vector_stddev(values,vector_mean(values));
	}


	double vector_stddev(std::vector<double>& values, double mean){
		if(0 >= values.size()){return 0;}

		double sum = 0;
		std::vector<double>::iterator iter;
		for(iter = values.begin(); iter != values.end(); iter++){
			double diff =  *iter - mean;
			sum += diff * diff;
		}
		return sqrt( sum / (double) values.size() );
	}


	double vector_com(std::vector<double>& values){
		if(0 >= values.size()){return 0;}

		double sum = 0;
		double weightSum = 0;
		double index = 0;
		std::vector<double>::iterator iter;
		for(iter = values.begin(); iter != values.end(); iter++){

			double weight = fabs(*iter);
			sum += weight * index;
			weightSum += weight;

			index++;
		}

		if(weightSum == 0){
			return 0;
		}

		return sum / weightSum;
	}

	double vector_spread(std::vector<double>& values){
		return vector_spread(values,vector_com(values));
	}

	double vector_spread(std::vector<double>& values,double com){
		if(0 >= values.size()){return 0;}

		double sum = 0;
		double weightSum = 0;
		double index = 0;
		std::vector<double>::iterator iter;
		for(iter = values.begin(); iter != values.end(); iter++){

			double weight = fabs(*iter);
			double dist = index-com;
			sum += weight * dist * dist;	//distance squared, weighted by the distribution
			weightSum += weight;

			index++;
		}

		if(weightSum <= 0 || sum <=0){
			return 0;
		}

		return sqrt(sum / weightSum);
	}


	void vector_minmax(std::vector<double>& values, double* minval, double* maxval, int* minpos, int* maxpos){

		if(values.size() <=0 ){return;}

		double min_value = values[0];
		double max_value = values[0];
		int min_index = 0;
		int max_index = 0;

		int index = 0;
		std::vector<double>::iterator iter;
		for(iter = values.begin(); iter != values.end(); iter++){

			if(*iter < min_value){
				min_value = *iter;
				min_index = index;
			}

			if(*iter > max_value){
				max_value = *iter;
				max_index = index;
			}

			index++;
		}


		if(NULL != minval){
			*minval = min_value;
		}
		if(NULL != maxval){
			*maxval = max_value;
		}
		if(NULL != minpos){
			*minpos = min_index;
		}
		if(NULL != maxpos){
			*maxpos = max_index;
		}
		return;
	}

	double vector_median(std::vector<double>& values){
		//compute the sum of the vector
		if(values.size() <=0 ){return 0;}

		std::vector<double> duplicate = values;
		std::sort(duplicate.begin(),duplicate.end());
		return duplicate[duplicate.size()/2];


	}

	bool overLine(double x,double y,double a,double b){
		return y > (a*x+b);
	}

	unsigned int log2( unsigned int zvalue )
	{
		// log2(0) is undefined so error
		if (zvalue == 0) UL_ERROR(std::domain_error("Log base 2 of 0 is undefined."));

		unsigned int result = 0;

		// Loop until zvalue equals 0
		while (zvalue)
		{
			zvalue >>= 1;
			++result;
		}

		return result - 1;
	}


}
