#include "LinearFit2D.h"
#include <algorithm>

namespace mathutils{

	LinearFit2D::LinearFit2D()
		: a(0)
		, b(0)
		, r(0)
		, xMin(0)
		, xMax(0)
		, valid(false)
	{

	}

	LinearFit2D::LinearFit2D( const LinearFit2DData& p_data )
		: a(0)
		, b(0)
		, r(0)
		, xMin(0)
		, xMax(0)
		, valid(false)
	{
		Fit(p_data);
	}

	LinearFit2D::~LinearFit2D()
	{

	}

	bool LinearFit2D::Fit( const LinearFit2DData& p_data )
	{
		//reset everything
		a = 0;
		b = 0;
		r = 0;
		xMin = 0;
		xMax = 0;
		valid = false;

		if(p_data.size() <= 1){
			//0 or 1 data points: cannot compute;
			valid = false;
		}else if(p_data.size() == 2){
			//2 data points: compute linear fit directly.
			float dx = p_data.at(1).X - p_data.at(0).X;
			float dy = p_data.at(1).Y - p_data.at(0).Y;

			if(dx != 0){
				a = dy/dx;
				b = p_data.at(0).Y - (a * p_data.at(0).X);
				r = 1.0;

				xMin = std::min(p_data.at(1).X,p_data.at(0).X);
				xMax = std::max(p_data.at(1).X,p_data.at(0).X);

				valid = true;
			}else{
				//vertical line: invalid (div by 0)
				valid = false;
			}
		}else{
			//general case: 3 or more points;
			double xAvg = 0;
			double yAvg = 0;
			xMin = p_data.at(0).X;
			xMax = p_data.at(0).X;

			//compute the average values of X and Y, and the min/max of X
			for(auto iter = p_data.begin(); iter != p_data.end(); iter++){
				xAvg += iter->X;
				yAvg += iter->Y;

				if(iter->X < xMin){ xMin = iter->X; }
				if(iter->X > xMax){ xMax = iter->X; }
			}

			xAvg = xAvg / (float) p_data.size();
			yAvg = yAvg / (float) p_data.size();


			double v1 = 0;
			double v2 = 0;

			

			for(auto iter = p_data.begin(); iter != p_data.end(); iter++){
				double x = iter->X;
				double y = iter->Y;

				double xDiff = (x-xAvg);

				v1 += xDiff * (y-yAvg);
				v2 += xDiff*xDiff;

			}

			if(0 == v2){
				//perfectly vertical line
				valid = false;
			}else{
				a = v1/v2;
				b = yAvg - a*xAvg;
				valid = true;

				//now compute R-squared
				// http://en.wikipedia.org/wiki/Coefficient_of_determination
				double ss_tot = 0;
				double ss_reg = 0;
				double ss_res = 0;

				for(auto iter = p_data.begin(); iter != p_data.end(); iter++){
					double x = iter->X;
					double y = iter->Y;

					double f = a*x + b;

					//add to Total Sum of Squares
					double yDiff = y - yAvg;
					ss_tot += (yDiff*yDiff);

					//add to Explained Sum of Squares
					double fiDiff = f - yAvg;
					ss_reg += (fiDiff*fiDiff);

					//add to the Residual Sum of Squares
					double yFiDiff = y - f;
					ss_res += (yFiDiff*yFiDiff);
				}

				if(ss_tot != 0){
					r = 1.0 - (ss_res/ss_tot);
				}else{
					r = 0.0;
				}
			}

		}

		return valid;
	}

	double LinearFit2D::A() const
	{
		return a;
	}

	double LinearFit2D::B() const
	{
		return b;
	}

	double LinearFit2D::R() const
	{
		return r;
	}

	double LinearFit2D::Y( double x ) const
	{
		return a * x + b;
	}

	bool LinearFit2D::Valid() const
	{
		return valid;
	}

	double LinearFit2D::XMin() const
	{
		return xMin;
	}

	double LinearFit2D::XMax() const
	{
		return xMax;
	}

}
