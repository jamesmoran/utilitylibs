#pragma once

#include <vector>
#include <windows.h>
#include <MathUtilFuncs.h>

namespace hardwareutils{

	enum DriveType
	{
		DRIVETYPE_UNKNOWN = 0,
		DRIVETYPE_NO_ROOT_DIR,
		DRIVETYPE_REMOVABLE,
		DRIVETYPE_FIXED,
		DRIVETYPE_REMOTE,
		DRIVETYPE_CDROM,
		DRIVETYPE_DRIVE_RAMDISK,
		DRIVETYPE_NOTADRIVE_USEDTOTEST
	};

	struct DriveInfo
	{
		DriveInfo();
		DriveInfo(DriveType p_type, const std::string& p_name);
		DriveType dtype;
		std::string dname;
	};

	std::string DriveTypeDescriptive(DriveType drive);
	/*	Function: GetLogicalDrivesStringsandType
			Gets all hard drives of the computer in type, name format 
	
		Return:
			std::vector<std::pair<DriveType, std::string>> - List of all drives with their Type and Name.
	*/
	std::vector<DriveInfo> GetLogicalDrivesStringsandType();

	/*	Function: SetWindowsVolumePercent
		Sets the windows volume to the selected percentage.

	Return:
		bool - whether setting the volume was successful 
	*/
	bool SetWindowsVolumePercent(int p_volume);
	int GetWindowsVolumePercent();

	/*	Function: SetApplicationVolumePercent
	Sets the windows volume to the selected percentage.

	Return:
		bool - whether setting the volume was successful
	*/
	bool SetApplicationVolumePercent(int p_volume);
	int GetApplicationVolumePercent();

	BOOL CALLBACK MonitorEnumProc(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData);
	/*	Function: SetBrightnessPercent
	Sets the monitor brightness to the selected percentage.

	Return:
		bool - whether setting the brightness was successful
	*/
	bool SetBrightnessPercent(int p_brightness);
}