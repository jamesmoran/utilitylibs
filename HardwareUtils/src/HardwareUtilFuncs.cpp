#include "HardwareUtilFuncs.h"
#include <physicalmonitorenumerationapi.h>
#include <lowlevelmonitorconfigurationapi.h>
#include <mmdeviceapi.h>
#include <endpointvolume.h>

namespace hardwareutils{


	std::string DriveTypeDescriptive(DriveType drive)
	{
		std::string str;
		switch (drive)
		{
		case hardwareutils::DRIVETYPE_UNKNOWN:
			str = "Unknown Drive Types";
			break;
		case hardwareutils::DRIVETYPE_NO_ROOT_DIR:
			str = "No Root Directorys";
			break;
		case hardwareutils::DRIVETYPE_REMOVABLE:
			str = "Devices with Removable Storage - USB";
			break;
		case hardwareutils::DRIVETYPE_FIXED:
			str = "Hard Disk Drives";
			break;
		case hardwareutils::DRIVETYPE_REMOTE:
			str = "Network Locations";
			break;
		case hardwareutils::DRIVETYPE_CDROM:
			str = "Devices with Removable Storage - CD";
			break;
		case hardwareutils::DRIVETYPE_DRIVE_RAMDISK:
			str = "RAM Disks";
			break;
		default:
			break;
		}
		return str;
	}


	DriveInfo::DriveInfo()
		:dtype(DRIVETYPE_UNKNOWN)
		, dname("A")
	{

	}

	DriveInfo::DriveInfo(DriveType p_type, const std::string& p_name)
		:dtype(p_type)
		, dname(p_name)
	{

	}

	std::vector<DriveInfo> GetLogicalDrivesStringsandType()
	{
		DWORD dwSize = MAX_PATH;
		char szLogicalDrives[MAX_PATH] = { 0 };
		DWORD dwResult = GetLogicalDriveStringsA(dwSize, szLogicalDrives);

		std::vector<DriveInfo> retvect;

		if (dwResult > 0 && dwResult <= MAX_PATH)
		{
			char* szSingleDrive = szLogicalDrives;
			while (*szSingleDrive)
			{
				UINT nDriveType = GetDriveTypeA(szSingleDrive);

				DriveInfo temp = DriveInfo((DriveType)nDriveType, std::string(szSingleDrive));
				retvect.push_back(temp);
				// get the next drive
				szSingleDrive += strlen(szSingleDrive) + 1;
			}
		}
		return retvect;
	}

	bool SetWindowsVolumePercent(int p_volume)
	{
		float newvolume = (float)p_volume / 100.f;
		newvolume = mathutils::clamp(newvolume, 0.f, 1.f);
		
		HRESULT hr = NULL;

		CoInitialize(NULL);
		IMMDeviceEnumerator *deviceEnumerator = NULL;
		hr = CoCreateInstance(__uuidof(MMDeviceEnumerator), NULL, CLSCTX_INPROC_SERVER,
			__uuidof(IMMDeviceEnumerator), (LPVOID *)&deviceEnumerator);
		IMMDevice *defaultDevice = NULL;

		hr = deviceEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &defaultDevice);
		if (hr != S_OK)
		{
			return false;
		}

		deviceEnumerator->Release();
		deviceEnumerator = NULL;

		IAudioEndpointVolume *endpointVolume = NULL;
		hr = defaultDevice->Activate(__uuidof(IAudioEndpointVolume),
			CLSCTX_INPROC_SERVER, NULL, (LPVOID *)&endpointVolume);
		defaultDevice->Release();
		defaultDevice = NULL;

		// -------------------------

		hr = endpointVolume->SetMasterVolumeLevelScalar(newvolume, NULL);
		if (hr != S_OK)
		{
			return false;
		}
		endpointVolume->Release();

		CoUninitialize();

		return true;
	}

	int GetWindowsVolumePercent()
	{
		HRESULT hr = NULL;

		CoInitialize(NULL);
		IMMDeviceEnumerator *deviceEnumerator = NULL;
		hr = CoCreateInstance(__uuidof(MMDeviceEnumerator), NULL, CLSCTX_INPROC_SERVER,
			__uuidof(IMMDeviceEnumerator), (LPVOID *)&deviceEnumerator);
		IMMDevice *defaultDevice = NULL;

		hr = deviceEnumerator->GetDefaultAudioEndpoint(eRender, eConsole, &defaultDevice);
		if (hr != S_OK)
		{
			return false;
		}

		deviceEnumerator->Release();
		deviceEnumerator = NULL;

		IAudioEndpointVolume *endpointVolume = NULL;
		hr = defaultDevice->Activate(__uuidof(IAudioEndpointVolume),
			CLSCTX_INPROC_SERVER, NULL, (LPVOID *)&endpointVolume);
		defaultDevice->Release();
		defaultDevice = NULL;

		// -------------------------

		float thevolume;
		hr = endpointVolume->GetMasterVolumeLevelScalar(&thevolume);
		if (hr != S_OK)
		{
			return -1;
		}
		endpointVolume->Release();

		CoUninitialize();

		int rounded = mathutils::round(thevolume*100.f);
		return rounded;
	}

	bool SetApplicationVolumePercent(int p_volume)
	{
		bool success = false;

		int newvol = mathutils::round(p_volume * 655.35f);
		newvol = mathutils::clamp(newvol, 0, 65535);

		if (waveOutSetVolume(NULL, (DWORD)newvol) == MMSYSERR_NOERROR)
		{
			success = true;
		}
		return success;
	}

	int GetApplicationVolumePercent()
	{
		DWORD dw;
		int volume_raw = -1;
		if (waveOutGetVolume(NULL, &dw) == MMSYSERR_NOERROR)
		{
			volume_raw = mathutils::round((float)dw / 65535.f * 100.f);
		}
		return volume_raw;
	}

	BOOL CALLBACK MonitorEnumProc(HMONITOR hMonitor, HDC hdcMonitor, LPRECT lprcMonitor, LPARAM dwData)
	{
		std::vector<HMONITOR> *monitorList = (std::vector<HMONITOR>*)dwData;
		monitorList->push_back(hMonitor);
		return TRUE;
	}

	bool SetBrightnessPercent(int p_brightness)
	{
		// percentage to value
		bool successs = false;

		int newbright = p_brightness ;
		newbright = mathutils::clamp(newbright, 0, 100);

		std::vector<HMONITOR> monitorList;
		// This function will call the MonitorEnumProc for every monitor that exists on the system
		EnumDisplayMonitors(0, 0, MonitorEnumProc, (LPARAM)(&monitorList));


		for (int n = 0; n < (int)monitorList.size(); ++n)
		{
			DWORD numPhysicalMonitors = 0;
			GetNumberOfPhysicalMonitorsFromHMONITOR(monitorList[n], &numPhysicalMonitors);

			LPPHYSICAL_MONITOR physicalMonitorList = new PHYSICAL_MONITOR[numPhysicalMonitors];
			GetPhysicalMonitorsFromHMONITOR(monitorList[n], numPhysicalMonitors, physicalMonitorList);

			for (int m = 0; m<(int)numPhysicalMonitors; ++m)
			{
				DWORD physicalDescriptionLength = 0;

				BOOL success = GetCapabilitiesStringLength(physicalMonitorList[m].hPhysicalMonitor, &physicalDescriptionLength);

				if (success && physicalDescriptionLength > 0)
				{
					successs = true;

					// VCP feature for brightness is 0x10 value is from 0-255
					SetVCPFeature(physicalMonitorList[m].hPhysicalMonitor, 0x10, (DWORD)newbright);
				}
			}

			DestroyPhysicalMonitors(numPhysicalMonitors, physicalMonitorList);
		}
		return successs;
	}

}
