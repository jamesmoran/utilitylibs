#include "StringUtils.h"

#include <locale>
#include <algorithm>
#include <sstream>
#include <iomanip>
#include <MathConsts.h>
#include <MathUtilFuncs.h>


namespace stringutils{

	bool IsInteger(const std::string& to_parse, bool p_hasleadingzeros /*= false*/)
	{
		static const boost::regex regex_without_leading_zeros("([-+]?[1-9][0-9]*)|0");
		static const boost::regex regex_with_leading_zeros("([-+]?[0-9]+)");

		if (p_hasleadingzeros)
			return boost::regex_match(to_parse, regex_with_leading_zeros);

		return boost::regex_match(to_parse, regex_without_leading_zeros);
	}

	bool ToInteger(const std::string& thestr, int& val)
	{
		if (IsInteger(thestr))
		{
			val = atoi(thestr.c_str());
			return true;
		}
		return false;
	}

	bool IsNetworkAddress(const std::string& strtocheck, bool includeport/*= true*/)
	{
		static const boost::regex regex_without_port("(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])");
		static const boost::regex regex_with_port("(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]):([0-9]{1,4}|[1-5][0-9]{4}|6[0-4][0-9]{3}|65[0-4][0-9]{2}|655[0-2][0-9]|6553[0-5])");

		if (includeport)
			return boost::regex_match(strtocheck, regex_with_port);

		return boost::regex_match(strtocheck, regex_without_port);
	}

	std::wstring ToWideString(const std::string& oldstr)
	{
		std::wstring newstr(oldstr.begin(), oldstr.end());
		return newstr;
	}

	std::string FormatDataQty(__int64 bytes){
		std::stringstream ss;
		ss << std::setprecision(3);
		if(bytes < mathutils::KILOBYTE){
			ss << bytes << " B";
		}else if(bytes < mathutils::MEGABYTE){
			ss << mathutils::Kilobytes(bytes) << " kB";
		}else if(bytes < mathutils::GIGABYTE){
			ss << mathutils::Megabytes(bytes) << " MB";
		}else if(bytes < mathutils::TERABYTE){
			ss << mathutils::Gigabytes(bytes) << " GB";
		}else{
			ss << mathutils::Terabytes(bytes) << " TB";
		}
		return ss.str();
	}

	std::string FormatTimestamp(time_t timestamp,bool iso){

		struct tm timeStruct;
		localtime_s(&timeStruct,&timestamp);

		std::stringstream ss;
		ss << std::setw(4) << std::setfill('0');
		ss << timeStruct.tm_year + 1900 << '-';
		ss << std::setw(2) << timeStruct.tm_mon + 1 << '-';
		ss << std::setw(2) << timeStruct.tm_mday;
		ss << (iso ? 'T' : ' ');
		ss << std::setw(2) << timeStruct.tm_hour << ':';
		ss << std::setw(2) << timeStruct.tm_min << ':';
		ss << std::setw(2) << timeStruct.tm_sec;

		return ss.str();
	}

	bool isValidTextChar(char c, const std::locale& loc){
		if(c == '\n'){
			return true;
		}else if(std::isspace(c,loc)){
			return true;
		}else if(std::iscntrl(c,loc)){
			return false;
		}
		return true;
	}

	std::istream& getline_safe(std::istream& is, std::string& str, char delim, char& endChar, bool& end_of_text){

		const std::locale loc;

		end_of_text = false;
		str = "";

		char currChar;
		while(is){

			is.get(currChar);
			if(is.fail())
			{
				end_of_text = true;
				return is;
			}

			if(!isValidTextChar(currChar,loc))
			{
				end_of_text = true;
				break;
			}
			else if(currChar == delim)
			{
				endChar = currChar;
				break;
			}
			str += currChar;
		}
		if(is.eof() || is.fail())
		{
			end_of_text = true;
		}

		endChar = currChar;
		return is;
	}

	int Split(const std::string& str, char delim, std::vector< std::string >& output){
		std::stringstream ss(str);
		output.clear();

		std::string token;
		while(std::getline(ss,token,delim)){
			output.push_back(token);
		}

		return (int)output.size();
	}

	int Split(const std::wstring& wstr, wchar_t delim, std::vector< std::wstring >& output)
	{
		std::wstringstream wss(wstr);
		output.clear();

		std::wstring token;
		while (std::getline(wss, token, delim)){
			output.push_back(token);
		}

		return (int)output.size();
	}

	std::string Join(const std::vector< std::string > strlist, char glue){
		std::stringstream resultStr;
		std::vector< std::string >::const_iterator iter;
		bool first = true;
		for(iter = strlist.begin(); iter != strlist.end(); iter++){
			if(!first){
				resultStr << glue;
			}
			resultStr << *iter;
			first = false;
		}
		return resultStr.str();
	}

	std::wstring Join(const std::vector< std::wstring >& wstrlist, wchar_t glue)
	{
		std::wstringstream resultWStr;
		std::vector< std::wstring >::const_iterator iter;
		bool first = true;
		for (iter = wstrlist.begin(); iter != wstrlist.end(); iter++){
			if (!first){
				resultWStr << glue;
			}
			resultWStr << *iter;
			first = false;
		}
		return resultWStr.str();
	}

	bool csvGetString(std::istringstream& stream, std::string& val, char delim){
		std::string entry;
		if(std::getline(stream,entry,delim)){
			val = entry;
			return true;
		}
		return false;
	}

	std::string StringManip::Trim(const std::string& str)
	{
		std::string trimmed = str;
		size_t index = str.find_last_not_of(" \n\r\t");
		if (index != std::string::npos)
		{
			trimmed.erase(index+1);
			index = str.find_first_not_of(" \n\r\t");
			if (index != std::string::npos)
				trimmed = trimmed.substr(index);
			else
				trimmed.clear();
		}
		else
			trimmed.clear();
		return trimmed;
	}

	std::vector<std::string> StringManip::Split(const std::string& str, char c)
	{
		std::vector<std::string> tokens;
		std::stringstream ss(str);
		std::string item;
		while(std::getline(ss, item, c)) {
			tokens.push_back(item);
		}
		return tokens;
	}

	std::string StringManip::ToUpper( const std::string& str )
	{
		std::string copy_str = str;
		std::transform(copy_str.begin(), copy_str.end(), copy_str.begin(), ::toupper);
		return copy_str;
	}

	std::string StringManip::ToLower( const std::string& str )
	{
		std::string copy_str = str;
		std::transform(copy_str.begin(), copy_str.end(), copy_str.begin(), ::tolower);
		return copy_str;
	}

	bool Equal( const std::string& A, const std::string& B )
	{
		return 0 == A.compare(B);
	}

	std::string RemoveWhitespace( const std::string p_input )
	{
		std::string temp = p_input;
		temp.erase( std::remove_if( temp.begin(), temp.end(), RemoveDelimiter()), temp.end());
		return temp;
	}

}
