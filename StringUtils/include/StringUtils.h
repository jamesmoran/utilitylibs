#ifndef UTILITYLIBS_STRINGUTILS_H_
#define UTILITYLIBS_STRINGUTILS_H_

#include <vector>
#include <string>
#include <ctype.h>
#include <iosfwd>
#include <sstream>
#include <time.h>
#include <iomanip>
#include "CoreDefines.h"

#include <boost/regex.hpp>

/*	Namespace: stringutils
	Functions and classes for the purpose of string manipulation, formatting and conversion 
	to other types.
*/
namespace stringutils{
	/*	Function IsInteger
	Checks if the given string is an integer.
	*/
	MMI_EXPORT bool IsInteger(const std::string& to_parse, bool p_hasleadingzeros = false);

	/*	Function IsInteger
	Checks if the given string is an integer.
	*/
	MMI_EXPORT bool ToInteger(const std::string& thestr, int& val);

	/*	Function IsNetworkAddress
	Checks if the given string is a valid network address.
	*/
	MMI_EXPORT bool IsNetworkAddress(const std::string& strtocheck, bool includeport= true);

	/*	Function ToWideString
		Returns the wide string equivalent of the given string.
	*/
	MMI_EXPORT std::wstring ToWideString(const std::string& oldstr);

	/*	Function FormatDataQty
		Formats a data quantity and an amount of bytes into human-readable format to two decimal places
		with the appropriate units for the quantity.
	*/
	MMI_EXPORT std::string FormatDataQty(__int64 bytes);

	/*	Function FormatTimestamp
		Formats the time_t timestamp as a data/time field in the format yyyy-mm-dd hh:mm:ss in compliance
		with the iso standard. If the iso flag is set, the format inserts the 'T' character between the
		date and time, resulting in yyyy-mm-ddThh:mm:ss
	*/
	MMI_EXPORT std::string FormatTimestamp(time_t timestamp, bool iso = false);

	/*	Function: isValidTextChar
		Determines if the supplied character is valid in the specified locale. This permits parsing
		of files that may contain control characters or uncontrolled values prior to passing them
		to functions that assume all characters will be valid text.
	*/
	MMI_EXPORT bool isValidTextChar(char c, const std::locale& loc);

	/*	Function getline_safe
		Gets a line from an input stream, explicitly checking each character
		for a delimiter character, as well as non-printable characters such as NULL
		or other control characters. 
	
		This is to avoid loading extremely large numbers
		of characters when there is binary data in a file that should terminate text
		loading, but does not match the supplied delimiter character

		Parameters:
			is - The input stream to be read from. Reading starts at the current cursor position.
			str - The output string. This string will be cleared under all conditions, and then
				incrementally filled with the line value up to the delimiting character or any control
				characters such as NULL or ESC. Control or delimiter characters are not added to the string.
			delim - a delimiter character to demarcate the end of a 'line'. Can be set to any control
				character such as the NULL '\0' character if lines shouldn't break except for control chars.
				Traditionally this is set to the endline character '\n'.
			endChar - An output parameter which is set to the last character read. This includes any delimiter
				characters or control characters, and shows what exact condition stopped the read process. If the function
				is totally unable to read any characters at all from the stream, this character is left untouched.
			end_of_text - This output flag is set to false if the getline_safe operation terminates due to encountering
				a delimiter - getline_safe could be run again and conceivably capture another line. end_of_text is set to true
				if the stream encounters the EOF, a failure condition, or a control character, then this flag is set to true
				indicating that subsequent getline_safe operations are likely to fail.
		
		See Also: 
		getline_safe relies on the <isValidTextChar> function to classify each character.
	*/
	MMI_EXPORT std::istream& getline_safe(std::istream& is, std::string& str, char delim, char& endChar, bool& end_of_text);

	/*	Function: Split
		Divides a string into tokens based on the presence of a delimiter character delim, inserting the
		results into an output vector.

		See Also: 
		Join
	*/
	MMI_EXPORT int Split(const std::string& str, char delim, std::vector< std::string >& output);
	MMI_EXPORT int Split(const std::wstring& wstr, wchar_t delim, std::vector< std::wstring >& output);
	/*	Function: Join
		Concatenates a vector of strings into a single output string with the specified "glue" character between 
		each successive string.

		See Also: 
		Split
	*/
	MMI_EXPORT std::string Join(const std::vector< std::string > strlist, char glue);
	MMI_EXPORT std::wstring Join(const std::vector< std::wstring >& wstrlist, wchar_t glue);
	/*	Function: csvGetString
		Takes the next entry from a delimited stream and provides it as-is into the output
		parameter val.

		Parameters:
			stream - the istream containing the delimited value(s).
			val - the reference parameter (string) that will receive the value on success.
			delim - optional parameter specifies the delimiter character: default is the comma ','.

		Returns: 
		Returns true on success, false on failure. Failure can result from reaching the end 
		of the stream.
	*/
	MMI_EXPORT bool csvGetString(std::istringstream& stream,std::string& val, char delim = ',');

	/*	Function: csvGet
		Templatized function, takes the next entry from a delimited stream and performs a lexical
		cast to the templated type of the output parameter val.

		Parameters:
			stream - the istream containing the delimited value(s).
			val - the reference parameter that will receive the value on success.
			delim - optional parameter specifies the delimiter character: default is the comma ','.

		Returns: 
		Returns true on success, false on failure. Failure can result from bad formatting of 
		the next delimited entry, or reaching the end of the stream.

		Warning: 
		Internally the template uses the stream >> operator to perform conversion of the
		formatted stream contents to the target type. The stream >> operator takes only the first element
		(whitespace separated) and converts it. So, if your stream contains the following: "1 2 3,4 5"
		then the first call to csvGet(stream,myInt,',') will set myInt to 1. The second identical call
		will set myInt to 4. The whitespace after the 1 and after the 4 prevent anything beyond them from
		being processed. This is particularly important if you're using csvGet to acquire strings. The output
		will only contain the csv token contents up to the first non-leading space. If you want to capture 
		strings with spaces in them, use the <csvGetString> function instead.

		See Also: 
		csvGetString
	*/
	template<class T>
	bool csvGet(std::istringstream& stream, T& val,char delim = ','){
		std::string entry;
		if(std::getline(stream,entry,delim)){
			std::stringstream parserSS(entry);
			parserSS >> val;
			return !!parserSS;
		}
		return false;
	}

	/*	Function: csvGet
		Templatized function, takes the next n entries from a delimited stream and performs a lexical
		cast to the templated type, inserting it into the output list parameter valArray.

		Parameters:
			stream - the istream containing the delimited value(s).
			valArray - a pointer to the initial entry of an array of type T. This array MUST have a length of
				at least n or the function will crash.
			delim - optional parameter specifies the delimiter character: default is the comma ','.

		Returns: 
		Returns true on success, false on failure. Failure can result from bad formatting of 
		the next delimited entry, or reaching the end of the stream before n entries are extracted.

		Warning: 
		Internally the template uses the stream >> operator to perform conversion of the
		formatted stream contents to the target type. The stream >> operator takes only the first element
		(whitespace separated) and converts it. So, if your stream contains the following: "1 2 3,4 5"
		then the first call to csvGet(stream,myInt,',') will set myInt to 1. The second identical call
		will set myInt to 4. The whitespace after the 1 and after the 4 prevent anything beyond them from
		being processed. This is particularly important if you're using csvGet to acquire strings. The output
		will only contain the csv token contents up to the first non-leading space. If you want to capture 
		strings with spaces in them, use the <csvGetString> function instead.

		See Also: 
		csvGetString
	*/
	template<class T>
	bool csvGet(std::istringstream& stream, T* valArray, int n,char delim = ','){
		for(int i = 0; i < n; i++){
			T val;
			if(!csvGet(stream,val,delim)){
				return false;
			}
			valArray[i] = val;
		}
		return true;
	}

	template<class T>
	std::string str(T val,int padWidth = 0,char padChar = ' '){
		std::stringstream ss;
		if(padWidth > 0){
			ss << std::setw(padWidth) << std::setfill(padChar);
		}
		ss << val;
		return ss.str();
	}

	class StringManip
	{
	public:
		MMI_EXPORT static std::string ToUpper(const std::string& str);
		MMI_EXPORT static std::string ToLower(const std::string& str);
		MMI_EXPORT static std::string Trim(const std::string& str);
		MMI_EXPORT static std::vector<std::string> Split(const std::string& str, char c);

		template<typename T>
		static T lexical_cast(const std::string& str)
		{
			std::stringstream ss;
			ss << str;
			T val;
			ss >> val;
			return val;
		}

		template<typename T>
		static bool lexical_cast(const std::string& str, T& output)
		{
			std::stringstream ss;
			ss << str;
			T val;
			ss >> val;
			if(!!ss){
				output = val;
				return true;
			}
			return false;
		}
	};

	struct RemoveDelimiter
	{
		bool operator()(char c)
		{
			return (c =='\r' || c =='\t' || c == ' ' || c == '\n');
		}
	};

	MMI_EXPORT bool Equal(const std::string& A, const std::string& B);

	MMI_EXPORT std::string RemoveWhitespace(const std::string p_input);


}

#endif