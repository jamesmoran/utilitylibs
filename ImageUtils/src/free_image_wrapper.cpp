// jpeg_tiling.cpp : Defines the entry point for the console application.
//

#include "ImageUtils.h"
#include <algorithm>

namespace imageutils
{
	const FREE_IMAGE_MDMODEL metadata_model = FIMD_COMMENTS;
	
	void error_handler(const char *message);
	void error_handler(FREE_IMAGE_FORMAT fif, const char *message);
	
	struct initilizer
	{
		initilizer();
		~initilizer();
	};
	
	
	struct metadata_tag
	{
		metadata_tag(const char *name, const std::string &val);
		metadata_tag(FIBITMAP *b);
		~metadata_tag();
		
		const char *key() const;
		const char *value() const;
		
		FITAG *tag() const { return tag_; }
		
	private:
		FITAG *tag_;
		std::string value_;				// only valid when creating tag_ by name
		bool release_at_destruction;
		void init(const char *name, const std::string &val);
	};
}

imageutils::initilizer use_lib;


void imageutils::error_handler(const char *message)
{
	//print_message("FI: %s\n", message);
}

void imageutils::error_handler(FREE_IMAGE_FORMAT fif, const char *message)
{
	//if (fif != FIF_UNKNOWN)
		//print_message("FI: %s Format. %s\n", FreeImage_GetFormatFromFIF(fif), message);
	//else
		//print_message("FI: %s\n", message);
}



imageutils::initilizer::initilizer()
{
	FreeImage_Initialise();
	FreeImage_SetOutputMessage(error_handler);
//	print_message("FreeImage library version %s\n\n", FreeImage_GetVersion());
}


imageutils::initilizer::~initilizer()
{
	FreeImage_DeInitialise();
}



imageutils::metadata_tag::metadata_tag(const char *name, const std::string &val)
	: tag_(0), release_at_destruction(true)
{
	init(name, val);
}

imageutils::metadata_tag::metadata_tag(FIBITMAP *b)
	: tag_(0), release_at_destruction(false)
{
	FIMETADATA *handle = FreeImage_FindFirstMetadata(metadata_model, b, &tag_);
	if (handle)
		FreeImage_FindCloseMetadata(handle);
}

imageutils::metadata_tag::~metadata_tag()
{
	if(release_at_destruction)
		FreeImage_DeleteTag(tag_);
}

const char *imageutils::metadata_tag::key() const
{
	return FreeImage_GetTagKey(tag_);
}

const char *imageutils::metadata_tag::value() const
{
	return static_cast<const char *>(FreeImage_GetTagValue(tag_));
}

void imageutils::metadata_tag::init(const char *name, const std::string &val)
{
	tag_ = FreeImage_CreateTag();

	if (!tag_)
		return;

	value_ = val;
	const size_t length = value_.size() + 1;

	FreeImage_SetTagKey(tag_, name);
	FreeImage_SetTagType(tag_, FIDT_ASCII);
	FreeImage_SetTagLength(tag_, static_cast<DWORD>(length));
	FreeImage_SetTagCount(tag_, static_cast<DWORD>(length));
	FreeImage_SetTagValue(tag_, value_.c_str());
}


imageutils::image::image()
	: ref_count(new size_t(1))
{
	init(0);
}

imageutils::image::image(const char *name)
	: ref_count(new size_t(1))
{
	init(name);
}

imageutils::image::image(const std::string &name)
	: ref_count(new size_t(1))
{
	init(name.c_str());
}

imageutils::image::image(size_t w, size_t h, size_t pitch, size_t bpp,
						 const BYTE *data, bool upside_down, bool convert_to_grayscale)
	: ref_count(new size_t(1))
{
	init(data, w, h, pitch, bpp, upside_down, convert_to_grayscale);
}

imageutils::image::image(size_t w, size_t h, size_t bpp, const BYTE *data,
						 bool upside_down, bool convert_to_grayscale)
	: ref_count(new size_t(1))
{
	// for the pitch, assume no padding at the end of each line
	const size_t pitch = w * bpp / 8;
	init(data, w, h, pitch, bpp, upside_down, convert_to_grayscale);
}

// imageutils::image::image(CBitmap &b)
// 	: ref_count(new size_t(1))
// {
// 	BITMAP info;
// 	b.GetBitmap(&info);
// 
// 	const size_t raw_data_size = info.bmHeight * info.bmWidthBytes;
// 	BYTE *raw_data = new BYTE[raw_data_size];
// 	b.GetBitmapBits(raw_data_size, raw_data);
// 
// 	init(raw_data, info.bmWidth, info.bmHeight, info.bmWidthBytes, info.bmBitsPixel,
// 		 false, false);
// 
// 	delete[] raw_data;
// }

// take ownership of the FIBITMAP object
imageutils::image::image(FIBITMAP *b)
	: bitmap(b), ref_count(new size_t(1))
{
}

// implement reference-counting
imageutils::image::image(const image &i)
	: bitmap(i.bitmap), ref_count(i.ref_count)
{
	if (ref_count)
	++*ref_count;
}

imageutils::image &imageutils::image::operator=(const image &i)
{
	image temp(i);
	swap(temp);
	return *this;
}

imageutils::image::~image()
{

	if (!ref_count || --*ref_count != 0)
		return;

	FreeImage_Unload(bitmap);			// can handle null bitmaps
	delete ref_count;
	ref_count = 0;
	bitmap = 0;
}


bool imageutils::image::operator!() const
{
	return ref_count == 0 || bitmap == 0;
}

bool imageutils::image::set_metadata(const std::string &comment)
{
	metadata_tag the_tag("Comment", comment);
	return 0 != FreeImage_SetMetadata(metadata_model, bitmap,
								 the_tag.key(), the_tag.tag());
}

std::string imageutils::image::get_metadata() const
{
	metadata_tag the_tag(bitmap);
	const char *data = the_tag.value();

	if (!data)
		return "";

	return data;
}

void imageutils::image::init(const BYTE *data, size_t w, size_t h, size_t pitch,
							 size_t bpp, bool upside_down, bool convert_to_grayscale)
{

	if (!data)
		bitmap = FreeImage_Allocate(static_cast<int>(w), static_cast<int>(h), static_cast<int>(bpp));
	else
	{
		unsigned red_mask, green_mask, blue_mask;

		switch (bpp)
		{
		case 32:
		case 24:
			red_mask = FI_RGBA_RED_MASK;
			green_mask = FI_RGBA_GREEN_MASK;
			blue_mask = FI_RGBA_BLUE_MASK;
			break;
		case 16:
			red_mask = FI16_565_RED_MASK;
			green_mask = FI16_565_GREEN_MASK;
			blue_mask = FI16_565_BLUE_MASK;
/*
			red_mask = FI16_555_RED_MASK;
			green_mask = FI16_555_GREEN_MASK;
			blue_mask = FI16_555_BLUE_MASK;
*/
			break;
		case 8:
		case 4:
		case 1:
		default:
			red_mask = 0;
			green_mask = 0;
			blue_mask = 0;
			break;
		};

		bitmap = FreeImage_ConvertFromRawBits(const_cast<BYTE *>(data), static_cast<int>(w), static_cast<int>(h),
			static_cast<int>(pitch), static_cast<uint32_t>(bpp), static_cast<uint32_t>(red_mask), static_cast<uint32_t>(green_mask), static_cast<uint32_t>(blue_mask), static_cast<uint32_t>(upside_down));
	}

	if (!bitmap)
		return;

	if ( convert_to_grayscale && (bpp == 4 || bpp == 8) )
	{ 
		// build a grey-scale palette 
		RGBQUAD *pallete = FreeImage_GetPalette(bitmap); 
	
		for(uint32_t i = 0; i != (1 << bpp); ++i) 
		{ 
			BYTE ib = static_cast<BYTE>(i);
			pallete[i].rgbRed = ib; 
			pallete[i].rgbGreen = ib; 
			pallete[i].rgbBlue = ib; 
		}
	}
}


void imageutils::image::convert_to_8bpp()
{
	image converted(FreeImage_ConvertTo8Bits(bitmap));
	swap(converted);
}


void imageutils::image::convert_to_24bpp()
{
	image converted(FreeImage_ConvertTo24Bits(bitmap));
	swap(converted);
}

void imageutils::image::flip_image(bool vertical /* = true */)
{
	if (vertical)
	{
		FreeImage_FlipVertical(bitmap);
	}
	else
	{
		FreeImage_FlipHorizontal(bitmap);
	}
}

bool imageutils::image::tile(const image &i, size_t x, size_t y)
{
	// 256 or larger == no alpha blending
	return 0 != FreeImage_Paste(bitmap, i.bitmap, static_cast<int>(x), static_cast<int>(y), 256);
}


imageutils::image imageutils::image::
				do_get_resized_copy(size_t new_width, size_t new_height) const
{
	const size_t current_width = width();
	const size_t current_height = height();

	if (new_width == current_width && new_height == current_height)
		return *this;		// nothing to do

	image new_image(new_width, new_height, bpp());
	bool result = true;

	// only try and access the current image data if there is something there
	if (current_width != 0 && current_height != 0)
	{
		if (new_width > current_width && new_height > current_height)
			result = new_image.tile(*this, 0, 0);
		else
		{
			new_width = std::min(new_width, current_width);
			new_height = std::min(new_height, current_height);

			image old_image_section(FreeImage_Copy(bitmap, 0, 0, static_cast<int>(new_width), static_cast<int>(new_height)));

			result = new_image.tile(old_image_section, 0, 0);
		}
	}

	if (result)
		return new_image;

	return image();
}



bool imageutils::image::resize(size_t new_width, size_t new_height)
{
	image resized(do_get_resized_copy(new_width, new_height));
	if (!resized)
		return false;

	swap(resized);
	return true;
}


bool imageutils::image::get_8bpp_raw_data(BYTE *data,
										  size_t dest_width, size_t dest_height,
										  size_t origin_x, size_t origin_y) const
{
	const size_t src_height = height();
	if ( bpp() != 8 || origin_x+dest_width > width() || origin_y+dest_height > src_height )
		return false;

	// images are stored upside down
	for (size_t y = 0; y != dest_height; ++y)
		for (size_t x = 0; x != dest_width; ++x)
			FreeImage_GetPixelIndex( bitmap , static_cast<uint32_t>(x+origin_x),
									 static_cast<uint32_t>(src_height-origin_y-dest_height+y),
									 data+(dest_height-1-y)*dest_width+x );

	return true;
}

bool imageutils::image::get_24bpp_raw_data(BYTE *data,
										   size_t dest_width, size_t dest_height,
										   size_t origin_x, size_t origin_y) const
{
	const size_t src_height = height();
	const size_t src_width = width();
	if ( bpp() != 24 || origin_x+dest_width > src_width || origin_y+dest_height > src_height )
		return false;

	image image_section(FreeImage_Copy(bitmap, static_cast<int>(origin_x), static_cast<int>(origin_y),
		static_cast<int>(origin_x + dest_width), static_cast<int>(origin_y + dest_height)));
	image_section.flip_image();

	FreeImage_ConvertToRawBits(data, image_section.bitmap, static_cast<uint32_t>(3*src_width), 24,
							   FI_RGBA_RED_MASK, FI_RGBA_GREEN_MASK, FI_RGBA_BLUE_MASK);

	return true;
}

bool imageutils::image::save_jpeg(const char *name, unsigned quality) const
{
	//valid quality options are:
	//  JPEG_QUALITYSUPERB
	//	JPEG_QUALITYGOOD
	//	JPEG_QUALITYNORMAL
	//	JPEG_QUALITYAVERAGE
	//	JPEG_QUALITYBAD
	try
	{
		return 0 != FreeImage_Save(FIF_JPEG, bitmap, name, JPEG_PROGRESSIVE|quality);
	}
	catch (...)	//didn't want to use a catchall here, but there is no literature on what stuff this throws (and it can throw)
	{
		return false;
	}
}

bool imageutils::image::save_jpeg( const std::string &name, unsigned quality ) const
{
	return save_jpeg(name.c_str(), quality);
}

bool imageutils::image::save_bmp(const char *name) const
{
	// RLE only works for 8bpp images; it'll be ignored for everything else
	try
	{
		return 0 != FreeImage_Save(FIF_BMP, bitmap, name, BMP_SAVE_RLE);
	}
	catch (...)
	{
		return false;
	}
}

bool imageutils::image::save_bmp( const std::string &name ) const
{
	return save_bmp(name.c_str());
}

void imageutils::image::init(const char *name)
{
	if (!name)
		bitmap = 0;
	else
		bitmap = FreeImage_Load( FreeImage_GetFileType(name), name );
}

void imageutils::image::swap( image &i ) throw()
{
	std::swap(bitmap, i.bitmap);
	std::swap(ref_count, i.ref_count);
}
