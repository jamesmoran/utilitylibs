#include "imageUtils.h"
#include <MathUtils.h>
#include <cstring>
#include <algorithm>


using namespace std;

namespace imageutils{

bool ucRoiCopy(const unsigned char* src,unsigned char* dst,int src_width,int src_height,int ROI_left,int ROI_top,int ROI_width, int ROI_height){
	
	//zero the destination buffer
	memset(dst,0,ROI_width*ROI_height*sizeof(unsigned char));
	
	//for each pixel that is both in the ROI and the source buffer
	for(int src_x = std::max(0,ROI_left); src_x < std::min(src_width,ROI_left+ROI_width); src_x++){
		for(int src_y = std::max(0,ROI_top); src_y < std::min(src_height,ROI_top+ROI_height); src_y++){
			//compute the index in the source buffer
			int src_index = src_y*src_width + src_x;
			
			//compute the position in the destination buffer
			int dst_x = src_x - ROI_left;
			int dst_y = src_y - ROI_top;
			
			//compute the index in the destination buffer
			int dst_index = dst_y*ROI_width + dst_x;
			
			//copy the pixel
			dst[dst_index] = src[src_index];
		}
	}
	return true;
}

bool uc2IplRoiCopy(const unsigned char* src, IplImage* dst,int src_width, int src_height, int ROI_left, int ROI_top, int ROI_width, int ROI_height){
	//for each pixel that is both in the ROI and the source buffer
	for(int src_x = std::max(0,ROI_left); src_x < std::min(src_width,ROI_left+ROI_width); src_x++){
		for(int src_y = std::max(0,ROI_top); src_y < std::min(src_height,ROI_top+ROI_height); src_y++){
			//compute the index in the source buffer
			int src_index = src_y*src_width + src_x;
			
			//compute the position in the destination buffer
			int dst_x = src_x - ROI_left;
			int dst_y = src_y - ROI_top;
			
			if(dst_x >= 0 && dst_x < dst->width && dst_y >= 0 && dst_y < dst->height){
				//compute the index in the destination buffer
				int dst_index = dst_y*dst->widthStep + dst_x;
				
				//copy the pixel
				dst->imageData[dst_index] = src[src_index];
			}
		}
	}
	return true;
}

int ucSum(const unsigned char* src, int length){
	int sum = 0;
	for(int i = 0; i < length; i++){
		sum += (int) src[i];
	}
	return sum;
}

bool checkBlank(const unsigned char* src, int length){
	int sum = ucSum(src,length);
	return sum == 0 || sum == 255*length;
}


bool uc2ipl(const unsigned char* src,IplImage* dst,int width, int height){
	int x,y;
	if(src == NULL || dst == NULL || dst->width != width || dst->height != height){
		return false;
	}
	for(y = 0; y < height; y++){
		for(x = 0; x < width; x++){
			dst->imageData[y*dst->widthStep+x] = src[y*width+x];
		}
	}
	return true;
}

bool ipl2uc(const IplImage* src, int xmax, int ymax, unsigned char* dst){
	if(src == NULL || dst == NULL || src->width < xmax || src->height < ymax){
		return false;
	}
	for(int y = 0; y < ymax && y < src->height; y++){
		for(int x = 0; x < xmax && x < src->width; x++){
			dst[y*xmax+x] = src->imageData[y*src->widthStep+x];
		}
	}
	return true;
}

bool int2uc(int* src,unsigned char* dst,int width,int height){
	if(NULL == src || NULL == dst){return false;}
	
	for(int y = 0; y < height; y++){
		for(int x = 0; x < width; x++){
			dst[y*width+x] = (unsigned char) src[y*width+x];
		}
	}
	return true;
}

bool ucCopyFromROI(unsigned char* src,unsigned char* dst,int src_width, int src_height, int x, int y, int dst_width, int dst_height){
	int dst_x,dst_y,src_x,src_y;
	
	for(dst_y = 0; dst_y < dst_height; dst_y++){
		for(dst_x = 0; dst_x < dst_width; dst_x++){
			
			src_x = x+dst_x;
			src_y = y+dst_y;
			
			int dst_index = dst_y*dst_width+dst_x;
			int src_index = src_y*src_width+src_x;
			
			dst[dst_index] = src[src_index];
		}
	}
	
	return true;
}


bool ucCopyToROI(unsigned char* src,unsigned char* dst,int src_width, int src_height,int dst_width,int dst_height,int x,int y){
	int dst_x,dst_y,src_x,src_y;
	
	for(src_y = 0; src_y < src_height; src_y++){
		for(src_x = 0; src_x < src_width; src_x++){
			dst_x = x + src_x;
			dst_y = y + src_y;
			
			int src_index = src_y*src_width+src_x;
			int dst_index = dst_y*dst_width+dst_x;
			
			dst[dst_index] = src[src_index];
		}
	}
	return true;
}


IplImage* stackImages(std::vector<IplImage*>& images){

	int width = 0;
	int height = 0;
	int depth = 0;
	int nChannels = 0;

	bool first = true;

	int i = 0;
	int validCount = 0;

	//find the image dimensions, and verify that the channels and depth all match
	for(i = 0; i < (int) images.size(); i++){
		if(NULL == images[i]){
			continue;
		}

		if(first){
			depth = images[i]->depth;
			nChannels = images[i]->nChannels;
			first = false;
		}else{
			if(depth != images[i]->depth || nChannels != images[i]->nChannels){
				continue;
			}
		}

		CvRect roi = cvGetImageROI(images[i]);
		if(roi.width > width){ width = roi.width; }
		height += roi.height;
		validCount++;
	}

	if(validCount <= 0){
		return NULL;
	}

	if(width <= 0 || height <= 0){
		return NULL;
	}

	//allocate an appropriately-sized image
	IplImage* result_img = cvCreateImage(cvSize(width,height),depth,nChannels);
	cvZero(result_img);

	int y = 0;

	//copy the images into the required ROIs
	for(i = 0; i < (int) images.size(); i++){
		if(NULL == images[i]){
			continue;
		}

		if(depth != images[i]->depth || nChannels != images[i]->nChannels){
			continue;
		}

		CvRect src_roi = cvGetImageROI(images[i]);
		CvRect dst_roi = src_roi;
		dst_roi.y = y;
		dst_roi.x = 0;

		cvSetImageROI(result_img,dst_roi);
		cvCopy(images[i],result_img);

		y += dst_roi.height;


	}

	cvResetImageROI(result_img);

	return result_img;
}

}
