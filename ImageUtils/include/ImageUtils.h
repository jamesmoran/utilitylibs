#ifndef UTILITYLIBS_IMAGEUTILS_H_
#define UTILITYLIBS_IMAGEUTILS_H_

#include <opencv/cv.h>
#include <vector>
#include <string>
#include <freeimage.h>

namespace imageutils{

	struct image
	{
		image();
		explicit image(const char *name);
		explicit image(const std::string &name);
		//image(CBitmap &b);
		image(size_t w, size_t h, size_t bpp, const BYTE *data = 0,
			bool upside_down = false, bool convert_to_grayscale = false);
		image(size_t w, size_t h, size_t pitch, size_t bpp, const BYTE *data = 0,
			bool upside_down = false, bool convert_to_grayscale = false);
		image(FIBITMAP *b);		// take ownership of the FIBITMAP object

		image(const image &i);
		image &operator=(const image &i);

		~image();

		bool operator!() const;

		bool set_metadata(const std::string &comment);
		std::string get_metadata() const;

		void convert_to_8bpp();
		void convert_to_24bpp();
		void flip_image(bool vertical = true);

		bool get_8bpp_raw_data(BYTE *data, size_t width, size_t height,
			size_t origin_x, size_t origin_y) const;
		bool get_24bpp_raw_data(BYTE *data, size_t width, size_t height,
			size_t origin_x, size_t origin_y) const;

		// Method: save_jpeg
		// Parameters:
		//   - name: The filename of the target file
		//   - quality: jpeg lossy compression quality. Valid options are:
		//          JPEG_QUALITYSUPERB
		//			JPEG_QUALITYGOOD
		//			JPEG_QUALITYNORMAL
		//			JPEG_QUALITYAVERAGE
		//			JPEG_QUALITYBAD
		bool save_jpeg(const std::string &name, unsigned quality = JPEG_QUALITYNORMAL) const;
		bool save_bmp(const std::string &name) const;

		// Method: save_jpeg
		// Parameters:
		//   - name: The filename of the target file
		//   - quality: jpeg lossy compression quality. Valid options are:
		//          JPEG_QUALITYSUPERB
		//			JPEG_QUALITYGOOD
		//			JPEG_QUALITYNORMAL
		//			JPEG_QUALITYAVERAGE
		//			JPEG_QUALITYBAD
		bool save_jpeg(const char *name, unsigned quality  = JPEG_QUALITYNORMAL) const;
		bool save_bmp(const char *name) const;

		bool tile(const image &i, size_t x, size_t y);

		bool resize(size_t new_width, size_t new_height);

		size_t width() const { return FreeImage_GetWidth(bitmap); }
		size_t height() const { return FreeImage_GetHeight(bitmap); }
		unsigned bpp() const { return FreeImage_GetBPP(bitmap); }
	private:
		void init(const char *name);
		void init(const BYTE *data, size_t w, size_t h, size_t pitch, size_t bpp,
			bool upside_down, bool convert_to_grayscale);

		FIBITMAP *bitmap;

		image do_get_resized_copy(size_t new_width, size_t new_height) const;

		void swap(image &i) throw();

		size_t *ref_count;
	};

bool ucRoiCopy(const unsigned char* src,unsigned char* dst,int src_width,int src_height,int ROI_left,int ROI_top,int ROI_width, int ROI_height);
bool uc2IplRoiCopy(const unsigned char* src, IplImage* dst,int src_width, int src_height, int ROI_left, int ROI_top, int ROI_width, int ROI_height);

int ucSum(const unsigned char* src, int length);
bool checkBlank(const unsigned char* src, int length);

bool uc2ipl(const unsigned char* src,IplImage* dst,int width, int height);
bool ipl2uc(const IplImage* src, int xmax, int ymax, unsigned char* dst);
bool int2uc(int* src,unsigned char* dst,int width,int height);

bool ucCopyFromROI(unsigned char* src,unsigned char* dst,int src_width, int src_height, int x, int y, int dst_width, int dst_height);
bool ucCopyToROI(unsigned char* src,unsigned char* dst,int src_width, int src_height,int dst_width,int dst_height,int x,int y);

IplImage* stackImages(std::vector<IplImage*>& images);

}

#endif
