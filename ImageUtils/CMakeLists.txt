set(LIBRARY_NAME ImageUtils)
set(INCLUDE_FILES
	include/CoreDefines.h
	include/ImageUtils.h
)
set(SRC_FILES
	src/free_image_wrapper.cpp
	src/ImageUtils.cpp
)

find_package(OpenCV REQUIRED)
if (OpenCV_FOUND)
else()
	message("could not find OpenCV")
endif()

add_library(${LIBRARY_NAME} SHARED
	${INCLUDE_FILES}
	${SRC_FILES}
)

target_include_directories(${LIBRARY_NAME} PUBLIC
	include/
	${OPENCV_INCLUDE_DIRS}
)
target_link_libraries(${LIBRARY_NAME}
	${OpenCV_LIBRARIES}
)
set(${LIBRARY_NAME}_INCLUDE_DIRS 
	${CMAKE_CURRENT_SOURCE_DIR}include/
	${OpenCV_INCLUDE_DIRS}
)
install(TARGETS
	${LIBRARY_NAME}
	RUNTIME DESTINATION ${INSTALL_BINARIES_DIR} COMPONENT Runtime
	ARCHIVE DESTINATION ${INSTALL_LIBRARIES_DIR} COMPONENT Runtime
)
install(FILES
	${INCLUDE_FILES}
	DESTINATION ${INSTALL_HEADERS_DIR}/${LIBRARY_NAME}
)
