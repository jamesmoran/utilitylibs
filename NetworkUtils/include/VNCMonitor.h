#pragma once

#include "TCPTable.h"
#include <time.h>

namespace networkutils{

	/*	Class: VNCMonitor
		The VNCMonitor determines the current state of a VNC server on a specified
		port (default is 5900). Based on the state of the port, the VNCMonitor infers
		the state of the server. If the port is not open, the server is "not found". 
		If the port is open but listening, then the server is "active". If the port 
		is open and a connection is established to a remote address, then the server 
		is "connected".
	*/
	class VNCMonitor{
	public:

		/*	Variable: VNC_PORT
			The constant that determines the VNC port number. This is
			hardcoded to 5900.
		*/
		static const unsigned short VNC_PORT;

		/*	Method: VNCMonitor
			Create a new VNCMonitor. Note: the monitor will not contain any useful
			information until <Refresh> is called.

			Parameters:
				port - The port number to watch on the local machine. This defaults
				to 5900, the default VNC server port.
		*/
		MMI_EXPORT VNCMonitor(unsigned short port = VNC_PORT);

		/*	Method: Refresh
			Query the TCP table and check to see if a VNC server is active, and if
			so, is there a connection. This function populates all the VNCMonitor's
			fields, making the other Get____ and Is____ functions return meaningful
			data.
		
			Return:
				bool - Returns true if an active VNC server is detected, regardless
				of current VNC status.
		*/
		MMI_EXPORT bool Refresh();

		/*	Method: IsActive
			Returns true if the last Refresh detected an active VNC server. (i.e.
			the port was open).
		*/
		MMI_EXPORT bool IsActive() const;

		/*	Method: IsConnected
			Returns true if the last Refresh detected a VNC client connected to the
			VNC server. (i.e. the port's status was [connection] "ESTABLISHED").
		*/
		MMI_EXPORT bool IsConnected() const;

		/*	Method: GetStatus
			Returns the status of the port at the time of the last Refresh. The DWORD
			should be interpreted according to the MIB_TCP_STATE enumeration in tcpmib.h.
		*/
		MMI_EXPORT DWORD GetStatus() const;

		/*	Method: GetStatusString
			Get an [English] string interpretation of the port's status.
		*/
		MMI_EXPORT std::string GetStatusString() const;

		/*	Method: GetLocalAddress
			Get an <Address> object corresponding to the local port status. This
			is only meaningful if <IsActive> is true.
		*/
		MMI_EXPORT Address GetLocalAddress() const;

		/*	Method: GetRemoteAddress
			Get an <Address> object corresponding to the remote port status. This
			is only meaningful if <IsConnected> is true.
		*/
		MMI_EXPORT Address GetRemoteAddress() const;

		/*	Method: Port
			Return the number of the local port being monitored for "VNC" activity.
		*/
		MMI_EXPORT unsigned short Port() const;

		/*	Method: GetLastUpdate
			Returns the timestamp of the last time <Refresh> was called. Returns 0 
			if <Refresh> has never been called.
		*/
		MMI_EXPORT time_t GetLastUpdate() const;

	protected:

		bool m_valid;
		TCPConnection m_vncConn;
		time_t m_lastUpdate;
		unsigned short m_port;
	
	};
	
}