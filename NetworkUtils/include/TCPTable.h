#pragma once


#include <winsock2.h>
#include <ws2tcpip.h>
#include <iphlpapi.h>
#include <stdio.h>
#include <map>
#include <vector>
#include <Address.h>

#pragma comment(lib, "iphlpapi.lib")
#pragma comment(lib, "ws2_32.lib")

#define MALLOC(x) HeapAlloc(GetProcessHeap(), 0, (x))
#define FREE(x) HeapFree(GetProcessHeap(), 0, (x))

namespace networkutils{

	class TCPConnection{
	public:
		TCPConnection();
		std::string ToString() const;

		Address localAddress;
		Address remoteAddress;
		int state;
		std::string stateString;

		
	};

	

	class TCPTable{
	public:
		class TCPError{
		public:
			static TCPError& Instance();
			static bool Valid(DWORD errCode);
			static std::string ToString( DWORD errCode );

		private:
			TCPError();

			std::map<DWORD,std::string> m_errorTable;
		};

	
		TCPTable();

		int Refresh();

		void Clear();

		int Count() const;

		std::vector<TCPConnection>::const_iterator begin() const;
		std::vector<TCPConnection>::const_iterator end() const;

		std::string ToString() const;

	protected:
		
		std::vector<TCPConnection> m_connections;

	};


}
