//
// async_tcp_client.cpp
// ~~~~~~~~~~~~~~~~~~~~
//
// Copyright (c) 2003-2010 Christopher M. Kohlhoff (chris at kohlhoff dot com)
//
// Distributed under the Boost Software License, Version 1.0. (See accompanying
// file LICENSE_1_0.txt or copy at http://www.boost.org/LICENSE_1_0.txt)
//
// Modified by Sammie
#pragma once

#include "Address.h"
#include "StringUtils.h"

#include <boost/asio/deadline_timer.hpp>
#include <boost/asio/io_service.hpp>
#include <boost/asio/ip/tcp.hpp>
#include <boost/asio/read_until.hpp>
#include <boost/asio/streambuf.hpp>
#include <boost/asio/write.hpp>
#include <boost/bind.hpp>
#include <boost/signals2/signal.hpp>
#include <iostream>
#include <atomic>

using boost::asio::deadline_timer;
using boost::asio::ip::tcp;

/*
	The TCPSocketWrapper is a wrapper class around the  boost asio tcp socket.
	It has a signal which should be connected to a function that handles a TCPSocketWrapperPacket object.
	If no signal is connected it std::cout received stuff.
*/
namespace networkutils
{
	enum PacketDescription
	{
		CONNECT_SUCCESS = 0,
		CONNECT_FAIL,
		READ_SUCCESS,
		READ_FAIL,
		WRITE_SUCCESS,
		WRITE_FAIL,
		PACKET_OTHER
	};

	struct TCPSocketWrapperPacket
	{
		MMI_EXPORT TCPSocketWrapperPacket(PacketDescription pd, const std::string& pdata);
		MMI_EXPORT std::string getPacketTypeAsString() const;
		PacketDescription packettype;
		std::string packetdata;

	};

	typedef boost::signals2::signal<void(TCPSocketWrapperPacket&)> PacketSignal;

	class TCPSocketWrapper
	{
	public:
		MMI_EXPORT TCPSocketWrapper(boost::asio::io_service& io_service, Address ipaddress = Address());

		// Called by the user of the client class to initiate the connection process.
		// The endpoint iterator will have been obtained using a tcp::resolver.
		MMI_EXPORT void start(tcp::resolver::iterator endpoint_iter);
		MMI_EXPORT void start();

		// This function terminates all the actors to shut down the connection. It
		// may be called by the user of the client class, or by the class itself in
		// response to graceful termination or an unrecoverable error.
		MMI_EXPORT bool isStopped();
		MMI_EXPORT void stop();
		MMI_EXPORT void SetPortnum(unsigned int portnum);
		MMI_EXPORT boost::signals2::connection RegisterForPacketSignal(const PacketSignal::slot_type& p_slot);
	private:
		void start_connect(tcp::resolver::iterator endpoint_iter);
		void handle_connect(const boost::system::error_code& ec, tcp::resolver::iterator endpoint_iter);
		void start_read();
		void handle_read(const boost::system::error_code& ec);
		void start_write();
		void handle_write(const boost::system::error_code& ec);
		void check_deadline();
		bool stopped_;
		Address ipaddress_;
		tcp::socket socket_;
		tcp::resolver resolver_;
		boost::asio::streambuf input_buffer_;
		deadline_timer deadline_;
		deadline_timer heartbeat_timer_;
		PacketSignal m_PacketSignal;
		char delimiter;
	};
}
