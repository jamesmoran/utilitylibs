#pragma once
#include <sstream>
#include <assert.h>
#include "CoreDefines.h"

namespace networkutils{

	/*	Class: MMStream
		C++ version of the UtilityLibsCS MMStream used by MetricsManager for receiving/sending messages
		Last updated: 2014-07-22
	*/
	class MMStream
	{
	public:
		/*	Method: MMStream
			Constructor, if header & body are ok, builds an MMStream.
		*/
		MMI_EXPORT MMStream(const std::string &header, const std::string &body);

		/*	Method: MMStream
			Constructor, if header, body, extension, & data are all ok, builds an MMStream.
			dataSize is limited to 512mb (and should probably not even be anywhere near this large!)
		*/
		MMI_EXPORT MMStream(const std::string &header, const std::string &body, const std::string &extension, const unsigned char* data, __int32 dataSize);
		MMI_EXPORT ~MMStream();
		
		enum StreamState {OK, BAD_HEADER, BAD_BODY, BAD_DATA, BAD_DATASIZE};
		StreamState getStatus() { return status; };
		unsigned char* getPacket() { return packetStart; };
		__int32 getPacketSize() { return packetSize; };
		
	private:
		void buildPacket(const std::string &header, const std::string &body, const std::string &extension, const unsigned char* data, __int32 dataSize);
		void addBytes(const unsigned char* data, __int32 dataSize);
		void addString(const std::string &str);
		void packetInsert(const void *source, __int32 sourceSize);
		StreamState status;
		unsigned char* packetStart;
		__int32 position;
		__int32 packetSize;
	};
}