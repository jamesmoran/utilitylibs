#ifndef UTILITYLIBS_ADDRESS_H_
#define UTILITYLIBS_ADDRESS_H_

#include <string>
#include "CoreDefines.h"

namespace networkutils{

class Address
{
public:
	MMI_EXPORT Address();
	MMI_EXPORT Address(unsigned int address, unsigned short port);
	MMI_EXPORT Address(unsigned short a, unsigned short b, unsigned short c, unsigned short d, unsigned short port);

	MMI_EXPORT std::string ToString() const;

	MMI_EXPORT void ChangePort(unsigned int p_portnum);
	MMI_EXPORT unsigned short Port() const;
	MMI_EXPORT unsigned int IPAddress() const;

	MMI_EXPORT bool operator==(const Address& other) const;
	MMI_EXPORT bool operator!=(const Address& other) const;
	MMI_EXPORT bool operator<(const Address& other) const;
	MMI_EXPORT bool operator>(const Address& other) const;

	MMI_EXPORT static unsigned int StringToIPAddr(const std::string& str);
	MMI_EXPORT static networkutils::Address IpandPortStr_toAddress(const std::string& str);
private:
	unsigned int m_Address;
	unsigned short m_Port;
};

}

#endif