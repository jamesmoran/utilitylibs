#ifndef UTILITYLIBS_UDPSOCKET_H_
#define UTILITYLIBS_UDPSOCKET_H_


#include <winsock2.h>
#include "Address.h"

namespace networkutils{

bool InitializeSockets();

void UninitializeSockets();

// A simple wrapper around a basic UDP socket.
class UDPSocket
{
public:
	MMI_EXPORT UDPSocket();
	MMI_EXPORT ~UDPSocket();

	MMI_EXPORT bool Bind(unsigned short port);
	MMI_EXPORT bool IsBound() const;
	MMI_EXPORT void Close();
	MMI_EXPORT int Send(const Address& address, const void* data, size_t length);
	MMI_EXPORT int Receive(Address& sender, void* buffer, size_t length);

private:
	unsigned short m_Port;
	SOCKET m_SocketHandle;
};

}

#endif