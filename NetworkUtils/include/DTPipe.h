#pragma once

#include <boost/asio.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/json_parser.hpp>
#include <sstream>
#include <boost/lambda/bind.hpp>
#include <boost/lambda/lambda.hpp>
#include "HardwareID.h"
#include "MMStream.h"

namespace networkutils{
	using boost::asio::ip::tcp;
	using boost::asio::deadline_timer;
	using boost::lambda::bind;
	using boost::lambda::var;
	using boost::lambda::_1;

	/*	Class: DTPacket
		The Data Transmission Packet helps in preparing a Inter-Process Message to the Data Transmitter Windows Service.
		Note that it is not meant to be reused
	*/
	class DTPacket
	{
		friend class DTPipe;
	public:
		MMI_EXPORT DTPacket(const std::string &header, const std::string &body);
		MMI_EXPORT DTPacket(const std::string &header, const std::string &body, const std::string &filePath);
		MMI_EXPORT DTPacket(const std::string &header, const std::string &body, const std::string &extension, const unsigned char* data, __int32 dataSize);
		MMI_EXPORT ~DTPacket(void);

		MMStream::StreamState getStatus() { return stream->getStatus(); };
		const unsigned char* getPacket() { return stream->getPacket(); };
		__int32 getPacketSize() { return stream->getPacketSize(); };

	private:
		MMStream *stream;
	};

	/* Class: DTPipe
		A TCP 'pipe' used by an application to communicate with the Data Transmitter's internal TCP server.
	*/
	class DTPipe
	{
	public:
		MMI_EXPORT DTPipe();
		MMI_EXPORT ~DTPipe(void);

		MMI_EXPORT void start(unsigned short portNumber);
		MMI_EXPORT void stop();
		MMI_EXPORT bool sendPacket(DTPacket *packet, boost::posix_time::time_duration timeout);

	private:
		void connect();
		static void threadEntry(void *lpVoid);
		void threadLoop();
		void enterConnectState();
		bool write(const unsigned char* data, __int32 dataLength, boost::posix_time::time_duration timeout);
		std::string FormatTimestamp(time_t timestamp, bool iso);
		void checkDeadline();

		boost::asio::io_service _io_service;
		tcp::socket _socket;
		boost::asio::deadline_timer _deadline;
		HANDLE clientThread;
		HANDLE hEvent;
		bool isConnecting;
		bool runningFlag;
		bool isStarted;
		unsigned short portNumber;
		static const int tcpPrefixSize = 4;
	};
}
