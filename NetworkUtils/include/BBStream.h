#pragma once

#include <winsock2.h>
#include <Iphlpapi.h>
#include <stdint.h>
#include "HardwareID.h"
#include <iostream>
#include <sstream>
#include "CoreDefines.h"

namespace networkutils{
	struct BBCpyStruct{
		unsigned char* packetStart;
		__int32 position;
		__int32 packetSize;
	};

	class BBStream
	{
	public:
		MMI_EXPORT BBStream(void);
		MMI_EXPORT ~BBStream(void);

		MMI_EXPORT static unsigned char* encodeData(std::string fileName, __int32 dataSize, unsigned char* data, std::string subsystemID, int &packageSize);
	private:
		static std::string GetMACAddress();
		static std::string GetIPAddress();
		static void addBytes(const unsigned char* data, __int32 dataSize, BBCpyStruct &cpyStruct);
		static void addString(const std::string &str, BBCpyStruct &cpyStruct);
		static void BBStream::packetInsert(const void *source, __int32 sourceSize, BBCpyStruct &cpyStruct);
	};
};
