#include "TCPTable.h"

#include <sstream>

namespace networkutils{


	TCPConnection::TCPConnection()
		: state(0)
	{

	}


	std::string TCPConnection::ToString() const
	{
		std::stringstream ss;
		ss << "LOCAL[" << localAddress.ToString() << "] ";
		ss << "REMOTE[" << remoteAddress.ToString() << "] STATE[";
		ss << state << "] " << stateString;
		return ss.str();
	}

	TCPTable::TCPTable()
	{

	}

	int TCPTable::Refresh()
	{
		Clear();

		// Declare and initialize variables
		PMIB_TCPTABLE pTcpTable;
		DWORD dwSize = 0;
		DWORD dwRetVal = 0;

		char szLocalAddr[128];
		char szRemoteAddr[128];

		struct in_addr IpAddr;

		int i;

		pTcpTable = (MIB_TCPTABLE *) MALLOC(sizeof (MIB_TCPTABLE));
		if (pTcpTable == NULL) {
			printf("Error allocating memory\n");
			return 1;
		}

		dwSize = sizeof (MIB_TCPTABLE);
		// Make an initial call to GetTcpTable to
		// get the necessary size into the dwSize variable
		if ((dwRetVal = GetTcpTable(pTcpTable, &dwSize, TRUE)) == ERROR_INSUFFICIENT_BUFFER) {
				FREE(pTcpTable);
				pTcpTable = (MIB_TCPTABLE *) MALLOC(dwSize);
				if (pTcpTable == NULL) {
					printf("Error allocating memory\n");
					return 1;
				}
		}

		// Make a second call to GetTcpTable to get
		// the actual data we require
		if ((dwRetVal = GetTcpTable(pTcpTable, &dwSize, TRUE)) == NO_ERROR) {
			
			int numEntries = (int) pTcpTable->dwNumEntries;	
			m_connections.resize(numEntries);
			
			for (i = 0; i < (int) pTcpTable->dwNumEntries; i++) {
				IpAddr.S_un.S_addr = (u_long) pTcpTable->table[i].dwLocalAddr;
				strcpy_s(szLocalAddr, sizeof (szLocalAddr), inet_ntoa(IpAddr));
				IpAddr.S_un.S_addr = (u_long) pTcpTable->table[i].dwRemoteAddr;
				strcpy_s(szRemoteAddr, sizeof (szRemoteAddr), inet_ntoa(IpAddr));

				m_connections[i].localAddress = Address(Address::StringToIPAddr(std::string(szLocalAddr)),ntohs((u_short)pTcpTable->table[i].dwLocalPort));
				m_connections[i].remoteAddress = Address(Address::StringToIPAddr(std::string(szRemoteAddr)),ntohs((u_short)pTcpTable->table[i].dwRemotePort));
				m_connections[i].state = pTcpTable->table[i].dwState;
				m_connections[i].stateString = TCPError::ToString(m_connections[i].state);
			}
		} else {
			printf("\tGetTcpTable failed with %d\n", dwRetVal);
			FREE(pTcpTable);
			return 1;
		}

		if (pTcpTable != NULL) {
			FREE(pTcpTable);
			pTcpTable = NULL;
		}  

		return Count();
	}

	void TCPTable::Clear()
	{
		m_connections.clear();
	}

	std::string TCPTable::ToString() const
	{
		std::stringstream ss;
		auto iter = m_connections.begin();
		for(; iter != m_connections.end(); iter++){
			ss << iter->ToString() << std::endl;
		}
		return ss.str();
	}

	std::vector<TCPConnection>::const_iterator TCPTable::begin() const
	{
		return m_connections.begin();
	}

	std::vector<TCPConnection>::const_iterator TCPTable::end() const
	{
		return m_connections.end();
	}

	int TCPTable::Count() const
	{
		return (int) m_connections.size();
	}


	TCPTable::TCPError& TCPTable::TCPError::Instance()
	{
		static TCPError theInstance;
		return theInstance;
	}

	bool TCPTable::TCPError::Valid( DWORD errCode )
	{
		auto iter = Instance().m_errorTable.find(errCode);
		return Instance().m_errorTable.end() != iter;
	}

	std::string TCPTable::TCPError::ToString( DWORD errCode )
	{
		auto iter = Instance().m_errorTable.find(errCode);
		if(Instance().m_errorTable.end() != iter){
			return iter->second;
		}
		return "UNKNOWN";
	}

	TCPTable::TCPError::TCPError()
	{
		m_errorTable[MIB_TCP_STATE_CLOSED] = "CLOSED";
		m_errorTable[MIB_TCP_STATE_LISTEN] = "LISTEN";
		m_errorTable[MIB_TCP_STATE_SYN_SENT] = "SYN-SENT";
		m_errorTable[MIB_TCP_STATE_SYN_RCVD] = "SYN-RECEIVED";
		m_errorTable[MIB_TCP_STATE_ESTAB] = "ESTABLISHED";
		m_errorTable[MIB_TCP_STATE_FIN_WAIT1] = "FIN-WAIT-1";
		m_errorTable[MIB_TCP_STATE_FIN_WAIT2] = "FIN-WAIT-2 ";
		m_errorTable[MIB_TCP_STATE_CLOSE_WAIT] = "CLOSE-WAIT";
		m_errorTable[MIB_TCP_STATE_CLOSING] = "CLOSING";
		m_errorTable[MIB_TCP_STATE_LAST_ACK] = "LAST-ACK";
		m_errorTable[MIB_TCP_STATE_TIME_WAIT] = "TIME-WAIT";
		m_errorTable[MIB_TCP_STATE_DELETE_TCB] = "DELETE-TCB";
	}

}