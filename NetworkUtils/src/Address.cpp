#include "Address.h"
#include <sstream>
#include <StringUtils.h>

namespace networkutils{

Address::Address()
	: m_Address(0)
	, m_Port(0)
{
}

Address::Address( unsigned int address, unsigned short port )
	: m_Address(address)
	, m_Port(port)
{

}

Address::Address( unsigned short a, unsigned short b, unsigned short c, unsigned short d, unsigned short port )
	: m_Port(port)
{
	m_Address = (a << 24) | (b << 16) | (c << 8) | d;
}

std::string Address::ToString() const
{
	std::stringstream ss;
	ss << (m_Address >> 24) << "." << ((m_Address >> 16) & 0xff) << "." << ((m_Address >> 8) & 0xff) << "." << (m_Address & 0xff) << ":" << m_Port;
	return ss.str();
}

void Address::ChangePort(unsigned int p_portnum)
{
	m_Port = p_portnum;
}

unsigned int Address::StringToIPAddr(const std::string& str){
	std::vector<std::string> tokens;
	stringutils::Split(str,'.',tokens);
	if(tokens.size() < 4){
		return 0;
	}

	unsigned short values[4];
	for(int i = 0; i < 4; i++){
		values[i] = stringutils::StringManip::lexical_cast<unsigned short>(tokens[i]);
	}

	unsigned int addr = (values[0] << 24) | (values[1] << 16) | (values[2] << 8) | values[3];

	return addr;
}

networkutils::Address Address::IpandPortStr_toAddress(const std::string& str)
{
	// extract port from ipaddress
	std::vector<std::string> addressandport;
	stringutils::Split(str, ':', addressandport);

	unsigned int ipaddr, port = 0;
	if (addressandport.size() > 0)
		ipaddr = networkutils::Address::StringToIPAddr(addressandport[0]);
	if (addressandport.size() > 1)
		port = static_cast<unsigned int>(atoi(addressandport[1].c_str()));

	return networkutils::Address(ipaddr, port);
}

unsigned short Address::Port() const
{
	return m_Port;
}

unsigned int Address::IPAddress() const
{
	return m_Address;
}

bool Address::operator==( const Address& other ) const
{
	if (this->m_Port == other.m_Port &&
		this->m_Address == other.m_Address)
		return true;
	return false;
}

bool Address::operator!=(const Address& other) const
{
	return !(*this == other);
}

bool Address::operator<(const Address& other) const
{
	if (m_Address == other.m_Address)
		return m_Port < other.m_Port;
	else
		return m_Address < other.m_Port;
}

bool Address::operator>(const Address& other) const
{
	if (m_Address == other.m_Address)
		return m_Port > other.m_Port;
	else
		return m_Address > other.m_Port;
}

}