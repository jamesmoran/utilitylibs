#include "BBStream.h"

namespace networkutils{
	BBStream::BBStream(void)
	{
		
	}

	BBStream::~BBStream(void)
	{
	}

	unsigned char* BBStream::encodeData(std::string fileName, __int32 dataSize, unsigned char* data, std::string subsystemID, int &packageSize)
	{
		std::string macAddress = BBStream::GetMACAddress();

		BBCpyStruct cpyStruct;
		cpyStruct.packetSize = static_cast<int>(macAddress.length() + subsystemID.length() + fileName.length() + dataSize + 16);
		cpyStruct.packetStart = new unsigned char[cpyStruct.packetSize];
		cpyStruct.position = 0;
		addString(macAddress, cpyStruct);
		addString(subsystemID, cpyStruct);
		addString(fileName, cpyStruct);
		addBytes(data, dataSize, cpyStruct);
		
		packageSize = cpyStruct.packetSize;
		return cpyStruct.packetStart;
	}

	std::string BBStream::GetMACAddress()
	{
		std::string macAddr;
		DWORD ret;
		struct in_addr srcip;
		ULONG MacAddr[2];
		ULONG PhyAddrLen = 6;  /* default to length of six bytes */

		srcip.s_addr = inet_addr(GetIPAddress().c_str());
		//Send an arp packet
		ret = SendARP(srcip.S_un.S_addr , srcip.S_un.S_addr , MacAddr , &PhyAddrLen);

		//Prepare the mac address
		if(PhyAddrLen)
		{
			BYTE *bMacAddr = (BYTE*) &MacAddr;
			char macFormatted[32];
			sprintf_s(macFormatted,"%.2X-%.2X-%.2X-%.2X-%.2X-%.2X",bMacAddr[0],bMacAddr[1],bMacAddr[2],bMacAddr[3],bMacAddr[4],bMacAddr[5]);
			macAddr = std::string(macFormatted);
		}
		return macAddr;
	}

	std::string BBStream::GetIPAddress()
	{
		std::string ip_addr;

		WORD requested_version;
		WSADATA wsa_data;
		requested_version = MAKEWORD( 1, 0 );
		if(WSAStartup(requested_version, &wsa_data) == 0)
		{
			char name[255];
			if(gethostname(name, sizeof(name)-1) == 0)
			{
				hostent *hostinfo;
				if(hostinfo = gethostbyname(name))
					ip_addr = std::string(inet_ntoa (*(struct in_addr*)(*hostinfo->h_addr_list)));
			}
			WSACleanup( );
		}

		return ip_addr;
	}

	void BBStream::addBytes(const unsigned char* data, __int32 dataSize, BBCpyStruct &cpyStruct)
	{
		packetInsert(&dataSize, sizeof(dataSize), cpyStruct);
		packetInsert(data, dataSize, cpyStruct);
	}

	void BBStream::addString(const std::string &str, BBCpyStruct &cpyStruct)
	{
		__int32 strSize = static_cast<__int32>(str.length());
		packetInsert(&strSize, sizeof(strSize), cpyStruct);
		packetInsert(str.c_str(), strSize, cpyStruct);
	}

	void BBStream::packetInsert(const void *source, __int32 sourceSize, BBCpyStruct &cpyStruct) {
		errno_t err = memcpy_s(cpyStruct.packetStart + cpyStruct.position, cpyStruct.packetSize - cpyStruct.position, source, sourceSize);
		if(!err)
			cpyStruct.position += sourceSize;
	}
}