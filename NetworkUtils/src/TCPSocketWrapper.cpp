#include "TCPSocketWrapper.h"


namespace networkutils{

	TCPSocketWrapperPacket::TCPSocketWrapperPacket(PacketDescription pd, const std::string& pdata)
		: packettype(pd)
		, packetdata(pdata)
	{

	}

	std::string TCPSocketWrapperPacket::getPacketTypeAsString() const
	{
		switch(packettype)
		{
			case CONNECT_SUCCESS:
				return "CONNECT_SUCCESS";
			case CONNECT_FAIL:
				return "CONNECT_FAIL";
			case READ_SUCCESS:
				return "READ_SUCCESS";
			case READ_FAIL:
				return "READ_FAIL";
			case WRITE_SUCCESS:
				return "WRITE_SUCCESS";
			case WRITE_FAIL:
				return "WRITE_FAIL";
			case PACKET_OTHER:
				return "PACKET_OTHER";
			default:
				return "UNKNOWN_PACKET_TYPE";
		}
	}

	TCPSocketWrapper::TCPSocketWrapper(boost::asio::io_service& io_service, Address ipaddress)
		: stopped_(false)
		, socket_(io_service)
		, deadline_(io_service)
		, heartbeat_timer_(io_service)
		, resolver_(io_service)
		, delimiter('\n')
		, ipaddress_(ipaddress)
	{

	}

	void TCPSocketWrapper::start(tcp::resolver::iterator endpoint_iter)
	{
		// Start the connect actor.
		start_connect(endpoint_iter);

		// Start the deadline actor. You will note that we're not setting any
		// particular deadline here. Instead, the connect and input actors will
		// update the deadline prior to each asynchronous operation.
		deadline_.async_wait(boost::bind(&TCPSocketWrapper::check_deadline, this));
	}

	void TCPSocketWrapper::start()
	{
		std::string ipaddrstr = ipaddress_.ToString();
		std::vector<std::string> addrport;
		stringutils::Split(ipaddrstr, ':', addrport);
		start(resolver_.resolve(tcp::resolver::query(addrport[0], addrport[1])));
	}

	void TCPSocketWrapper::stop()
	{
		stopped_ = true;
		boost::system::error_code error;

		try{
			socket_.shutdown(boost::asio::socket_base::shutdown_both);
			if (error){
				// An error occurred
				return;
			}
		}
		catch (...) {}

		try {
			socket_.close(error);
			if (error){
				// An error occurred
				return;
			}
		}
		catch (...) {}

		try {
			deadline_.cancel(error);
			if (error){
				// An error occurred
				return;
			}
		}
		catch (...) {}

		try {
			heartbeat_timer_.cancel(error);
			if (error){
				// An error occurred
				return;
			}
		}
		catch (...) {}
	}

	bool TCPSocketWrapper::isStopped()
	{
		return stopped_;
	}

	void TCPSocketWrapper::SetPortnum(unsigned int portnum)
	{
		ipaddress_.ChangePort(portnum);
	}

	boost::signals2::connection TCPSocketWrapper::RegisterForPacketSignal(const PacketSignal::slot_type& p_slot)
	{
		return m_PacketSignal.connect(p_slot);
	}

	void TCPSocketWrapper::start_connect(tcp::resolver::iterator endpoint_iter)
	{
		if (endpoint_iter != tcp::resolver::iterator())
		{
			// Set a deadline for the connect operation.
			deadline_.expires_from_now(boost::posix_time::seconds(2));

			// Start the asynchronous connect operation.
			socket_.async_connect(endpoint_iter->endpoint(),
				boost::bind(&TCPSocketWrapper::handle_connect,
				this, _1, endpoint_iter));
		}
		else
		{
			// There are no more endpoints to try. Shut down the client.
			stop();
		}
	}

	void TCPSocketWrapper::handle_connect(const boost::system::error_code& ec, tcp::resolver::iterator endpoint_iter)
	{
		// The async_connect() function automatically opens the socket at the start
		// of the asynchronous operation. If the socket is closed at this time then
		// the timeout handler must have run first.
		if (!socket_.is_open())
		{
			std::stringstream ss;
			ss << "Connect timed out";
			m_PacketSignal(TCPSocketWrapperPacket(CONNECT_FAIL, ss.str()));
			// Try the next available endpoint.
			start_connect(++endpoint_iter);
		}

		// Check if the connect operation failed before the deadline expired.
		else if (ec)
		{
			std::stringstream ss;
			ss << "Connect timed out";
			m_PacketSignal(TCPSocketWrapperPacket(CONNECT_FAIL, ss.str()));
			// We need to close the socket used in the previous connection attempt
			// before starting a new one.
			try {
				socket_.shutdown(boost::asio::socket_base::shutdown_both);
				socket_.close();
			}
			catch (...)
			{

			}

			// Try the next available endpoint.
			start_connect(++endpoint_iter);
		}

		// Otherwise we have successfully established a connection.
		else
		{
			stopped_ = false;
			std::stringstream ss;
			ss << "Connected to " << endpoint_iter->endpoint();
			m_PacketSignal(TCPSocketWrapperPacket(CONNECT_SUCCESS, ss.str()));

			// Start the input actor.
			start_read();

			// Start the heartbeat actor.
			//start_write();
		}
	}

	void TCPSocketWrapper::start_read()
	{
		// Set a deadline for the read operation.
		deadline_.expires_from_now(boost::posix_time::seconds(5));

		// Start an asynchronous operation to read a newline-delimited message.
		boost::asio::async_read_until(socket_, input_buffer_, delimiter, boost::bind(&TCPSocketWrapper::handle_read, this, _1));
	}

	void TCPSocketWrapper::handle_read(const boost::system::error_code& ec)
	{
		if (stopped_)
			return;

		if (!ec)
		{
			// Extract the delimited message from the buffer.
			std::string line;
			std::istream is(&input_buffer_);
			std::getline(is, line);

			// Empty messages are heartbeats and so ignored.
			if (!line.empty())
			{
				std::stringstream ss;
				ss << line;
				m_PacketSignal(TCPSocketWrapperPacket(READ_SUCCESS, ss.str()));
			}

			start_read();
		}
		else
		{
			std::stringstream ss;
			ss << "Error on receive: " << ec.message();
			m_PacketSignal(TCPSocketWrapperPacket(READ_FAIL, ss.str()));
			stop();
		}
	}

	void TCPSocketWrapper::start_write()
	{
		if (stopped_)
		{
			return;
		}

		// Start an asynchronous operation to send a heartbeat message.
		boost::asio::async_write(socket_, boost::asio::buffer("\n", 1), boost::bind(&TCPSocketWrapper::handle_write, this, _1));
	}

	void TCPSocketWrapper::handle_write(const boost::system::error_code& ec)
	{
		if (stopped_)
		{
			return;
		}

		if (!ec)
		{
			// Wait 10 seconds before sending the next heartbeat.
			heartbeat_timer_.expires_from_now(boost::posix_time::seconds(3));
			heartbeat_timer_.async_wait(boost::bind(&TCPSocketWrapper::start_write, this));
		}
		else
		{
			std::stringstream ss;
			ss << "Error on heartbeat: " << ec.message();
			m_PacketSignal(TCPSocketWrapperPacket(WRITE_FAIL, ss.str()));
			stop();
		}
	}

	void TCPSocketWrapper::check_deadline()
	{
		if (stopped_)
		{
			return;
		}

		// Check whether the deadline has passed. We compare the deadline against
		// the current time since a new asynchronous operation may have moved the
		// deadline before this actor had a chance to run.
		if (deadline_.expires_at() <= deadline_timer::traits_type::now())
		{
			// The deadline has passed. The socket is closed so that any outstanding
			// asynchronous operations are cancelled.
			try {
				socket_.shutdown(boost::asio::socket_base::shutdown_both);
				socket_.close();
			}
			catch (...)
			{

			}

			// There is no longer an active deadline. The expiry is set to positive
			// infinity so that the actor takes no action until a new deadline is set.
			deadline_.expires_at(boost::posix_time::pos_infin);
		}

		// Put the actor back to sleep.
		deadline_.async_wait(boost::bind(&TCPSocketWrapper::check_deadline, this));
	}
}
