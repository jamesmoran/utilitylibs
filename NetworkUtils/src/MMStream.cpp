#include "MMStream.h"

namespace networkutils{
	MMStream::MMStream(const std::string &header, const std::string &body)
		: packetStart(NULL)
		, position(0)
		, packetSize(0)
	{
		if(header.empty()){
			this->status = BAD_HEADER;
		} else {
			buildPacket(header, body, "", NULL, 0);
		}
	}

	MMStream::MMStream(const std::string &header, const std::string &body, const std::string &extension, const unsigned char* data, __int32 dataSize)
		: packetStart(NULL)
		, position(0)
		, packetSize(0)
	{
		assert(dataSize <= 536870912);

		if(header.empty()){
			this->status = BAD_HEADER;
		} else if(extension.empty() ^ (data == NULL)) {
			this->status = BAD_DATA;
		} else if(data != NULL && dataSize <= 0) {
			this->status = BAD_DATASIZE;
		} else {
			buildPacket(header, body, extension, data, dataSize);
		}
	}

	MMStream::~MMStream()
	{
		if(packetStart != NULL){
			delete[] packetStart;
		}
	}

	void MMStream::buildPacket(const std::string &header, const std::string &body, const std::string &extension, const unsigned char* data, __int32 dataSize)
	{
		position = 0;
		packetSize = static_cast<int>(header.length() + body.length() + 2 * sizeof(__int32));
		if(data != NULL) {
			packetSize += static_cast<int>((dataSize + extension.length() + 2 * sizeof(__int32)));
		}
		packetStart = new unsigned char[packetSize];
		addString(header);
		addString(body);
		if(dataSize > 0){
			addString(extension);
			addBytes(data, dataSize);
		}
		this->status = OK;
	}

	void MMStream::addBytes(const unsigned char* data, __int32 dataSize)
	{
		packetInsert(&dataSize, sizeof(dataSize));
		packetInsert(data, dataSize);
	}

	void MMStream::addString(const std::string &str)
	{
		__int32 strSize = static_cast<int>(str.length());
		packetInsert(&strSize, sizeof(strSize));
		packetInsert(str.c_str(), strSize);
	}

	void MMStream::packetInsert(const void *source, __int32 sourceSize) {
		errno_t err = memcpy_s(packetStart + position, packetSize - position, source, sourceSize);
		if(!err)
			position += sourceSize;
	}
}