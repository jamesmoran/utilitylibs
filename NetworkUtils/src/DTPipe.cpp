#include "DTPipe.h"

namespace networkutils{
	DTPipe::DTPipe()
		:isConnecting(false),
		_socket(_io_service),
		_deadline(_io_service),
		isStarted(false),
		clientThread(NULL),
		hEvent(NULL),
		runningFlag(true)
	{
		_deadline.expires_at(boost::posix_time::pos_infin);
		checkDeadline();
	}

	DTPipe::~DTPipe(void)
	{
		stop();
	}

	// Starts the connection with a given portNumber.
	void DTPipe::start(unsigned short portNumber)
	{
		if(!isStarted) {
			this->portNumber = portNumber;
			isStarted = true;
			runningFlag = true;
			hEvent = CreateEvent( NULL, false, false, "DTPipeEvent");
			clientThread = (HANDLE) _beginthread(threadEntry, 0, this);
		}
	}

	// Static entrance function for threadLoop
	void DTPipe::threadEntry(void *lpVoid)
	{
		DTPipe* pipe = (DTPipe*) lpVoid;
		pipe->threadLoop();
	}

	// Separate thread that attempts to connect to the TCP server and sleeps while connected
	void DTPipe::threadLoop()
	{
		while(runningFlag){
			connect();
			isConnecting = false;
			WaitForSingleObject(hEvent, INFINITE);
		}
	}

	// Stops the connect thread and closes the socket
	void DTPipe::stop()
	{
		if(isStarted) {
			if(clientThread != NULL && clientThread != INVALID_HANDLE_VALUE) {
				runningFlag = false;
				SetEvent(hEvent);
				WaitForSingleObject(clientThread, INFINITE);
				clientThread = NULL;
			}

			if(_socket.is_open()){
				boost::system::error_code error;
				_socket.shutdown(tcp::socket::socket_base::shutdown_both, error);
				_socket.close();
			}
			isStarted = false;
		}
	}

	// Blocking method that connects the socket to a local TCP Server. Runs on a separate thread
	void DTPipe::connect() {
		_socket.close();
		boost::asio::ip::tcp::endpoint endpoint(boost::asio::ip::address::from_string("127.0.0.1"), portNumber);
		boost::system::error_code error = boost::asio::error::host_not_found;
		while(runningFlag && error){
			_socket.connect(endpoint, error);
			Sleep(5);
		}
	}

	// Writes a length prefix and data to the socket. Automatically enters a connecting state if the write fails or times out.
	bool DTPipe::write(const unsigned char* data, __int32 dataLength, boost::posix_time::time_duration timeout)
	{
		bool result = false;
		if(_socket.is_open() && !isConnecting){
			_deadline.expires_from_now(timeout);

			//Write length prefix so the tcp server knows how much data to read from the socket
			boost::system::error_code error = boost::asio::error::would_block;
			boost::asio::async_write(_socket, boost::asio::buffer(&dataLength, sizeof(__int32)), var(error) = _1);

			do {
				_io_service.run_one();
			} while (error == boost::asio::error::would_block);

			// Length prefix sent, now send actual data
			if(!error) {
				_deadline.expires_from_now(timeout);
				boost::system::error_code error2 = boost::asio::error::would_block;
				boost::asio::async_write(_socket, boost::asio::buffer(data, dataLength), var(error2) = _1);

				do {
					_io_service.run_one();
				} while (error2 == boost::asio::error::would_block);

				if(!error2) {
					result = true; // Complete message was sent successfully
				}
			}
		} 
		
		// If the socket failed, then we need to reconnect
		if(!result) {
			enterConnectState();
		}
		return result;
	}

	// If the socket is not already connecting, then put it into the connecting state
	void DTPipe::enterConnectState()
	{
		if(!isConnecting) {	
			isConnecting = true;
			PulseEvent(hEvent);
		}
	}

	// Send a DTPacket object over the TCP connection with a given timeout. Returns false if the socket is disconnected.
	bool DTPipe::sendPacket(DTPacket *packet, boost::posix_time::time_duration timeout)
	{
		if(packet->getStatus() != MMStream::OK)
			return false;

		__int32 packetSize = packet->getPacketSize();
		const unsigned char* data = packet->getPacket();
		return write(data, packetSize, timeout);
	}

	// Converts c++ time_t to c# datetime style (ISO 8601)
	std::string DTPipe::FormatTimestamp(time_t timestamp, bool iso) {
		struct tm timeStruct;
		localtime_s(&timeStruct,&timestamp);

		std::stringstream ss;
		ss << std::setw(4) << std::setfill('0');
		ss << timeStruct.tm_year + 1900 << '-';
		ss << std::setw(2) << timeStruct.tm_mon + 1 << '-';
		ss << std::setw(2) << timeStruct.tm_mday;
		ss << (iso ? 'T' : ' ');
		ss << std::setw(2) << timeStruct.tm_hour << ':';
		ss << std::setw(2) << timeStruct.tm_min << ':';
		ss << std::setw(2) << timeStruct.tm_sec;

		return ss.str();
	}

	// Watches the timeout and forcibly closes the socket if an action (connect/read/write) runs out of time
	// The socket being closed will cause the action to abort with error code 1236
	void DTPipe::checkDeadline()
	{
		if (_deadline.expires_at() <= deadline_timer::traits_type::now())
		{
			boost::system::error_code ignored_ec;
			_socket.close(ignored_ec);
			_deadline.expires_at(boost::posix_time::pos_infin);
		}

		_deadline.async_wait(bind(&DTPipe::checkDeadline, this));
	}

	DTPacket::DTPacket(const std::string &header, const std::string &body)
		:stream(NULL)
	{
		stream = new MMStream(header, body);
	}

	DTPacket::DTPacket(const std::string &header, const std::string &body, const std::string &filePath)
		:stream(NULL)
	{
		__int32 pathLength = (__int32)filePath.length();
		stream = new MMStream(header, body, ".isAFilePath", (const unsigned char*)filePath.c_str(), pathLength);
	}

	// data is not deleted by either DTPacket or the underlying MMStream, it is simply mem-copied into the DTPacket for transmission
	DTPacket::DTPacket(const std::string &header, const std::string &body, const std::string &extension, const unsigned char* data, __int32 dataSize)
	{
		stream = new MMStream(header, body, extension, data, dataSize);
	}

	DTPacket::~DTPacket() {
		if(stream != NULL){
			delete stream;
		}
	}
}