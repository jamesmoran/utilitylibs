#include "UDPSocket.h"

namespace networkutils{

bool InitializeSockets()
{
	WSADATA WsaData;
	return WSAStartup( MAKEWORD(2,2), &WsaData ) == NO_ERROR;
}

void UninitializeSockets()
{
	WSACleanup();
}

UDPSocket::UDPSocket()
	: m_Port(0)
	, m_SocketHandle(INVALID_SOCKET)
{
}


UDPSocket::~UDPSocket()
{
	Close();
}

bool UDPSocket::Bind(unsigned short port)
{
	m_Port = port;
	m_SocketHandle = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (m_SocketHandle == INVALID_SOCKET)
	{
		int errCode = WSAGetLastError();

		printf("Failed to create listener socket! %d\n",errCode);
		return false;
	}

	sockaddr_in addr;
	addr.sin_port = htons(m_Port);
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = INADDR_ANY;

	if (bind(m_SocketHandle, (const sockaddr*)&addr, sizeof(sockaddr_in)) < 0)
	{
		int errCode = WSAGetLastError();

		printf("Failed to bind listener socket! %d\n",errCode);
		closesocket(m_SocketHandle);
		m_SocketHandle = INVALID_SOCKET;
		return false;
	}

	DWORD non_blocking = 1;
	if (ioctlsocket(m_SocketHandle, FIONBIO, &non_blocking) != 0 )
	{
		printf( "failed to set non-blocking socket\n" );
		closesocket(m_SocketHandle);
		m_SocketHandle = INVALID_SOCKET;
		return false;
	}

	return true;
}

bool UDPSocket::IsBound() const
{
	return m_SocketHandle != INVALID_SOCKET;
}

void UDPSocket::Close()
{
	if (IsBound())
		closesocket(m_SocketHandle);
	m_SocketHandle = INVALID_SOCKET;
}

int UDPSocket::Send(const Address& address, const void* data, size_t length)
{
	sockaddr_in net_address;
    net_address.sin_family = AF_INET;
	net_address.sin_addr.s_addr = htonl(address.IPAddress());
    net_address.sin_port = htons(address.Port());

	int sent_bytes = sendto(m_SocketHandle, (const char*)data, (int)length,
                             0, (sockaddr*)&net_address, sizeof(sockaddr_in) );

    if (sent_bytes != (int)length )
	{
        printf("Failed to send packet: return value = %d, err = %d\n", sent_bytes, WSAGetLastError());
	}

	return sent_bytes;
}

int UDPSocket::Receive(Address& sender, void* buffer, size_t length)
{
	sockaddr_in from;
    int from_length = sizeof(sockaddr_in);

    int received_bytes = recvfrom(m_SocketHandle, (char*)buffer, (int)length,
                                0, (sockaddr*)&from, &from_length);
	sender = Address(ntohl(from.sin_addr.s_addr), ntohs(from.sin_port));
	return received_bytes;
}

}