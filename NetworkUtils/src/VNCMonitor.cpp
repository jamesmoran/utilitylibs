#include "VNCMonitor.h"

namespace networkutils{

	const unsigned short VNCMonitor::VNC_PORT = 5900;

	VNCMonitor::VNCMonitor(unsigned short port)
		: m_lastUpdate(0)
		, m_port(port)
		, m_valid(false)
	{

	}

	bool VNCMonitor::Refresh()
	{
		m_valid = false;
		TCPTable tcpTable;
		tcpTable.Refresh();

		auto iter = tcpTable.begin();
		for(;iter != tcpTable.end(); iter++){
			if(m_port == iter->localAddress.Port() && iter->localAddress.IPAddress() > 0){
				m_vncConn = *iter;
				m_valid = true;
				break;
			}
		}

		m_lastUpdate = time(NULL);
		return m_valid;
	}

	bool VNCMonitor::IsActive() const
	{
		return m_valid;
	}



	DWORD VNCMonitor::GetStatus() const
	{
		return m_valid ? m_vncConn.state : MIB_TCP_STATE_CLOSED;
	}

	std::string VNCMonitor::GetStatusString() const
	{
		if(m_valid){
			return m_vncConn.stateString;
		}
		return "";
	}

	networkutils::Address VNCMonitor::GetLocalAddress() const
	{
		if(m_valid){
			return m_vncConn.localAddress;
		}
		return Address();
	}

	networkutils::Address VNCMonitor::GetRemoteAddress() const
	{
		if(m_valid){
			return m_vncConn.remoteAddress;
		}
		return Address();
	}

	time_t VNCMonitor::GetLastUpdate() const
	{
		return m_lastUpdate;
	}

	bool VNCMonitor::IsConnected() const
	{
		return m_valid && MIB_TCP_STATE_ESTAB == m_vncConn.state;
	}

	unsigned short VNCMonitor::Port() const
	{
		return m_port;
	}

}
