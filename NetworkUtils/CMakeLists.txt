set(LIBRARY_NAME NetworkUtils)
set(INCLUDE_FILES
	include/CoreDefines.h
	include/Address.h
	include/DTPipe.h
	include/TCPSocketWrapper.h
	include/UDPSocket.h
	include/BBStream.h
	include/MMStream.h
	include/TCPTable.h
	include/VNCMonitor.h
)
set(SRC_FILES
	src/Address.cpp
	src/DTPipe.cpp
	src/TCPSocketWrapper.cpp
	src/UDPSocket.cpp
	src/BBStream.cpp
	src/MMStream.cpp
	src/TCPTable.cpp
	src/VNCMonitor.cpp
)

find_package(Boost 1.58.0 REQUIRED COMPONENTS
	system
	date_time
	thread
)
# static library because any code changes will cause a re-link anyway (template library)
add_library(${LIBRARY_NAME} SHARED 
	${INCLUDE_FILES}
	${SRC_FILES}
)
target_include_directories(${LIBRARY_NAME} PUBLIC
	include/
	${StringUtils_INCLUDE_DIRS}
	${AppUtils_INCLUDE_DIRS}
)
target_link_libraries(${LIBRARY_NAME}
	StringUtils
	AppUtils
	${Boost_LIBRARIES}
)
set(${LIBRARY_NAME}_INCLUDE_DIRS 
	${CMAKE_CURRENT_SOURCE_DIR}include/
	${StringUtils_INCLUDE_DIRS}
	${AppUtils_INCLUDE_DIRS}
)
install(TARGETS
	${LIBRARY_NAME}
	RUNTIME DESTINATION ${INSTALL_BINARIES_DIR} COMPONENT Runtime
	ARCHIVE DESTINATION ${INSTALL_LIBRARIES_DIR} COMPONENT Runtime
)
install(FILES
	${INCLUDE_FILES}
	DESTINATION ${INSTALL_HEADERS_DIR}/${LIBRARY_NAME}
)
